package com.decoders.aerobit.IntroScreen;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Created by 10DECODERS on 2/21/2018.
 */

public class IntroScreenViewPager extends Scroller {

    public int mDuration=1500;
    public IntroScreenViewPager(Context context) {
        super(context);
    }

    public IntroScreenViewPager(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public IntroScreenViewPager(Context context, Interpolator interpolator, boolean flywheel) {
        super(context, interpolator, flywheel);
    }
    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        startScroll(startX,startY,dx,dy,mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }

    public int getmDuration() {
        return mDuration;
    }

    public void setmDuration(int duration) {
        mDuration = duration;
    }
}
