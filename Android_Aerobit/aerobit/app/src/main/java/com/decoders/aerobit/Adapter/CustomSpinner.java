package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.decoders.aerobit.R;

import java.util.ArrayList;

/**
 * Created by 10decoders on 2/10/18.
 */

public class CustomSpinner extends BaseAdapter {
    Context context;

    //String[] username;
    LayoutInflater inflter;
   private ArrayList<String> mNameList=new ArrayList<>();
   private ArrayList<String> mGenderArr=new ArrayList<>();

    public CustomSpinner(Context applicationContext,ArrayList<String> mNameList,ArrayList<String> mGenderArr) {
        this.context = applicationContext;

        this.mNameList = mNameList;
        this.mGenderArr=mGenderArr;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return mGenderArr.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.activity_custom_spinner, null);
         ImageView female = (ImageView) view.findViewById(R.id.imagename);
        ImageView male=(ImageView)view.findViewById(R.id.imageMale);
        TextView names = (TextView) view.findViewById(R.id.textname);

        names.setText(mNameList.get(i));


        String gender=mGenderArr.get(i);
        if(gender.trim().equals("male"))
        {
            male.setVisibility(View.VISIBLE);

        }
        if(gender.trim().equals("female"))
        {
            female.setVisibility(View.VISIBLE);

        }
        return view;
    }
}
