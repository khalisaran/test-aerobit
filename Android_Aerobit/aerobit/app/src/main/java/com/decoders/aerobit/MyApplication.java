package com.decoders.aerobit;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.aerobit.medapp.util.PrefManager;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

/**
 * Created by crosssales on 2/13/2018.
 */

public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        // some of your own operations before content provider will launch
        Realm.init(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("client_id", "szw6T6f8WdKbUPzWO7d8cA6J0zyYab05");
            jsonObject.put("client_secret", "p5bf1q-1aFIjucID34qaqCTsm7HncbsgexGAakHbDSR5rjVOK2A3nMFCNQpKgmIY");
            jsonObject.put("audience", "http://54.156.240.171:8000/");
            jsonObject.put("grant_type", "client_credentials");
            GetAuthorize(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void GetAuthorize(JSONObject jsonObject) {

        String url = "https://vbjgjko57j.execute-api.us-east-1.amazonaws.com/api/token";
        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.v("responce", response.toString(5));
                            String token = response.getString("access_token");
                            String expires_in = response.getString("expires_in");
                            PrefManager.getInstance().Authorize(getApplicationContext(), token, expires_in);

                        } catch (Exception e) {
                            Log.v("JSON Exception", response.toString());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.v("anError", anError.toString());


                    }
                });

    }
}
