package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.decoders.aerobit.R;

import java.util.ArrayList;

/**
 * Created by 10decoders on 2/13/18.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    ArrayList personNames;
    ArrayList personImages;
    Context context;

    public DataAdapter(Context context, ArrayList personNames, ArrayList personImages) {
        this.context = context;
        this.personNames = personNames;
        this.personImages = personImages;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_medicine, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int position) {

        viewHolder.name.setText((CharSequence) personNames.get(position));
        viewHolder.image.setImageResource((Integer) personImages.get(position));
    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        ImageView image;
        public ViewHolder(View view) {
            super(view);

            name = (TextView) itemView.findViewById(R.id.textview);
            image = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

}
