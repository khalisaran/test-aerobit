package com.decoders.aerobit.Settings;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.decoders.aerobit.Adapter.CustomAdapter;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.DatabaseContract;
import com.decoders.aerobit.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.decoders.aerobit.DatabaseContract.TaskColumns.CONTENT_FAMILY;

public class ProfileActivity extends AppCompatActivity {
    @BindView(R.id.profileSettings)
    TextView mTextView;
    @BindView(R.id.profile_btn_save)
    Button mbtnSave;
    @BindView(R.id.editPhoto)
    TextView mEditPhoto;
    @BindView(R.id.info)
    TextView mInfo;
    @BindView(R.id.edit_profile_fname)
    EditText mFname;
    @BindView(R.id.edit_profile_lname)
    EditText mLname;
    @BindView(R.id.RemoveMember)
    TextView mRemoveMember;
    @BindView(R.id.edit_profile_email)
    EditText mEmail;

    @BindView(R.id.profile_birth_year)
    TextView mBirthYear;
    @BindView(R.id.profile_spinnerYear)
    Spinner mSpinner;
    @BindView(R.id.radi1)
    RadioButton rdo_male;
    @BindView(R.id.radi2)
    RadioButton rdo_female;
    @BindView(R.id.profilemale)
    TextView textmale;
    @BindView(R.id.profilefemale)
    TextView textfemale;
    @BindView(R.id.backgroundProfile)
    ImageView background;

    private ArrayList<String> years = new ArrayList<String>();
    private int searchID;
    private String fname,lname,byear,gender,UserID,dbGender,selectedGender,lastSync,email;
    private Realm mRelam;
    private FamilyDataObserver mFamilyOberserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_settings);
        ButterKnife.bind(this);
        InitializeFonts();                                //// Typeface initial for all view
        LoadYear();

        mRelam=Realm.getDefaultInstance();


        Bundle extras = getIntent().getExtras();
        searchID = extras.getInt("db_id");

        mFname.setText(fname);
        mLname.setText(lname);
        mEmail.setText(email);

        if(gender.equals("male"))
        {
            rdo_female.setBackgroundResource(R.drawable.femaleblur);
            rdo_male.setBackgroundResource(R.drawable.male);
            textfemale.setTextColor(Color.BLACK);
            textmale.setTextColor(Color.GRAY);
            dbGender="male";
            selectedGender=dbGender;
        }
        if(gender.equals("female"))
        {
            rdo_male.setBackgroundResource(R.drawable.maleblur);
            rdo_female.setBackgroundResource(R.drawable.female);
            textmale.setTextColor(Color.BLACK);
            //textfemale.setTextColor(Color.BLACK);
            textfemale.setTextColor(Color.GRAY);
            dbGender="female";
            selectedGender=dbGender;
        }

        int i=years.indexOf(byear);
        mSpinner.setSelection(i);


        rdo_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rdo_male.isChecked()) {
                    //Toast.makeText(getApplicationContext(),"I am Male", Toast.LENGTH_SHORT).show();
                    rdo_female.setBackgroundResource(R.drawable.femaleblur);
                    rdo_male.setBackgroundResource(R.drawable.male);
                    textfemale.setTextColor(Color.BLACK);
                    textmale.setTextColor(Color.GRAY);
                    selectedGender="male";
                }
            }
        });

        rdo_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rdo_female.isChecked()) {
                    //Toast.makeText(getApplicationContext(),"I am Male", Toast.LENGTH_SHORT).show();
                    rdo_male.setBackgroundResource(R.drawable.maleblur);
                    rdo_female.setBackgroundResource(R.drawable.female);
                    textmale.setTextColor(Color.BLACK);
                    //textfemale.setTextColor(Color.BLACK);
                    textfemale.setTextColor(Color.GRAY);
                    selectedGender="female";
                }
            }
        });
        mRemoveMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(ProfileActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.popup_family, null);

                LinearLayout mLinearlayout=(LinearLayout)mView.findViewById(R.id.popupFamily);
                mLinearlayout.setVisibility(View.VISIBLE);

                Button btnDelete = (Button) mView.findViewById(R.id.btnDelete);
                Button btnRemove=(Button) mView.findViewById(R.id.btnCancel);

                mBuilder.setView(mView);
                mBuilder.setCancelable(true);
                Shadowpopup(false);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();
                btnRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                    }
                });
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer indexValue = mSpinner.getSelectedItemPosition();
                        String gen=years.get(indexValue);
                        String getFname=mFname.getText().toString().trim();
                        String getLname=mLname.getText().toString().trim();
                        String et_email=mEmail.getText().toString().trim();

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String currentDateandTime = sdf.format(new Date());
                        // Toast.makeText(getApplicationContext(),"Changes",Toast.LENGTH_LONG).show();
                        ContentValues contentValues=new ContentValues();

                        contentValues.put(DatabaseContract.TaskColumns.ID,searchID);
                        contentValues.put(DatabaseContract.TaskColumns.FIRST_NAME,getFname);
                        contentValues.put(DatabaseContract.TaskColumns.LAST_NAME,getLname);
                        contentValues.put(DatabaseContract.TaskColumns.GENDER,selectedGender);
                        contentValues.put(DatabaseContract.TaskColumns.BIRTH_YEAR,gen);
                        contentValues.put(DatabaseContract.TaskColumns.STATUS,"NotSynced");
                        contentValues.put(DatabaseContract.TaskColumns.USER_ID,UserID);
                        contentValues.put(DatabaseContract.TaskColumns.LASTSYNC,lastSync);
                        contentValues.put(DatabaseContract.TaskColumns.UPDATETIME,currentDateandTime);
                        contentValues.put(DatabaseContract.TaskColumns.EMAIL,et_email);
                        contentValues.put(DatabaseContract.TaskColumns.ACTIVE,"false");

                        getContentResolver().update(DatabaseContract.TaskColumns.CONTENT_FAMILY,contentValues,
                                DatabaseContract.TaskColumns.ID+"=?",new String[]{String.valueOf(searchID)});
                        Intent intent=new Intent(getApplicationContext(), BottomNavigation.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                });

            }
        });

        mbtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  Integer indexValue = mSpinner.getSelectedItemPosition();
                  String gen=years.get(indexValue);
                  String getFname=mFname.getText().toString().trim();
                  String getLname=mLname.getText().toString().trim();
                  String et_email=mEmail.getText().toString().trim();
                  Log.v("Name",getFname+" "+fname);

                if(dbGender.equals(selectedGender)&&getFname.equals(fname)&&getLname.equals(lname)&&byear.equals(gen)
                        &&et_email.equals(email))
                {
                    //Toast.makeText(getApplicationContext(),"NoChanges",Toast.LENGTH_LONG).show();

                }else
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    String currentDateandTime = sdf.format(new Date());
                   // Toast.makeText(getApplicationContext(),"Changes",Toast.LENGTH_LONG).show();
                    ContentValues contentValues=new ContentValues();

                    contentValues.put(DatabaseContract.TaskColumns.ID,searchID);
                    contentValues.put(DatabaseContract.TaskColumns.FIRST_NAME,getFname);
                    contentValues.put(DatabaseContract.TaskColumns.LAST_NAME,getLname);
                    contentValues.put(DatabaseContract.TaskColumns.GENDER,selectedGender);
                    contentValues.put(DatabaseContract.TaskColumns.BIRTH_YEAR,gen);
                    contentValues.put(DatabaseContract.TaskColumns.STATUS,"NotSynced");
                    contentValues.put(DatabaseContract.TaskColumns.USER_ID,UserID);
                    contentValues.put(DatabaseContract.TaskColumns.LASTSYNC,lastSync);
                    contentValues.put(DatabaseContract.TaskColumns.UPDATETIME,currentDateandTime);
                    contentValues.put(DatabaseContract.TaskColumns.EMAIL,et_email);
                    contentValues.put(DatabaseContract.TaskColumns.ACTIVE,"true");

                    getContentResolver().update(DatabaseContract.TaskColumns.CONTENT_FAMILY,contentValues,
                    DatabaseContract.TaskColumns.ID+"=?",new String[]{String.valueOf(searchID)});
                    Intent intent=new Intent(getApplicationContext(), BottomNavigation.class);
                    startActivity(intent);
                }


            }
        });
        mFamilyOberserver = new FamilyDataObserver();


    }
    public void LoadYear()
    {
        for (int i = 2000; i <= 2025; i++) {
            years.add(Integer.toString(i));
        }
        CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(), years);
        mSpinner.setAdapter(customAdapter);

    }
    public void InitializeFonts()
    {
        Typeface face = Typeface.createFromAsset(getAssets(),
                "Font/AEH.ttf");
        mTextView.setTypeface(face);
        Typeface Avenir_Roman=Typeface.createFromAsset(getAssets(),"Font/Avenir Roman.otf");
        Typeface face2=Typeface.createFromAsset(getAssets(),"Font/Avenir Heavy.otf");
        mbtnSave.setTypeface(Avenir_Roman);
        mEditPhoto.setTypeface(Avenir_Roman);

        mInfo.setTypeface(face2);
        mFname.setTypeface(Avenir_Roman);
        mLname.setTypeface(Avenir_Roman);
        mRemoveMember.setTypeface(Avenir_Roman);
        mEmail.setTypeface(Avenir_Roman);
        textfemale.setTypeface(Avenir_Roman);
        textmale.setTypeface(Avenir_Roman);
        mBirthYear.setTypeface(Avenir_Roman);

    }

    @Override
    protected void onStart() {
        super.onStart();
        getContentResolver().registerContentObserver(CONTENT_FAMILY,true,mFamilyOberserver);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(mFamilyOberserver!=null)
        {
            getContentResolver().unregisterContentObserver(mFamilyOberserver);
        }

    }
    private void refreshArticles() {
        Log.i(getClass().getName(), "Articles data has changed!");
    }
    private final class FamilyDataObserver extends ContentObserver {
        private FamilyDataObserver() {
            super(new Handler(Looper.getMainLooper()));
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            refreshArticles();
        }
    }

    public void Shadowpopup(boolean isVisible) {
        if (isVisible) {
            background.setVisibility(View.VISIBLE);
        } else {
            background.setVisibility(View.GONE);
        }
    }
}
