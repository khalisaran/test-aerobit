package com.decoders.aerobit;

import android.net.Uri;

/**
 * Created by crosssales on 2/12/2018.
 */

public class DatabaseContract {
    public static final String CONTENT_AUTHORITY = "com.example.sync";
    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    static final String PATH_ARTICLES = "articles";
    static final String PATH_MEDICINE="medicine";
    static final String PATH_FAMILY="family";
    static final String PATH_USER="user";
    static final String PATH_CAREPLAN="carePlan";

    public static abstract class TaskColumns {
        //Task ID
        public static final String _ID = "id";
        public static final String name = "name";
         public static final String phone = "phone";
        public static final String address="address";


        public static final String ID="id";
        public static final String USER_ID="userID";
        public static final String FIRST_NAME="firstname";
        public static final String LAST_NAME="lastname";
        public static final String BIRTH_YEAR="birthyear";
        public static final String GENDER="gender";
        public static final String STATUS="status";
        public static final String LASTSYNC="lastSync";
        public static final String UPDATETIME="updateTime";
        public static final String EMAIL="email";
        public static final String ACTIVE="active";
        public static final String THEME="theme";


        public static final String U_ID="id";
        public static final String U_USER_ID="userid";
        public static final String U_EMAIL="email";
        public static final String U_PASSWORD="password";
        public static final String U_FIRSTNAME="firstname";
        public static final String U_LASTNAME="lastname";
        public static final String U_BIRTHYEAR="year";
        public static final String U_GENDER="gender";
        public static final String U_STATUS="status";
        public static final String U_LASTSYNC="lastsync";
        public static final String U_THEME="theme";


        public static final String M_ID="id";
        public static final String M_USERID="userID";
        public static final String M_MED_ID="medID";
        public static final String M_TYPE="medType";
        public static final String M_NAME="mName";
        public static final String M_STRENGTH="mStrength";
        public static final String M_STATUs="mStatus";
        public static final String M_LAST_SYNC="lastSync";
        public static final String M_TIME="mTime";




        // ContentProvider information for articles
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ARTICLES).build();
        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_ARTICLES;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_ARTICLES;

        public static final Uri CONTENT_URI1=BASE_CONTENT_URI.buildUpon().appendPath(PATH_MEDICINE).build();

        public static final String CONTENT_TYPE1 =
                "vnd.android.cursor.dir/" + CONTENT_URI1 + "/" + PATH_MEDICINE;
        public static final String CONTENT_ITEM_TYPE1 =
                "vnd.android.cursor.item/" + CONTENT_URI1 + "/" + PATH_MEDICINE;

        public static final Uri CONTENT_FAMILY=BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAMILY).build();

        public static final String CONTENT_FAMILYTYPE1 =
                "vnd.android.cursor.dir/" + CONTENT_FAMILY + "/" + PATH_FAMILY;
        public static final String CONTENT_FAMILYITEM_TYPE1 =
                "vnd.android.cursor.item/" + CONTENT_FAMILY + "/" + PATH_FAMILY;


        public static final Uri CONTENT_USER=BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        public static final String USER_CONTENT= "vnd.android.cursor.dir/" + CONTENT_USER + "/" + PATH_USER;

        public static final String CONTENT_USER_TYPE1 =
                "vnd.android.cursor.item/" + CONTENT_USER + "/" + PATH_USER;


        public static final Uri CONTENT_CARE=BASE_CONTENT_URI.buildUpon().appendPath(PATH_CAREPLAN).build();

        public static final String CARE_CONTENT= "vnd.android.cursor.dir/" + CONTENT_CARE + "/" + PATH_CAREPLAN;

        public static final String CARE_PLAN_TYPE1 =
                "vnd.android.cursor.item/" + CONTENT_CARE + "/" + PATH_CAREPLAN;





    }
}
