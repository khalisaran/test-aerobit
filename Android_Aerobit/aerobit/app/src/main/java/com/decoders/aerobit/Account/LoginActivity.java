package com.decoders.aerobit.Account;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.remote.Session;
import com.aerobit.medapp.service.intent.DownloadResultReceiver;
import com.aerobit.medapp.service.intent.DownloadService;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements DownloadResultReceiver.Receiver{

    private TextView text;

    private Button btnLogin;
    private static final String TAG = LoginActivity.class.getName();

    private EditText mEmail, mPassword;
    private TextInputLayout etemaillogin, etPasswordLogin;

    private Typeface font, font2;

    private ProgressBar progressBar;
    private ImageView normaleye, hideeye;
    private PersonLoadDataService mPersonLoadDataService;
    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;

    private DownloadResultReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPersonLoadDataService = PersonLoadDataService.getInstance();

        initViews();
        toolbarInfo();


        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEmail.setError(null);
                etemaillogin.setError(null);

            }
        });
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etPasswordLogin.setError(null);
                normaleye.setVisibility(View.VISIBLE);
            }
        });

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
    }

    //button onclick for show password
    public void onClick_showPassword(View v){
        normaleye.setVisibility(View.GONE);
        hideeye.setVisibility(View.VISIBLE);
        mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
    }
    public void onClick_HidePassword(View v){
        hideeye.setVisibility(View.GONE);
        normaleye.setVisibility(View.VISIBLE);
        mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_RUNNING:

                progressBar.setVisibility(View.VISIBLE);
                btnLogin.getBackground().setAlpha(100);
                break;
            case DownloadService.STATUS_FINISHED:
                progressBar.setVisibility(View.GONE);
                btnLogin.setBackground(getResources().getDrawable(R.drawable.shapes));
                /* Hide progress & extract result from bundle */
                progressBar.setVisibility(View.INVISIBLE);

                /* Navigate to the Dashboard screen */
                Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


                break;
            case DownloadService.STATUS_ERROR:

                progressBar.setVisibility(View.GONE);
                btnLogin.setBackground(getResources().getDrawable(R.drawable.shapes));

                /* Handle the error */
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, error, Toast.LENGTH_LONG).show();

                break;
        }
    }

    //Login Button click function
    public void loginFunction(View v) {
        String email = mEmail.getText().toString();
        if (!isValidEmail(email)) {
            etemaillogin.setError("Invalid Email");
            mEmail.setError("Invalid Email");
        }
        final String pass = mPassword.getText().toString();
        if (!isValidPassword(pass)) {
            etPasswordLogin.setError("Password should be above 6 characters");
            mPassword.setError("Invalid Password");
            normaleye.setVisibility(View.INVISIBLE);
            hideeye.setVisibility(View.INVISIBLE);
        }
        if (isValidEmail(email)) {

            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(mPassword.getWindowToken(), 0);

            // show the progress bar

            // do the login
            Session mSession = new Session();
            mSession.setEmail(mEmail.getText().toString());
            mSession.setPassword(mPassword.getText().toString());
            mSession = PersonLoadDataService.getInstance().login(getApplicationContext(), mSession);

            /* Starting Download Service */
            mReceiver = new DownloadResultReceiver(new Handler());
            mReceiver.setReceiver(this);
            Intent intent = new Intent(Intent.ACTION_SYNC, null, this, DownloadService.class);

            /* Send optional extras to Download IntentService */
            intent.putExtra("userId", mSession.getUserId());
            intent.putExtra("LOGIN", true);

            intent.putExtra("receiver", mReceiver);
            startService(intent);

        }
    }

    private void initViews() {
        btnLogin=(Button)findViewById(R.id.buttonlogin);
        etemaillogin = (TextInputLayout) findViewById(R.id.etemaillogin);
        etPasswordLogin = (TextInputLayout) findViewById(R.id.etPasswordLogin);
        text = (TextView) findViewById(R.id.btntextview);
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.etPassword);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        normaleye = (ImageView) findViewById(R.id.normaleye);
        hideeye = (ImageView) findViewById(R.id.hideeye);
        mToolbar=(Toolbar)findViewById(R.id.loginToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);

        font = Typeface.createFromAsset(this.getAssets(), "Font/Avenir Heavy.otf");
        font2 = Typeface.createFromAsset(this.getAssets(), "Font/Avenir Roman.otf");
        normaleye.setVisibility(View.VISIBLE);

        text.setTypeface(font2);
        btnLogin.setTypeface(font2);

    }

    private boolean isValidEmail(String email) {
       // String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z0-9]+\\.+[a-z]+";
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 4) {
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Login");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                LoginActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });
        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){

                    loginFunction(v);



                    return true;
                }
                return false;
            }
        });


    }

}
