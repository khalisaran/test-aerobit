package com.decoders.aerobit.Family;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Person;
import com.decoders.aerobit.Account.MedicineActivity;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FamilyMemberActivity extends AppCompatActivity {
    @BindView(R.id.btns3)
    Button btnNext;


    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;

    private static final String PERSON_REF_DATA="person";
    private static final String FAMILY_REF_DATA="familyMember";
    private Person person;
    private Person familyMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            System.out.println("I am restoring the person in creaet");
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
        }
        setContentView(R.layout.activity_family_member);
        ButterKnife.bind(this);

        initViews();
        toolbarInfo();
        getBundle(getIntent());

    }

    @OnClick(R.id.btns3)
    public void onClick(){
        Intent intent=new Intent(getApplicationContext(),AddFamilyNameActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(PERSON_REF_DATA, person);
        intent.putExtra(FAMILY_REF_DATA, familyMember);
        System.out.println("Family details -- "+familyMember);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }


    private void initViews(){
        mToolbar=(Toolbar)findViewById(R.id.createAccountToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);

    }
    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Family Member");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                FamilyMemberActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });


    }

    void getBundle(Intent intent){
        familyMember = (Person) getIntent().getExtras()
                .getSerializable(FAMILY_REF_DATA);
        if(familyMember ==null){
            familyMember = new Person();
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MedicineActivity.class);
        intent.putExtra(FAMILY_REF_DATA, familyMember);
        intent.putExtra(PERSON_REF_DATA, person);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }

}
