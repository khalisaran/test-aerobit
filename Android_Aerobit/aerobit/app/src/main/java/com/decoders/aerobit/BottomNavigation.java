package com.decoders.aerobit;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.service.local.CarePlanService;
import com.aerobit.medapp.service.local.MedicationService;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.sync.authenticator.AccountGeneral;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.Fragments.DashboardImage;
import com.decoders.aerobit.Fragments.DrawerFragment;
import com.decoders.aerobit.Fragments.MedicationList;
import com.decoders.aerobit.MedicationScreen.SelectMedicationTypeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BottomNavigation extends AppCompatActivity {

    @BindView(R.id.dashboardbtn)
    ImageView dashboardbtn;
    @BindView(R.id.medicationbtn)
    ImageView medicationbtn;
    @BindView(R.id.addbtn)
    ImageView addbtn;
    @BindView(R.id.updatebtn)
    ImageView updatebtn;
    @BindView(R.id.morebtn)
    ImageView morebtn;
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;
    RelativeLayout mRelativeLayout;

    private Toolbar mToolbar;
    private ImageView userMale,userFemale;
    private TextView userName;

    private PersonService mPersonService;
    private MedicationService mMedicationService;
    private CarePlanService mCarePlanService;
    private PreferenceService mPreferenceService;
    SelectedFragment selectedFragment;

    public boolean medication;
    public String selectedUser;
    Person selectedUserObject;

    private static BottomNavigation instance = null;

    private static final String PERSON_REF_DATA="person";
    private Person person;

    public BottomNavigation() {
        instance = this;
    }
    public static synchronized BottomNavigation getInstance() {
        if (instance == null) {
            instance = new BottomNavigation();
        }
        return BottomNavigation.instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        ButterKnife.bind(this);
        mPersonService=PersonService.getInstance();
        mMedicationService=MedicationService.getInstance();
        mCarePlanService = CarePlanService.getInstance();
        mPreferenceService=PreferenceService.getInstance();



        initViews();
        UpdateUI();
        updateToolbar();

        selectedFragment = new SelectedFragment();

        loadNavigationDrawer(new DrawerFragment());
        loadFragment(new DashboardImage());
        selectedFragment.setName(FragmentName.DASHBOARD);

        try {
            final Bundle extras = getIntent().getExtras();
            if(extras!=null){
                String activity = extras.getString("medicationFragment");
                if(activity != null && activity.equals("medicationFragment")) {
                    selectedFragment.setName(FragmentName.MEDICATIONS);
                    showMedicationFragment();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");


        dashboardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medication=false;
             //   loadFragment(new DashboardFragment());
                loadFragment(new DashboardImage());
                updatebtn.setImageResource(R.drawable.updates);
                dashboardbtn.setImageResource(R.drawable.selectdashboard);
                medicationbtn.setImageResource(R.drawable.medication);
                morebtn.setImageResource(R.drawable.more);
                selectedFragment.setName(FragmentName.DASHBOARD);
            }
        });

        medicationbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedFragment.setName(FragmentName.MEDICATIONS);
                showMedicationFragment();
            }
        });

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SelectMedicationTypeActivity.class);
                intent.putExtra("UserID",selectedUser);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(PERSON_REF_DATA, person);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                medication = false;
            }
        });

        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medication=false;
                updatebtn.setImageResource(R.drawable.selectupdate);
                dashboardbtn.setImageResource(R.drawable.dashboard);
                medicationbtn.setImageResource(R.drawable.medication);
                morebtn.setImageResource(R.drawable.more);
                selectedFragment.setName(FragmentName.UPDATES);

            }
        });

        morebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medication=false;
                morebtn.setImageResource(R.drawable.selectmore);
                dashboardbtn.setImageResource(R.drawable.dashboard);
                medicationbtn.setImageResource(R.drawable.medication);
                updatebtn.setImageResource(R.drawable.updates);
                selectedFragment.setName(FragmentName.MORE);
            }
        });
        mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        //Adding Sync Account during onCreate of dashboard
        AccountGeneral.createSyncAccount(this);
    }

    public void showMedicationFragment() {
        String selectedFragmentName = selectedFragment.getName().toString();
        if(selectedFragmentName != null && selectedFragmentName.equals("MEDICATIONS")) {
            showMedicationButton();
            loadFragment(new MedicationList());
        }
    }


    private void showMedicationButton() {
        medication=true;
        updatebtn.setImageResource(R.drawable.updates);
        dashboardbtn.setImageResource(R.drawable.dashboard);
        medicationbtn.setImageResource(R.drawable.selectmedication);
        morebtn.setImageResource(R.drawable.more);
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    private void loadNavigationDrawer(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frame_navigation,Fragment.instantiate(BottomNavigation.this, fragment.getClass().getName()));
        fragmentTransaction.commit(); // save the changes
    }

    public void UpdateUI() {
        person = PrefManager.getInstance().getCurrentUserObject(getApplicationContext());
        //selectedUser= PrefManager.getInstance().getCurrentUser(getApplicationContext());
        if(person!=null) {
                mRelativeLayout.setVisibility(View.VISIBLE);
                userName.setVisibility(View.VISIBLE);
                userName.setText(person.getFirstName()+" "+person.getLastName());
                if ("male".equalsIgnoreCase(person.getGender())) {
                    userMale.setVisibility(View.VISIBLE);
                    userFemale.setVisibility(View.GONE);
                } else if("female".equalsIgnoreCase(person.getGender())){
                    userFemale.setVisibility(View.VISIBLE);
                    userMale.setVisibility(View.GONE);
                }
        }
    }

    public void updateToolbar()
    {
        person = PrefManager.getInstance().getCurrentUserObject(getApplicationContext());

        if(person!=null){
            String code=person.getThemeColor();
            if(code == null ){
                code = "#f83f91";
            }
            mToolbar.setBackgroundColor(Color.parseColor(code));
        }

    }

    private void initViews(){
        updatebtn.setImageResource(R.drawable.updates);
        dashboardbtn.setImageResource(R.drawable.selectdashboard);
        medicationbtn.setImageResource(R.drawable.medication);
        morebtn.setImageResource(R.drawable.more);
        mToolbar = (Toolbar) findViewById(R.id.main_toolbarrr);
        mRelativeLayout=(RelativeLayout)findViewById(R.id.toolObj);
        userMale=(ImageView)findViewById(R.id.userMale);
        userFemale=(ImageView)findViewById(R.id.userFemale);
        userName=(TextView)findViewById(R.id.userName);
    }

    private Person getCurrentPersonObject() {
        String currentUser = PrefManager.getInstance().getCurrentUser(getApplicationContext());
        return (Person) AerobitUtil.fromJson(currentUser, Person.class);
    }

    class SelectedFragment {

        private FragmentName name;

        public FragmentName getName() {
            return name;
        }

        public void setName(FragmentName name) {
            this.name = name;
        }
    }

    public enum FragmentName {
        DASHBOARD("DASHBOARD"),
        MEDICATIONS("MEDICATIONS"),
        UPDATES("UPDATES"),
        MORE("MORE");
        private final String text;
        /**
         * @param text
         */
        FragmentName(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }

}
