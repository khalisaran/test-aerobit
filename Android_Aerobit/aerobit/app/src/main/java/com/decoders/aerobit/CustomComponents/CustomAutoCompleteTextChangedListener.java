package com.decoders.aerobit.CustomComponents;

/**
 * Created by 10DECODERS on 3/7/2018.
 */

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.service.local.MedicationService;
import com.decoders.aerobit.Adapter.AutocompleteCustomArrayAdapter;
import com.decoders.aerobit.MedicationScreen.InhalerDetailsActivity;
import com.decoders.aerobit.R;

import java.util.List;

public class CustomAutoCompleteTextChangedListener implements TextWatcher{

    public static final String TAG = "CustomAutoCompleteTextChangedListener.java";
    Context context;
    private MedicationService medicationService;

    public CustomAutoCompleteTextChangedListener(Context context){
        this.context = context;
        medicationService = MedicationService.getInstance();
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        try {

            final InhalerDetailsActivity inhalerDetails = ((InhalerDetailsActivity) context);

            // update the adapater
            inhalerDetails.customAutoCompleteAdapter.notifyDataSetChanged();

            // get suggestions from the database
            final List<Medication> myObjs = medicationService.getMedicationByName(userInput.toString(), inhalerDetails.type);
             // update the adapter
            inhalerDetails.customAutoCompleteAdapter = new AutocompleteCustomArrayAdapter(inhalerDetails,
                    R.layout.custom_autocomplete_textview, myObjs, (AutocompleteCustomArrayAdapter.AutoListAdapterListener) context
                    , count, userInput.toString());


            inhalerDetails.medicineNameAutoComplete.setAdapter(inhalerDetails.customAutoCompleteAdapter);

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}