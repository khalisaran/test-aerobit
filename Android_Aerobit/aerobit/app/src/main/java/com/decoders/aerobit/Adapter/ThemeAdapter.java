package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.decoders.aerobit.R;

/**
 * Created by 10decoders on 2/17/18.
 */

public class ThemeAdapter extends BaseAdapter {

    Context context;
    int themeimage[];
    String[] themename;
    LayoutInflater inflter;

    public ThemeAdapter (Context applicationContext, int[] themeimage, String[] themename) {
        this.context = applicationContext;
        this.themeimage = themeimage;
        this.themename = themename;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return themeimage.length;
    }

    @Override
    public Object getItem(int position) {
        return themeimage;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {

        view = inflter.inflate(R.layout.themespinner, null);
        ImageView icon = (ImageView) view.findViewById(R.id.themeimage);
        TextView names = (TextView) view.findViewById(R.id.themename);
        icon.setImageResource(themeimage[i]);
        names.setText(themename[i]);
        return view;
    }
}
