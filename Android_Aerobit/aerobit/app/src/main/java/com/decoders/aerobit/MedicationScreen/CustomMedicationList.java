package com.decoders.aerobit.MedicationScreen;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.decoders.aerobit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 10decoders on 2/10/18.
 */

public class CustomMedicationList extends BaseAdapter{

    Context context;
    //String medicationtype[];
    ArrayList<String> medicationtypeList=new ArrayList<>();
    int medicationimg[];


    LayoutInflater inflter;


    public CustomMedicationList(Context applicationContext, ArrayList<String> medicationtype, int[] medicationimg) {
        this.context = context;
        this.medicationtypeList = medicationtype;
        this.medicationimg = medicationimg;

        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return medicationtypeList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        view = inflter.inflate(R.layout.custom_medicationlist, null);
        TextView country = (TextView) view.findViewById(R.id.medicationtype);
        ImageView icon = (ImageView) view.findViewById(R.id.medicationimg);
        StringBuffer medicationType = new StringBuffer(medicationtypeList.get(i));
        SpannableStringBuilder sb = new SpannableStringBuilder(medicationType);
        sb.setSpan(bss,0,medicationType.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        country.setText(sb);
        icon.setImageResource(medicationimg[i]);
        return view;
    }

}
