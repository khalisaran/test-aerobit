package com.decoders.aerobit.CustomComponents;

/**
 * Created by 10DECODERS on 2/23/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.service.local.CarePlanService;
import com.aerobit.medapp.util.AerobitUtil;
import com.decoders.aerobit.Fragments.MedicationList;
import com.decoders.aerobit.MedicationScreen.InhalerDetailsActivity;
import com.decoders.aerobit.R;



public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private LinearLayout editMedication;
    private TextView removeMedicationBottom;
    private OnDissmissListener callback;
    private OnDeleteListener mCallbackDelete;
    private String carePlan;
    private CarePlanService carePlanService;

    public interface OnDissmissListener {
        void doDissmiss(boolean dissmiss);
        void doShow(Bundle show);
    }
    public interface OnDeleteListener{
        public void onDeleteMedicine(String id);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            callback = (OnDissmissListener) getTargetFragment();
            mCallbackDelete=(OnDeleteListener)getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling Fragment must implement OnAddFriendListener");
        }
        final View v = inflater.inflate(R.layout.content_bottom_sheet, container, false);
        editMedication = (LinearLayout) v.findViewById(R.id.editLayout);
        removeMedicationBottom = (TextView) v.findViewById(R.id.removeMedicationBottom);
        carePlanService = CarePlanService.getInstance();

        final String medicatin = "editMedication";

        Bundle bundle = getArguments();

        if (getArguments() != null) {
            carePlan = bundle.getSerializable("carePlan").toString();
        }

        editMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InhalerDetailsActivity.class);
                intent.putExtra("activityName",medicatin);
                intent.putExtra("carePlan", carePlan);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                callback.doDissmiss(true);
            }
        });

        removeMedicationBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CarePlan getCarePlan = (CarePlan) AerobitUtil.fromJson(carePlan, CarePlan.class);
              mCallbackDelete.onDeleteMedicine(getCarePlan.getId());
              callback.doDissmiss(true);
                //  deleteMedication(getCarePlan.getId());
              //  Toast.makeText(getContext(), "Deleted.", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    public void deleteMedication(final String id) {
        carePlanService.deactivateCarePlan(getActivity().getContentResolver(), id);
        callback.doDissmiss(true);
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(R.id.medicationFragment, new MedicationList());
        tr.commit();
    }


}