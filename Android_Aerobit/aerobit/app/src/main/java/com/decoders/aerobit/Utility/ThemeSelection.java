package com.decoders.aerobit.Utility;

import android.content.Context;
import android.widget.ImageView;

import com.decoders.aerobit.R;

/**
 * Created by crosssales on 3/5/2018.
 */

public class ThemeSelection {
    public static final String color_biscuit="#d7c797";   //white
    public static final String code_ascent="#FF4081";  // ascent
    public static final String code_skyBlue="#FF3498E4";
    public static final String code_ightGreen="#FF6CCC6D";
    public static final String code_yellow="#FFDA9F2F";
    public static final String code_red="#FFD23737";
    public static final String code_brBlue="#FF614EDA";
    public static final String code_lightPink="#FF8D3CBF";
    public static final String code_lightBlue="#FF35CCB5";

    int[] themeimage = {R.drawable.whitedrop, R.drawable.lightbluedrop, R.drawable.greendrop, R.drawable.lightwaterbluedrop,
             R.drawable.orangedrop, R.drawable.droppink, R.drawable.violetdrop,
             R.drawable.darkpinkdrop, R.drawable.darkbluedrop};


    public static void setImage(ImageView imageView,String colorCode){
        switch (colorCode){
            case code_lightPink:
            {
                imageView.setImageResource(R.drawable.violetdrop);
                break;
            }
            case color_biscuit:
            {
                imageView.setImageResource(R.drawable.raindrop_biscuitecolor);
                break;
            }
            case code_ascent:
            {
                imageView.setImageResource(R.drawable.droppink);
                break;
            }
            case code_skyBlue:
            {
                imageView.setImageResource(R.drawable.lightbluedrop);
                break;
            }
            case code_lightBlue:
            {
                imageView.setImageResource(R.drawable.lightwaterbluedrop);
                //imageView.setImageResource(R.drawable.darkbluedrop);
                break;
            }
            case code_brBlue:
            {
                imageView.setImageResource( R.drawable.darkbluedrop);
                break;
            }
            case code_red:
            {
                imageView.setImageResource(R.drawable.darkpinkdrop);
                break;
            }
            case code_ightGreen:
            {
                imageView.setImageResource(R.drawable.greendrop);
                break;
            }
            case code_yellow:
            {
                imageView.setImageResource(R.drawable.orangedrop);
                break;
            }
        }

    }
}
