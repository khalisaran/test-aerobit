package com.decoders.aerobit.Fragments;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.Adapter.SidebarMemberAdapter;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.Family.AddFamilyNameActivity;
import com.decoders.aerobit.R;

import java.util.List;


/**
 * Created by 10DECODERS on 2/21/2018.
 */

public class DrawerFragment extends Fragment implements SidebarMemberAdapter.ListAdapterListener {

    //    private TextView familyMember;
    private SidebarMemberAdapter sidebarMemberAdapter;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    LinearLayout headerProfile;
    TextView mUserName, textView;
    ImageView mUserImage;
    private List<Person> familyMemberList;
    private Person mSelectePersoObject;

    View view;
    Context mContext;

    BottomNavigation instans = null;
    SidebarMemberAdapter adapter;
    private String selectedUser;

    PersonService personService;
    PreferenceService mPreferenceService;
    private static final String PERSON_REF_DATA="person";

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        initViews();
        instans = BottomNavigation.getInstance();

        mPreferenceService=PreferenceService.getInstance();

        updateProfileUI();
        UpdateFamilyUI();

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddFamilyNameActivity.class);
                intent.putExtra(PERSON_REF_DATA, mSelectePersoObject);
                startActivity(intent);
                instans.drawerLayout.closeDrawers();
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.drawer_layout, container, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onReceiveUser(Person objPerson) {

        // find the user Preference
        Preferences preferences = PreferenceService.getInstance().getPreferenceByPerson(getActivity().getContentResolver(), objPerson);
        objPerson.setThemeColor(preferences.getAvatarColor());
        PrefManager.getInstance().setSelectedUser(getActivity().getApplicationContext(), objPerson);
        updateProfileUI();
        UpdateFamilyUI();
        instans.UpdateUI();
        instans.updateToolbar();
        instans.showMedicationFragment();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    private void initViews(){
        mUserName = (TextView) view.findViewById(R.id.userName);
        mUserImage = (ImageView) view.findViewById(R.id.userImage);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.addFamilyMember);
        headerProfile = (LinearLayout) view.findViewById(R.id.headerProfile);
        textView = (TextView) view.findViewById(R.id.textView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewMember);

    }
    public void updateProfileUI() {

        String selectedUser=PrefManager.getInstance().getCurrentUser(getActivity().getApplicationContext());
        mSelectePersoObject = (Person) AerobitUtil.fromJson(selectedUser, Person.class);
        Preferences preferences = PreferenceService.getInstance().getPreferenceByPerson(getActivity().getContentResolver(), mSelectePersoObject);
        String code=null;
        if(preferences == null || preferences.getAvatarColor() == null ){
            code = AerobitUtil.DEFAULT_THEME_COLOR;

        } else{
            code = preferences.getAvatarColor();
        }
        headerProfile.setBackgroundColor(Color.parseColor(code));
        mUserName.setText("");

        if (selectedUser != null) {
            mUserName.setText(mSelectePersoObject.getFirstName()+" "+mSelectePersoObject.getLastName());
            textView.setVisibility(View.VISIBLE);
            mUserImage.setVisibility(View.VISIBLE);
            if ("male".equalsIgnoreCase(mSelectePersoObject.getGender())) {
                mUserImage.setImageResource(R.drawable.male);
            } else if ("female".equalsIgnoreCase(mSelectePersoObject.getGender())) {
                mUserImage.setImageResource(R.drawable.female);
            }
            Log.i("updateProfileUi", mSelectePersoObject.getFirstName() + mSelectePersoObject.getGender());
        }
    }

    public void UpdateFamilyUI() {
        personService = PersonService.getInstance();
        familyMemberList = personService.getAllPerson(getActivity().getApplicationContext().getContentResolver());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new SidebarMemberAdapter(familyMemberList, this, getActivity().getApplicationContext());
        recyclerView.setAdapter(adapter);
        mContext = view.getContext();

    }


}
