package com.decoders.aerobit.MedicationScreen;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.R;
//import com.decoders.aerobit.Utility.ThemeUtil;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class SelectMedicationTypeActivity extends AppCompatActivity {
    @BindView(R.id.list_item)
    ListView listView;
    @BindView(R.id.medTypeRelative)
    RelativeLayout medTypeRelative;
    Realm mRealm;
    ArrayList<String> medicationtypeList;
    int medicationimg[]={R.drawable.medicationinhalers, R.drawable.medicationsyrups, R.drawable.medicationtablets};

    private String userID;
    private Toolbar toolbarMain;
    private RelativeLayout whitearw;

    private Toolbar mToolbar;
    private RelativeLayout parentRelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_medication_type);
        ButterKnife.bind(this);

        initViews();
        toolbarInfo();
        medTypeRelative.setVisibility(View.VISIBLE);
        mRealm=Realm.getDefaultInstance();

        final Bundle extras = getIntent().getExtras();
        userID=extras.getString("UserID");

        initialToolbar();                                          //Initializing toolbar
        updateToolbar();



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                if (i == 0) {
                    Intent intent = new Intent(getApplicationContext(), InhalerDetailsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("type", "INHALER");
                    intent.putExtra("UserID",userID);
                    intent.putExtra("isStrength","Yes");
                    intent.putExtra("editActivity","No");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }  else if (i == 1) {
                    Intent intent = new Intent(getApplicationContext(), InhalerDetailsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("type", "SYRUP");
                    intent.putExtra("editActivity","No");
                    intent.putExtra("UserID",userID);
                    intent.putExtra("isStrength","No");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (i == 2) {
                    Intent intent = new Intent(getApplicationContext(), InhalerDetailsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("type", "TABLET");
                    intent.putExtra("UserID",userID);
                    intent.putExtra("isStrength","No");
                    intent.putExtra("editActivity","No");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        medicationtypeList=new ArrayList<>();
        medicationtypeList.add(getResources().getString(R.string.inhalers));
        medicationtypeList.add(getResources().getString(R.string.syrups));
        medicationtypeList.add(getResources().getString(R.string.tablets));
        Log.v("ArrayList",medicationtypeList.toString());
        CustomMedicationList customMedicationList = new CustomMedicationList(getApplicationContext(), medicationtypeList, medicationimg);
        listView.setAdapter(customMedicationList);
    }

    private void initViews(){
        mToolbar=(Toolbar)findViewById(R.id.main_toolbarr);
        parentRelative=(RelativeLayout)findViewById(R.id.medTypeRelative);
        whitearw =(RelativeLayout)findViewById(R.id.navBackbutton1);
        parentRelative.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    public void initialToolbar(){
        toolbarMain=(Toolbar)findViewById(R.id.main_toolbarr);
        toolbarMain.setTitle("");
    }
    public void updateToolbar() {

        String selectedUser = PrefManager.getInstance().getCurrentUser(getApplicationContext());
        if (selectedUser != null) {
            Log.v("selectedUserInfo", selectedUser);
            Person selectedUserObject = (Person) AerobitUtil.fromJson(selectedUser, Person.class);

            Preferences preferences = PreferenceService.getInstance().getPreferenceByPerson(getContentResolver(), selectedUserObject);

            String code = null;
            if (preferences == null || preferences.getAvatarColor() == null) {
                code = AerobitUtil.DEFAULT_THEME_COLOR;

            } else {
                code = preferences.getAvatarColor();
            }
            mToolbar.setBackgroundColor(Color.parseColor(code));
        }
    }
    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        whitearw.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
                intent.putExtra("medicationFragment", "medicationFragment");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });


    }

}
