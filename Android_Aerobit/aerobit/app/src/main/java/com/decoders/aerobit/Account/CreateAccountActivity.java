package com.decoders.aerobit.Account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.util.AppStatus;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.IntroScreen.IntroScreenActivity;
import com.decoders.aerobit.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class CreateAccountActivity extends AppCompatActivity {

    @BindView(R.id.rbtnbt)
    public Button btnss;
    public LinearLayout backbtn;
    @BindView(R.id.email)
    public EditText mEmail;
    @BindView(R.id.etPassword)
    public EditText mPassword;
    @BindView(R.id.TxCA)
    public TextView bottomtext1;
    @BindView(R.id.TxCA1)
    public TextView bottomtext2;
    @BindView(R.id.normaleye)
    public ImageView Normaleye;
    @BindView(R.id.hideeye)
    public ImageView Hideeye;
    @BindView(R.id.etemailLayout)
    public TextInputLayout etemailLayout;
    @BindView(R.id.etPasswordLayout)
    public TextInputLayout etPasswordLayout;
    public ImageView normaleye,hideeye;
    @BindView(R.id.create_progress_bar)
    public ProgressBar create_progress_bar;
    public Typeface font, font2;
    private Toolbar mToolbar;
    public RelativeLayout mBackNavigation,parentRelative;
    public TextView toolbarText;
    private static final String INVALID_EMAIL="Invalid Email ";
    private static final String PASSWORD_MIN_CAHRS="Password should be above 6 letters";
    private PrefManager prefManager;

   // private Bundle userBundle,userInfoBundle,genderBundle;
    private static final String PERSON_REF_DATA="person";

    private Person person ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        initViews();
        toolbarInfo();
        hideSoftKeyboard();
        prefManager=PrefManager.getInstance();

        create_progress_bar=(ProgressBar)findViewById(R.id.create_progress_bar);
        mEmail=(EditText)findViewById(R.id.email);
        mPassword=(EditText)findViewById(R.id.etPassword);
        bottomtext1=(TextView)findViewById(R.id.TxCA);
        bottomtext2=(TextView)findViewById(R.id.TxCA1);
        btnss = (Button) findViewById(R.id.rbtnbt);
        Normaleye = (ImageView)findViewById(R.id.normaleye);
        Hideeye = (ImageView)findViewById(R.id.hideeye);
        etemailLayout=(TextInputLayout)findViewById(R.id.etemailLayout);
        etPasswordLayout=(TextInputLayout)findViewById(R.id.etPasswordLayout);
        Normaleye.setVisibility(View.VISIBLE);
        
        userData();

        font = Typeface.createFromAsset(this.getAssets(),"Font/Avenir Heavy.otf");
        font2 = Typeface.createFromAsset(this.getAssets(),"Font/Avenir Roman.otf");

        Bundle b = getIntent().getExtras();

        if (b != null) {
            //back  button
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
           if(person!=null){
                mEmail.setText(person.getEmail());
                mPassword.setText(person.getPassword());
            }

        } else{
            // default flows - this is the only place new person should be there
            person = new Person();
       }
        btnss.setTypeface(font2);
        bottomtext1.setTypeface(font2);
        bottomtext2.setTypeface(font2);

        Normaleye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Normaleye.setVisibility(View.INVISIBLE);
                Hideeye.setVisibility(View.VISIBLE);
                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });
        Hideeye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hideeye.setVisibility(View.GONE);
                Normaleye.setVisibility(View.VISIBLE);
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });


        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                etemailLayout.setError(null);
                mEmail.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etemailLayout.setError(null);
            }
        });
        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etPasswordLayout.setError(null);
                Normaleye.setVisibility(View.VISIBLE);
            }
        });

        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    validation();
                }
                return false;
            }
        });


        btnss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();

            }
        });


        //Initializing user entered data
        userData();

    }
    private void validation(){
        final String email = mEmail.getText().toString();
        // check for connectivity
        if (AppStatus.getInstance(this).isOnline()) {
            //Toast.makeText(this,"You are online!!!!",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"You are NOT online!!!!",Toast.LENGTH_LONG).show();
            return;
        }
        if (!isValidEmail(email)) {
            etemailLayout.setError(INVALID_EMAIL);
        }
        final String pass = mPassword.getText().toString();
        if (!isValidPassword(pass)) {
            etPasswordLayout.setError(PASSWORD_MIN_CAHRS);
            Normaleye.setVisibility(View.INVISIBLE);
        }
        if (isValidEmail(email) && isValidPassword(pass)) {

            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("email",mEmail.getText().toString());
                CheckEmail(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private void initViews(){
        mToolbar=(Toolbar)findViewById(R.id.createAccountToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);

    }
    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Create Account");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CreateAccountActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), IntroScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  getBundle();
    }

    private boolean isValidEmail(String email) {
      //  String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z0-9]+\\.+[a-z]+";
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }



    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }
    public void userData() {
        if(!mEmail.equals("")) {
            mEmail.setFocusable(true);

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void CheckEmail(JSONObject objEmail){
        create_progress_bar.setVisibility(View.VISIBLE);
        btnss.getBackground().setAlpha(100);
        boolean status = PersonLoadDataService.getInstance().emailCheck(getApplicationContext(), mEmail.getText().toString());
        if(!status){
            etemailLayout.setError("Email Already Exists");
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            mEmail.setCompoundDrawables(null, null, dr, null);
            create_progress_bar.setVisibility(View.INVISIBLE);
        } else {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
            Intent intent = new Intent(getApplicationContext(), BirthyearAccountActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            person.setEmail(mEmail.getText().toString());
            person.setPassword(mPassword.getText().toString());
            intent.putExtra(PERSON_REF_DATA, person);
            startActivityForResult(intent,1);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle b = data.getExtras();
                if (b != null) {
                    Person person = (Person) b.getSerializable(PERSON_REF_DATA);
                    System.out.println("Name : " + person.getEmail());
                }
            } else if (resultCode == 0) {
                /*System.out.println("RESULT CANCELLED");
                Bundle b = data.getExtras();
                if (b != null) {
                    Person person = (Person) b.getSerializable(PERSON_REF_DATA);
                    System.out.println("Name : " + person.getEmail());
                }*/
            }
        }

    }
    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

}

