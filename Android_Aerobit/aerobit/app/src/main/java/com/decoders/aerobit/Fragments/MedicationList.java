package com.decoders.aerobit.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.service.local.CarePlanService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.Account.ForgotPasswordActivity;
import com.decoders.aerobit.Adapter.MedicationListAdapter;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.MedicationScreen.SelectMedicationTypeActivity;
import com.decoders.aerobit.R;
import com.decoders.aerobit.CustomComponents.CustomBottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by 10DECODERS on 2/19/2018.
 */

public class MedicationList extends Fragment implements CustomBottomSheetDialogFragment.OnDissmissListener
,CustomBottomSheetDialogFragment.OnDeleteListener{

    private List<CarePlan> carePlanInhalerList = new ArrayList<CarePlan>();
    private List<CarePlan> carePlanTabletList = new ArrayList<CarePlan>();
    private List<CarePlan> carePlanSyrubList = new ArrayList<CarePlan>();
    static List<CarePlan> list = new ArrayList<CarePlan>();

    private RecyclerView recyclerViewInhaler, recyclerViewTablet, recyclerViewSyrup;
    private MedicationListAdapter mInhalerAdapter, mSyrupAdapter, mTabletAdapter;
    TextView inhaler, tablet, syrubs;
    Realm mRealm;
    private ScrollView scrollView;
    private RelativeLayout relativeLayout;
    private Button addMedication;
    private ImageView iv_backroundscreen;
    private CarePlanService carePlanService;

    BottomSheetDialogFragment bottomSheetDialogFragment;
    BottomNavigation instance;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRealm = Realm.getDefaultInstance();

        recyclerViewInhaler = (RecyclerView) view.findViewById(R.id.inhalers);
        recyclerViewTablet = (RecyclerView) view.findViewById(R.id.tablest);
        recyclerViewSyrup = (RecyclerView) view.findViewById(R.id.syrups);
        scrollView = (ScrollView) view.findViewById(R.id.layoutMedicationList);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.layoutAddMedication);
        addMedication = (Button) view.findViewById(R.id.addMedication);
        iv_backroundscreen = (ImageView) view.findViewById(R.id.iv_backroundscreen);
        carePlanService = CarePlanService.getInstance();


        inhaler = (TextView) view.findViewById(R.id.test);
        tablet = (TextView) view.findViewById(R.id.txtTablet);
        syrubs = (TextView) view.findViewById(R.id.txtSy);

        getDataFromDB(getActivity().getApplicationContext());

        prepareData();
        loadInhalerData();
        loadTabletData();
        loadSyrupData();

        bottomSheetDialogFragment = new CustomBottomSheetDialogFragment();
        instance = BottomNavigation.getInstance();
        addMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMedicationTypeActivity();
            }
        });
    }

    private void openMedicationTypeActivity() {
        Intent intent = new Intent(getActivity(), SelectMedicationTypeActivity.class);
        intent.putExtra("UserID", PrefManager.getInstance().getCurrentUser(getActivity().getApplicationContext()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void getDataFromDB(Context context) {
        Person currentPerson = (Person) AerobitUtil.fromJson(PrefManager.getInstance().getCurrentUser(getActivity().getApplicationContext()), Person.class);
        try {
            list = CarePlanService.getInstance().getCarePlanByPatientId(context.getContentResolver(),
                    currentPerson.getId());
            if (list.isEmpty()) {
                relativeLayout.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.INVISIBLE);
            } else {
                scrollView.setVisibility(View.VISIBLE);
                relativeLayout.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return 0;
    }

    private void loadInhalerData() {
        if (carePlanInhalerList.size() > 0) {
            mInhalerAdapter = new MedicationListAdapter(carePlanInhalerList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewInhaler.setLayoutManager(mLayoutManager);
            recyclerViewInhaler.setItemAnimator(new DefaultItemAnimator());
            recyclerViewInhaler.setAdapter(mInhalerAdapter);
            mInhalerAdapter.notifyDataSetChanged();
        } else {
            inhaler.setVisibility(View.GONE);
            recyclerViewInhaler.setVisibility(View.GONE);
        }
    }

    private void loadTabletData() {
        if (carePlanTabletList.size() > 0) {
            mTabletAdapter = new MedicationListAdapter(carePlanTabletList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewTablet.setLayoutManager(mLayoutManager);
            recyclerViewTablet.setItemAnimator(new DefaultItemAnimator());
            recyclerViewTablet.setAdapter(mTabletAdapter);
            mTabletAdapter.notifyDataSetChanged();
        } else {
            tablet.setVisibility(View.GONE);
            recyclerViewTablet.setVisibility(View.GONE);
        }
    }

    private void loadSyrupData() {
        if (carePlanSyrubList.size() > 0) {
            mSyrupAdapter = new MedicationListAdapter(carePlanSyrubList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewSyrup.setLayoutManager(mLayoutManager);
            recyclerViewSyrup.setItemAnimator(new DefaultItemAnimator());
            recyclerViewSyrup.setAdapter(mSyrupAdapter);
            mSyrupAdapter.notifyDataSetChanged();
        } else {
            syrubs.setVisibility(View.GONE);
            recyclerViewSyrup.setVisibility(View.GONE);
        }
    }

    private void prepareData() {
        for (CarePlan mCarePlay : list) {
            if (mCarePlay.getMedication().getType().equals("INHALER")) {
             //   CarePlan carePlanModel = mCarePlay;
                carePlanInhalerList.add(mCarePlay);
            } else if (mCarePlay.getMedication().getType().equals("TABLET")) {
             //   CarePlan carePlanModel = mCarePlay;
                carePlanTabletList.add(mCarePlay);
            } else if (mCarePlay.getMedication().getType().equals("SYRUP")) {
             //   CarePlan carePlanModel = mCarePlay;
                carePlanSyrubList.add(mCarePlay);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_medication, container, false);
    }

    @Override
    public void doDissmiss(boolean dissmiss) {
        bottomSheetDialogFragment.dismiss();
    }

    @Override
    public void doShow(Bundle bundle) {
        try {
            bottomSheetDialogFragment = new CustomBottomSheetDialogFragment();
            bottomSheetDialogFragment.setArguments(bundle);
            bottomSheetDialogFragment.setTargetFragment(this, 0);
            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDeleteMedicine(String id) {
        showDeletePopup(id);
    }
    private void showDeletePopup(final String id){
        buildDialog(R.style.DialogTheme ,id);

    }

    // show pop up dialog

    private void buildDialog(int animationSource, final String id) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure you want to delete this medication?");
                alertDialogBuilder.setPositiveButton("Delete",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                deleteMedication(id);
                                Toast.makeText(getActivity(),"Successfully deleted",Toast.LENGTH_LONG).show();
                            }
                        });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = animationSource;
        alertDialog.show();
    }

    //Delete medication

    public void deleteMedication(final String id) {
        carePlanService.deactivateCarePlan(getActivity().getContentResolver(), id);
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(R.id.medicationFragment, new MedicationList());
        tr.commit();
    }
}
