package com.decoders.aerobit.Family;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.decoders.aerobit.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddFamilyNameActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btns;
    private EditText mFirstname,mLastname,mBirthyear;
    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;
    private TextInputLayout FirstnameLayout,LastnameLayout,DOBLayout;

    int num1;
    boolean firstname ;
    boolean lastname;
    boolean birth;
    //private Bundle userInfoBundle,genderBundle;

    private static final String PERSON_REF_DATA="person";
    private static final String FAMILY_REF_DATA="familyMember";
    private static final String FIRST_NAME_BLANK="First name can not be blank";
    private static final String LAST_NAME_BLANK="Last name can not be blank";
    private static final String YEAR_INCORRECT_MSG="Invalid year";
    private Person person;

    private Person familyMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addfamily_name);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            System.out.println("I am restoring the person");
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
        }
        // dont initialize objects like this - make it inside bundle intent

        initViews();
        toolbarInfo();
        getBundle(getIntent());
        hideSoftKeyboard();

        mFirstname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                FirstnameLayout.setError(null);
                mFirstname.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                FirstnameLayout.setError(null);
            }
        });
        mLastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                LastnameLayout.setError(null);
                mLastname.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                LastnameLayout.setError(null);

            }
        });
        mBirthyear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                DOBLayout.setError(null);
                mBirthyear.setCompoundDrawables(null,null,null,null);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                DOBLayout.setError(null);
            }
        });

    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(getApplicationContext(), FamilyMemberActivity.class);
        intent.putExtra(FAMILY_REF_DATA, familyMember);
        intent.putExtra(PERSON_REF_DATA, person);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);*/
        super.onBackPressed();
    }
    public static InputFilter getEditTextFilter() {
        return new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
                Matcher ms = ps.matcher(String.valueOf(c));
                return ms.matches();
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rbtn05:
            {
                if (!mFirstname.getText().toString().equals("")) {
                    firstname = true;
                }
                else
                {
                    firstname = false;
                    FirstnameLayout.setError(FIRST_NAME_BLANK);
                    Drawable dr = getResources().getDrawable(R.drawable.redicon);
                    //add an error icon to yur drawable files
                    dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                    mFirstname.setCompoundDrawables(null, null, dr, null);
                }
                if (!mLastname.getText().toString().equals("")) {
                    lastname = true;
                }
                else
                {
                    lastname = false;
                    LastnameLayout.setError(LAST_NAME_BLANK);
                    Drawable dr = getResources().getDrawable(R.drawable.redicon);
                    //add an error icon to yur drawable files
                    dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                    mLastname.setCompoundDrawables(null, null, dr, null);
                }
                if (!mBirthyear.getText().toString().equals("")) {
                    birth = true;
                }
                else
                {
                    birth = false;
                    DOBLayout.setError(YEAR_INCORRECT_MSG);
                    Drawable dr = getResources().getDrawable(R.drawable.redicon);
                    //add an error icon to yur drawable files
                    dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                    mBirthyear.setCompoundDrawables(null, null, dr, null);

                }
                if(firstname && lastname && birth) {
                    if (mBirthyear.getText().length() > 0) {
                        int num = Integer.parseInt(mBirthyear.getText().toString());
                        if (num >= 1900 && num <= 2018) {

                            //save the number
                            num1 = num;
                            //Creating Bundle

                            nextButton();

                        }
                        else {
                            Toast.makeText(AddFamilyNameActivity.this, "Range should be 1900-2018", Toast.LENGTH_SHORT).show();
                            mBirthyear.setText("");
                            num1 = -1;
                            DOBLayout.setError(YEAR_INCORRECT_MSG);
                            Drawable dr = getResources().getDrawable(R.drawable.redicon);
                            //add an error icon to yur drawable files
                            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                            mBirthyear.setCompoundDrawables(null, null, dr, null);
                        }
                    }
                }

                break;
            }

        }

    }

    private void initViews(){
        mFirstname=(EditText)findViewById(R.id.edtyr1);
        mLastname=(EditText)findViewById(R.id.edtyr2);
        mBirthyear=(EditText)findViewById(R.id.edtyr3);
        btns = (Button) findViewById(R.id.rbtn05);
        btns.setOnClickListener(this);
        mFirstname.setFilters(new InputFilter[]{getEditTextFilter()});
        mLastname.setFilters(new InputFilter[]{getEditTextFilter()});
        mToolbar=(Toolbar)findViewById(R.id.familyNameToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        FirstnameLayout  = (TextInputLayout)findViewById(R.id.FirstnameLayout);
        LastnameLayout = (TextInputLayout)findViewById(R.id.LastnameLayout);
        DOBLayout = (TextInputLayout)findViewById(R.id.DOBLayout);
        parentRelative.setVisibility(View.VISIBLE);

    }
    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Add Family Member");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                AddFamilyNameActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });
        mBirthyear.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){

                    saveButton(v);

                    return true;
                }
                return false;
            }
        });
    }
    public void saveButton(View v) {
        nextButton();
    }
    public void nextButton(){
        if (!mFirstname.getText().toString().equals("")) {
            firstname = true;
        }
        else
        {
            firstname = false;
            FirstnameLayout.setError(FIRST_NAME_BLANK);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            mFirstname.setCompoundDrawables(null, null, dr, null);
        }
        if (!mLastname.getText().toString().equals("")) {
            lastname = true;
        }
        else
        {
            lastname = false;
            LastnameLayout.setError(LAST_NAME_BLANK);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            mLastname.setCompoundDrawables(null, null, dr, null);

        }
        if (!mBirthyear.getText().toString().equals("")) {
            birth = true;
        }
        else
        {
            birth = false;
            DOBLayout.setError(YEAR_INCORRECT_MSG);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            mBirthyear.setCompoundDrawables(null, null, dr, null);

        }
        if(firstname && lastname && birth) {
            if (mBirthyear.getText().length() > 0) {
                int num = Integer.parseInt(mBirthyear.getText().toString());
                if (num >= 1900 && num <= 2018) {

                    //save the number
                    num1 = num;
                    //Creating Bundle
                    familyMember.setFirstName(mFirstname.getText().toString());
                    familyMember.setLastName(mLastname.getText().toString());
                    familyMember.setBirthDate(mBirthyear.getText().toString());

                    Intent intent = new Intent(getApplicationContext(), AddFamilyGenderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("gender", "empty");
                    intent.putExtra("familyMember", familyMember);
                    intent.putExtra(FAMILY_REF_DATA, familyMember);
                    intent.putExtra(PERSON_REF_DATA, person);
                    startActivityForResult(intent,1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                else {
                    Toast.makeText(AddFamilyNameActivity.this, "Range should be 1900-2018", Toast.LENGTH_SHORT).show();
                    mBirthyear.setText("");
                    num1 = -1;
                    DOBLayout.setError(YEAR_INCORRECT_MSG);
                    Drawable dr = getResources().getDrawable(R.drawable.redicon);
                    //add an error icon to yur drawable files
                    dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                    mBirthyear.setCompoundDrawables(null, null, dr, null);
                }
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK) {
            Bundle b = data.getExtras();
            if (b != null) {
                familyMember = (Person) b.getSerializable(FAMILY_REF_DATA);
                person = (Person) b.getSerializable(PERSON_REF_DATA);
                //System.out.println("email : " + "name "+familyMember.getFirstName());
            }
            getBundle(data);
        }
    }
    void getBundle(Intent intent){
        familyMember = (Person) getIntent().getExtras()
                .getSerializable(FAMILY_REF_DATA);
        if(familyMember!=null){
            Log.v("getBundle","-------------->"+"userInfoBundle");
            mFirstname.setText(familyMember.getFirstName());
            mLastname.setText(familyMember.getLastName());
            mBirthyear.setText(familyMember.getBirthDate());
        } else{
            familyMember = new Person();
        }
    }
    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

}
