package com.decoders.aerobit.MedicationScreen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;


//import com.decoders.aerobit.Authenticator.AccountGeneral;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.Fragments.DrawerFragment;
import com.decoders.aerobit.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class MedicationActivity extends AppCompatActivity{
    @BindView(R.id.dashboardbtn)
    ImageView dashboardbtn;
    @BindView(R.id.medicationbtn)
    ImageView medicationbtn;
    @BindView(R.id.addbtn)
    ImageView addbtn;
    @BindView(R.id.updatebtn)
    ImageView updatebtn;
    @BindView(R.id.morebtn)
    ImageView morebtn;

  //  @BindView(R.id.spinner)
  //  Spinner spinner;
    @BindView(R.id.addmedicationbtn)
    Button addmedication;

    @BindView(R.id.SpinnerNames)
    Spinner spinner;

    String[] username = {"Julie","Dinesh"};
    int[] userimage = {R.drawable.female, R.drawable.male};

    private Toolbar mToolbar;

    Realm mRelam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication);


        ButterKnife.bind(this);
        mToolbar=(Toolbar)findViewById(R.id.tool_med);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        mRelam=Realm.getDefaultInstance();

          addmedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SelectMedicationTypeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        loadFragment(new DrawerFragment());
        dashboardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashboardbtn.setImageResource(R.drawable.selectdashboard);

                medicationbtn.setImageResource(R.drawable.medication);
                updatebtn.setImageResource(R.drawable.updates);
                morebtn.setImageResource(R.drawable.more);
            }
        });
        medicationbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              medicationbtn.setImageResource(R.drawable.selectmedication);

                dashboardbtn.setImageResource(R.drawable.dashboard);
              updatebtn.setImageResource(R.drawable.updates);
                morebtn.setImageResource(R.drawable.more);
            }
        });
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            updatebtn.setImageResource(R.drawable.selectupdate);

                dashboardbtn.setImageResource(R.drawable.dashboard);
                medicationbtn.setImageResource(R.drawable.medication);
                morebtn.setImageResource(R.drawable.more);
            }
        });
        morebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           morebtn.setImageResource(R.drawable.selectmore);

                dashboardbtn.setImageResource(R.drawable.dashboard);
                medicationbtn.setImageResource(R.drawable.medication);
                updatebtn.setImageResource(R.drawable.updates);
            }
        });

//        AccountGeneral.createSyncAccount(this);

        // Perform a manual sync by calling this:
        //SyncAdapter.performSync();
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frame_navigation_medication, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
