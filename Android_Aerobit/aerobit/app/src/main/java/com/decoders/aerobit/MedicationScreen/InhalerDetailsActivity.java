package com.decoders.aerobit.MedicationScreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.SyncStatus;
import com.aerobit.medapp.model.remote.Text;
import com.aerobit.medapp.service.local.CarePlanService;
import com.aerobit.medapp.service.local.MedicationService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.Adapter.AutocompleteCustomArrayAdapter;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.R;

import com.decoders.aerobit.CustomComponents.CustomAutoCompleteTextChangedListener;
import com.decoders.aerobit.CustomComponents.CustomAutoCompleteView;
import com.decoders.aerobit.CustomComponents.CustomListAdapter;
import com.decoders.aerobit.CustomComponents.NonScrollListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class InhalerDetailsActivity extends AppCompatActivity implements AutocompleteCustomArrayAdapter.AutoListAdapterListener {

    @BindView(R.id.inhalerIcon)
    ImageView inhalerImage;

    @BindView(R.id.firstBox)
    RelativeLayout layoutFirstBox;

    @BindView(R.id.secondBox)
    RelativeLayout layoutSecondBox;

    @BindView(R.id.thirdBox)
    RelativeLayout layoutThirdBox;

    @BindView(R.id.nameAutoComplete)
    public CustomAutoCompleteView medicineNameAutoComplete;

    @BindView(R.id.scheduledDoseButton)
    Button scheduledDoseButton;

    @BindView(R.id.shceduleAnotherDose)
    Button scheduleAnotherDose;

    @BindView(R.id.selectedTimeList)
    NonScrollListView nonScrollListView;

    @BindView(R.id.asNeededButton)
    Button asNeededButton;

    @BindView(R.id.createMedicationScheduled)
    Button createMedication;

    @BindView(R.id.toolbarleft)
    ImageView backButton;

    @BindView(R.id.createMedicationAsNeeded)
    Button createMedicationAsNeeded;

    @BindView(R.id.editSaveButton)
    Button editSaveButton;

    @BindView(R.id.toolbarClose)
    ImageView toolBarClose;

    @BindView(R.id.inhalerName2)
    TextView selectedMedicineType;

    @BindView(R.id.clearMedicineName)
    ImageView clearButton;

    @BindView(R.id.removeMedicationButton)
    TextView removeMedication;
    TextView editMedText;

    public List<Medication> mMedicineList = new ArrayList<Medication>();
    private Bundle extras;
    private boolean asNeeded;
    private String activityName;
    public String type;

    public ArrayList<String> scheduledTimes = null;//new ArrayList<String>();


    private CarePlanService carePlanService;
    private MedicationService medicationService;

    public ArrayAdapter<Medication> customAutoCompleteAdapter;
    public CustomListAdapter adapter;

    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation, parentRelative;

    private static InhalerDetailsActivity instance = null;
    SelectedMedicine currentMedicine = null;

    public InhalerDetailsActivity() {
        instance = this;
    }

    public ArrayList<String> getScheduledTimes() {
        return  scheduledTimes;
    }


    public static synchronized InhalerDetailsActivity getInstance() {
        if (instance == null) {
            instance = new InhalerDetailsActivity();
        }
        return InhalerDetailsActivity.instance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inhaler_details);

        ButterKnife.bind(this);

        InitiVews();

        extras = getIntent().getExtras();

        medicationService = MedicationService.getInstance();
        carePlanService = CarePlanService.getInstance();
        if(currentMedicine == null) {
            currentMedicine = new SelectedMedicine();
        }

        if(scheduledTimes == null) {
            scheduledTimes = new ArrayList<String>();
        }
        carePlan = new CarePlan();
        //Get data for autocomplete
        selectMedicineType();
        toolbarInfo();

        //When user clicks scheduled dose
        scheduledDose();

    }


    /**
     * @param
     * @return void
     * User can type and select the medicines
     */
    private void selectMedicineType() {
        layoutFirstBox.setVisibility(View.VISIBLE);
        layoutSecondBox.setVisibility(View.INVISIBLE);
        layoutThirdBox.setVisibility(View.INVISIBLE);

        type = extras.getString("type");
        activityName = extras.getString("activityName");
        String getCarePlan = extras.getString("carePlan");

        if(getCarePlan != null) {
            carePlan = (CarePlan) AerobitUtil.fromJson(getCarePlan, CarePlan.class);
            type = carePlan.getMedication().getType();
            if (activityName != null && activityName.equals("editMedication")) {
                if(carePlan.getMedication().getType().equals("TABLET")) {
                    medicineNameAutoComplete.setText(carePlan.getMedication().getName()
                            + " " + carePlan.getMedication().getStrength());
                } else {
                    medicineNameAutoComplete.setText(carePlan.getMedication().getName());
                }
                for(String item : carePlan.getDosages()) {
                    getScheduledTimes().add(item);
                }
              //  Log.d("Time", "Schedule-->" + scheduledTimes.toString());
                notifyTimeAdapterChanged();
            }
           /* if(carePlan.getDosages()!=null){
                for(String item : carePlan.getDosages()) {
                    getScheduledTimes().add(item);
                }
            }*/
          //  notifyTimeAdapterChanged();
            showAllBoxes();
        }

        if (type.equals("INHALER")) {
            inhalerImage.setImageResource(R.drawable.medicationinhalers);
            selectedMedicineType.setText("inhaler ?");
        } else if (type.equals("TABLET")) {
            inhalerImage.setImageResource(R.drawable.tablets);
            selectedMedicineType.setText("tablet ?");
        } else if (type.equals("SYRUP")) {
            inhalerImage.setImageResource(R.drawable.syrup);
            selectedMedicineType.setText("syrup ?");
        }

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearScreen();
            }
        });
        addMedicationToList(type);
    }

    private void clearScreen() {
        getScheduledTimes().clear();
        createMedicationAsNeeded.setVisibility(View.GONE);
        createMedication.setVisibility(View.GONE);
        layoutSecondBox.setVisibility(View.GONE);
        layoutSecondBox.setVisibility(View.GONE);
        layoutThirdBox.setVisibility(View.GONE);
        medicineNameAutoComplete.setText("");
    }

    private void showAllBoxes() {
        layoutFirstBox.setVisibility(View.VISIBLE);
        layoutSecondBox.setVisibility(View.VISIBLE);
        if(getScheduledTimes().isEmpty()) {
            layoutThirdBox.setVisibility(View.INVISIBLE);
            createMedicationAsNeeded.setVisibility(View.VISIBLE);
            createMedication.setVisibility(View.INVISIBLE);
        } else {
            layoutThirdBox.setVisibility(View.VISIBLE);
            createMedication.setVisibility(View.VISIBLE);
            createMedicationAsNeeded.setVisibility(View.INVISIBLE);
        }

    }

    private void addMedicationToList(String type) {
        //TODO: need change
        List<Medication> medicationByType = medicationService.getMedicationByType(type);
        mMedicineList = medicationByType;
        medicineNameAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));
        customAutoCompleteAdapter = new AutocompleteCustomArrayAdapter(this,
                R.layout.custom_autocomplete_textview, mMedicineList, this);

        medicineNameAutoComplete.setThreshold(1);
        medicineNameAutoComplete.setAdapter(customAutoCompleteAdapter);
    }

    /**
     * @param
     * @return void
     * When user clicks on schedule dose
     */
    private void scheduledDose() {

        if(activityName != null && !activityName.equals("editMedication") && carePlan.getDosages() != null)  {
            for(String time : carePlan.getDosages()) {
                //scheduledTimes.add(time);
                getScheduledTimes().add(time);
            }
        }

        asNeededButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asNeeded = true;
                highLightAsNeededButton();
                layoutFirstBox.setVisibility(View.VISIBLE);
                layoutSecondBox.setVisibility(View.VISIBLE);
                layoutThirdBox.setVisibility(View.GONE);
                createMedication.setVisibility(View.INVISIBLE);
                createMedicationAsNeeded.setVisibility(View.VISIBLE);
            }
        });

        scheduledDoseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleDoseButtonClick();
            }
        });

        if (activityName != null && activityName.equals("doseTime")) {
            if(carePlan.getMedication().getType().equals("TABLET")) {
                medicineNameAutoComplete.setText(carePlan.getMedication().getName()
                        + " " + carePlan.getMedication().getStrength());
            } else {
                medicineNameAutoComplete.setText(carePlan.getMedication().getName());
            }
            medicineNameAutoComplete.dismissDropDown();
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //highlight the selected button
            highLightDoseButton();
            layoutThirdBox.setVisibility(View.VISIBLE);

            notifyTimeAdapterChanged();
        } else {
            //Open keyboard when edit text is empty
            if(medicineNameAutoComplete.getText().toString().equals("")) {
                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE))
                        .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        }

        createMedicationAsNeeded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveMedication();
                final String activityName = "medicationFragment";
                Intent showMedicationActivity = new Intent(getApplication(), BottomNavigation.class);
                showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                showMedicationActivity.putExtra("medicationFragment", activityName);
                startActivity(showMedicationActivity);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        createMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveMedication();
            }
        });


        scheduleAnotherDose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent doseTimeActivity = new Intent(InhalerDetailsActivity.this, DoseTimeActivity.class);
                doseTimeActivity.putExtra("carePlan", AerobitUtil.toJson(getCarePlan()));
              //  doseTimeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(doseTimeActivity);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    private void highLightAsNeededButton() {
        scheduledDoseButton.setBackgroundResource(R.drawable.button_radius_blur_left);
        asNeededButton.setBackgroundResource(R.drawable.button_shape_white_grey);
    }

    private void notifyTimeAdapterChanged() {
        if (getScheduledTimes() != null && !getScheduledTimes().isEmpty()){
            highLightDoseButton();
            adapter = new CustomListAdapter(this, removeDuplicates(getScheduledTimes()));
            nonScrollListView.setAdapter(adapter);
        } else {
            highLightAsNeededButton();
        }
    }

    private void scheduleDoseButtonClick() {
        asNeeded = false;
        createMedicationAsNeeded.setVisibility(View.INVISIBLE);
        highLightDoseButton();

        if(getScheduledTimes().isEmpty()) {
            Intent doseTimeActivity = new Intent(getApplication(), DoseTimeActivity.class);
            doseTimeActivity.putExtra("carePlan", AerobitUtil.toJson(getCarePlan()));
            startActivity(doseTimeActivity);

            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else {
            layoutThirdBox.setVisibility(View.VISIBLE);
            createMedication.setVisibility(View.VISIBLE);
        }
    }
    private void highLightDoseButton() {
        //highlight the selected button
        asNeededButton.setBackgroundResource(R.drawable.button_shape_white);
        scheduledDoseButton.setBackgroundResource(R.drawable.button_radius_blur_right_grey);
    }
    private CarePlan carePlan = null;//new CarePlan(); create or edit
    private CarePlan getCarePlan() {
        Person person = getCurrentPersonObject();
        Medication med = currentMedicine.getMedication();
        if(med == null) {
            med = carePlan.getMedication();
        }

        RealmList<String> list = new RealmList<String>();
        if(getScheduledTimes() != null) {
            for (String time : removeDuplicates(getScheduledTimes())) {
                list.add(time);
            }
        }
        if(asNeeded) {
            list.clear();
        }
        carePlan.setMedication(med);
        carePlan.setPerformer(person);
        carePlan.setSyncStatus(SyncStatus.NOT_SYNCED.getValue());
        carePlan.setDosages(list);
        carePlan.setActive(true);
        carePlan.setStrength(med.getStrength());
        return carePlan;
    }
    private void saveMedication() {
        try {
            if (!getScheduledTimes().isEmpty() || asNeeded == true) {
                CarePlan carePlan = getCarePlan();
                if(carePlan.getId() != null) {
                    carePlanService.deactivateCarePlan(getContentResolver(), carePlan.getId());
                    carePlan.setId(null);
                }
                carePlanService.createCarePlan(getContentResolver(), carePlan);
                final String activityName = "medicationFragment";
                Intent showMedicationActivity = new Intent(getApplication(), BottomNavigation.class);
                showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                showMedicationActivity.putExtra("medicationFragment", activityName);
                showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(showMedicationActivity);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            } else {
                Toast.makeText(instance, "Please select atleas one time.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(instance, "Problem in saving the medication.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Nullable
    private Person getCurrentPersonObject() {
        String currentUser = PrefManager.getInstance().getCurrentUser(getApplicationContext());
        return (Person) AerobitUtil.fromJson(currentUser, Person.class);
    }

    private void sortTimeArray(List<String> scheduledTimes) {
        Collections.sort(scheduledTimes, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                try {
                    return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
                } catch (ParseException e) {
                    return 0;
                }
            }
        });
    }

    private void InitiVews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbarWhite);
        mBackNavigation = (RelativeLayout) findViewById(R.id.navBackbutton);
        parentRelative = (RelativeLayout) findViewById(R.id.relativeBack);
        editMedText=(TextView)findViewById(R.id.editMedText);
        parentRelative.setVisibility(View.VISIBLE);
    }

    private void toolbarInfo() {
        if(activityName!=null&&activityName.equals("editMedication")) {
            hideSoftKeyboard();
            Log.v("Sample","editMedication");
            mToolbar.setTitle("");
            editMedText.setVisibility(View.VISIBLE);
            editMedText.setText("Edit Medication");
            mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final String activityName = "medicationFragment";
                    Log.v("activity name",activityName);
                    Intent showMedicationActivity = new Intent(getApplication(), BottomNavigation.class);
                    showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    showMedicationActivity.putExtra("medicationFragment", activityName);
                    showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(showMedicationActivity);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                    return false;
                }
            });
        }else
        {
            mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    startSelectMedicationActivity();
                    return false;
                }
            });
            mToolbar.setTitle("");
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(activityName!=null&&activityName.equals("editMedication")) {
                    final String activityName = "medicationFragment";
                    Log.v("activity name",activityName);
                    Intent showMedicationActivity = new Intent(getApplication(), BottomNavigation.class);
                    showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    showMedicationActivity.putExtra("medicationFragment", activityName);
                    showMedicationActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(showMedicationActivity);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }else
        {
            startSelectMedicationActivity();
        }

    }

    private void startSelectMedicationActivity() {
        Intent startMedicationTypeActivity = new Intent(InhalerDetailsActivity.this, SelectMedicationTypeActivity.class);
        startMedicationTypeActivity.putExtra("UserID", getCurrentPersonObject().getId());
        startMedicationTypeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMedicationTypeActivity);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    public List<String> removeDuplicates(List<String> list) {
        sortTimeArray(list);
        // Store unique items in result.
        ArrayList<String> result = new ArrayList<>();

        // Record encountered Strings in HashSet.
        HashSet<String> set = new HashSet<>();

        // Loop over argument list.
        for (String item : list) {

            // If String is not in set, add it to the list and the set.
            if (!set.contains(item)) {
                result.add(item);
                set.add(item);
            }
        }
        return result;
    }

    @Override
    public void onReceiveUser(Medication medication) {
        currentMedicine.setMedication(medication);
        medicineNameAutoComplete.dismissDropDown();
        updateAutoComplete();
    }

    public void updateAutoComplete() {
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(medicineNameAutoComplete.getWindowToken(), 0);

        //TODO:need to change
        layoutSecondBox.setVisibility(View.VISIBLE);
        String selectedMedicineName = currentMedicine.getMedication().getName();
        String strength = currentMedicine.getMedication().getStrength();
        if (currentMedicine.getMedication().getType().equals("TABLET")) {
            medicineNameAutoComplete.setText(selectedMedicineName + " " + strength);
        } else {
            medicineNameAutoComplete.setText(selectedMedicineName);
            medicineNameAutoComplete.dismissDropDown();
        }
    }

    public class SelectedMedicine {
        private Medication medication;

        public Medication getMedication() {
            return medication;
        }

        public void setMedication(Medication medication) {
            this.medication = medication;
        }
    }

    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
}
