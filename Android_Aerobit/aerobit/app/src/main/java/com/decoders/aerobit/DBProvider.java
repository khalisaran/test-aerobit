package com.decoders.aerobit;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by crosssales on 2/12/2018.
 */

public class DBProvider extends ContentProvider {
    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

//    private static final String TAG = DBProvider.class.getSimpleName();
//
//    private static final int CLEANUP_JOB_ID = 43;
//    private static final int TASKS = 100;
//    private static final int TASKS_WITH_ID = 101;
//
//    private static final int FAMILY=104;
//    private static final int FAMILY_WITH_ID=105;
//
//    private static final int USER=106;
//    private static final int USER_WITH_ID=107;
//
//    private static final int CARE=108;
//    private static final int CARE_WITH_ID=109;
//
//    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
//    static {
//        // content://com.example.rgher.realmtodo/tasks
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
//                DatabaseContract.PATH_ARTICLES,
//                TASKS);
//
//        // content://com.example.rgher.realmtodo/tasks/id
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
//                DatabaseContract.PATH_ARTICLES + "/#",
//                TASKS_WITH_ID);
//
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_FAMILY,FAMILY);
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_FAMILY + "/#",FAMILY_WITH_ID);
//
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_USER,USER);
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_USER + "/#",USER_WITH_ID);
//
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_CAREPLAN,CARE);
//        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,DatabaseContract.PATH_CAREPLAN + "/#",CARE_WITH_ID);
//    }
//    Realm mRealm;
//
//
//    @Override
//    public boolean onCreate() {
//        mRealm= Realm.getDefaultInstance();
//        return false;
//    }
//
//    @Nullable
//    @Override
//    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
//
//        int match = sUriMatcher.match(uri);
//
//        //Get Realm Instance
//        mRealm = Realm.getDefaultInstance();
//        MatrixCursor myCursor=null;
//
//        try {
//            switch (match)
//            {
//                case TASKS:
//                    myCursor = new MatrixCursor( new String[]{DatabaseContract.TaskColumns.name
//                            ,DatabaseContract.TaskColumns.phone, DatabaseContract.TaskColumns.address
//
//                    });
//                    RealmResults<Student> mRealResult=mRealm.where(Student.class).findAll();
//                    for (Student myTask : mRealResult) {
//                        Object[] rowData = new Object[]{myTask.getName(), myTask.getPhone(), myTask.getAddress()};
//                        myCursor.addRow(rowData);
//                        Log.v("RealmDB", myTask.toString());
//                    }
//                    break;
//                case TASKS_WITH_ID:
//                    Integer id = Integer.parseInt(uri.getPathSegments().get(1));
//                    Student myTask = mRealm.where(Student.class).equalTo("task_id", id).findFirst();
//                    myCursor.addRow(new Object[]{myTask.getName(), myTask.getPhone(), myTask.getAddress()});
//                    Log.v("RealmDB", myTask.toString());
//                    break;
//                case  USER:
//                    myCursor=new MatrixCursor(new String[]{
//                            DatabaseContract.TaskColumns.U_ID
//                            ,DatabaseContract.TaskColumns.U_USER_ID
//                            ,DatabaseContract.TaskColumns.U_EMAIL
//                            ,DatabaseContract.TaskColumns.U_PASSWORD
//                            ,DatabaseContract.TaskColumns.U_FIRSTNAME
//                            ,DatabaseContract.TaskColumns.U_LASTNAME
//                            ,DatabaseContract.TaskColumns.U_BIRTHYEAR
//                            ,DatabaseContract.TaskColumns.U_GENDER
//                            ,DatabaseContract.TaskColumns.U_LASTSYNC
//                            ,DatabaseContract.TaskColumns.U_STATUS
//                            ,DatabaseContract.TaskColumns.U_THEME
//                    });
//                    RealmResults<User> UserResults=mRealm.where(User.class).findAll();
//                    for(User mUser:UserResults) {
//                        Object[] rowData=new Object[]{
//                                mUser.getId(),mUser.getUserid(),mUser.getEmail(),mUser.getPassword(),
//                                mUser.getFirstname(),mUser.getLastname(),mUser.getBirthyear(),mUser.getGender(),
//                                mUser.getLastSync(),mUser.getStatus(),mUser.getTheme()
//
//                        };
//                        myCursor.addRow(rowData);
//                        Log.v("UserDB",mUser.toString());
//                    }
//                    break;
//
//                case FAMILY:
//                    myCursor=new MatrixCursor(new String[]{
//                     DatabaseContract.TaskColumns._ID
//                    ,DatabaseContract.TaskColumns.FIRST_NAME
//                    ,DatabaseContract.TaskColumns.LAST_NAME
//                    ,DatabaseContract.TaskColumns.GENDER
//                    ,DatabaseContract.TaskColumns.BIRTH_YEAR
//                    ,DatabaseContract.TaskColumns.USER_ID
//                    ,DatabaseContract.TaskColumns.STATUS
//                    ,DatabaseContract.TaskColumns.LASTSYNC
//                    ,DatabaseContract.TaskColumns.UPDATETIME
//                    ,DatabaseContract.TaskColumns.EMAIL
//                    ,DatabaseContract.TaskColumns.ACTIVE
//                    ,DatabaseContract.TaskColumns.THEME
//                    });
//                    RealmResults<Family> realmResults=mRealm.where(Family.class).findAll();
//                    for(Family family:realmResults)
//                    {
//                        Object[] rowData=new Object[]{family.getId(),family.getFirstname(),family.getLastname(),
//                                family.getGender(),family.getBirthyear(),family.getUserID(),
//                                family.getStatus(),family.getLastSync(),family.getUpdateTime(),family.getEmail()
//                                ,family.isActive(),family.getTheme()
//                        };
//                        myCursor.addRow(rowData);
//                        Log.v("FamilyDb",family.toString());
//                    }
//
//                    break;
//                case CARE:
//                    myCursor=new MatrixCursor(new String[]{
//                            DatabaseContract.TaskColumns.M_ID
//                            ,DatabaseContract.TaskColumns.M_USERID
//                            ,DatabaseContract.TaskColumns.M_TYPE
//                            ,DatabaseContract.TaskColumns.M_NAME
//                            ,DatabaseContract.TaskColumns.M_STRENGTH
//                            ,DatabaseContract.TaskColumns.M_TIME
//                            ,DatabaseContract.TaskColumns.M_STATUs
//                            ,DatabaseContract.TaskColumns.M_LAST_SYNC
//                            ,DatabaseContract.TaskColumns.M_MED_ID
//
//                    });
//                    RealmResults<CarePlan1> mRealmResult=mRealm.where(CarePlan1.class).findAll();
//                    for(CarePlan1 mCarePlan1 :mRealmResult)
//                    {
//                        Object[] rowData=new Object[]{mCarePlan1.getId(), mCarePlan1.getUserid(), mCarePlan1.getType(),
//                                mCarePlan1.getName(), mCarePlan1.getStrength(), mCarePlan1.getTime(), mCarePlan1.getStatus()
//                                , mCarePlan1.getLastSync()
//                                , mCarePlan1.getMedID()
//
//                        };
//                        myCursor.addRow(rowData);
//                        Log.v("CarePlan1", mCarePlan1.toString());
//                    }
//                    break;
//                default:
//                    throw new UnsupportedOperationException("Unknown uri: " + uri);
//            }
//            myCursor.setNotificationUri(getContext().getContentResolver(), uri);
//        } finally {
//            mRealm.close();
//        }
//        return myCursor;
//    }
//
//    @Nullable
//    @Override
//    public String getType(@NonNull Uri uri) {
//
//        switch (sUriMatcher.match(uri)) {
//            case TASKS:
//                return DatabaseContract.TaskColumns.CONTENT_TYPE;
//
//            default: throw new IllegalArgumentException("Invalid URI!");
//        }
//
//    }
//
//    @Nullable
//    @Override
//    public Uri insert(@NonNull Uri uri, @Nullable final ContentValues contentValues) {
//        int match=sUriMatcher.match(uri);
//        Uri returnUri;
//        mRealm = Realm.getDefaultInstance();
//        try
//        {
//            switch (match)
//            {
//                case TASKS:
//                    mRealm.beginTransaction();
//                    Student myNewTask = new Student();
//                    myNewTask.setName(contentValues.get(DatabaseContract.TaskColumns.name).toString());
//                    myNewTask.setPhone(contentValues.get(DatabaseContract.TaskColumns.phone).toString());
//                    myNewTask.setAddress(contentValues.get(DatabaseContract.TaskColumns.address).toString());
//                    mRealm.copyToRealm(myNewTask);
//                    mRealm.commitTransaction();
//                    returnUri = ContentUris.withAppendedId(DatabaseContract.TaskColumns.CONTENT_URI, '1');
//                    break;
//
//                case FAMILY:
//                    mRealm.beginTransaction();
//                    Number currId = mRealm.where(Family.class).max(DatabaseContract.TaskColumns.ID);
//                    Integer nextId = (currId == null) ? 1 : currId.intValue() + 1;
//                    Family mFamily=new Family();
//                    mFamily.setId(nextId);
//                    mFamily.setUserID(contentValues.get(DatabaseContract.TaskColumns.USER_ID).toString());
//                    mFamily.setFirstname(contentValues.get(DatabaseContract.TaskColumns.FIRST_NAME).toString());
//                    mFamily.setLastname(contentValues.get(DatabaseContract.TaskColumns.LAST_NAME).toString());
//                    mFamily.setBirthyear(contentValues.get(DatabaseContract.TaskColumns.BIRTH_YEAR).toString());
//                    mFamily.setGender(contentValues.get(DatabaseContract.TaskColumns.GENDER).toString());
//                    mFamily.setStatus(contentValues.get(DatabaseContract.TaskColumns.STATUS).toString());
//                    mFamily.setUpdateTime(contentValues.get(DatabaseContract.TaskColumns.UPDATETIME).toString());
//                    mFamily.setLastSync(contentValues.get(DatabaseContract.TaskColumns.LASTSYNC).toString());
//                    mFamily.setEmail(contentValues.get(DatabaseContract.TaskColumns.EMAIL).toString());
//                    mFamily.setActive(contentValues.get(DatabaseContract.TaskColumns.ACTIVE).toString());
//                    mFamily.setTheme(contentValues.get(DatabaseContract.TaskColumns.THEME).toString());
//                    mRealm.copyToRealm(mFamily);
//                    mRealm.commitTransaction();
//                    returnUri=ContentUris.withAppendedId(DatabaseContract.TaskColumns.CONTENT_FAMILY,'1');
//                    Log.v("DBProvider","~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                    break;
//                case USER:
//                    mRealm.beginTransaction();
//                    Number currUserId = mRealm.where(User.class).max(DatabaseContract.TaskColumns.U_ID);
//                    Integer nextUserId = (currUserId == null) ? 1 : currUserId.intValue() + 1;
//                    User mUser=new User();
//                    mUser.setId(nextUserId);
//                    mUser.setUserid(contentValues.get(DatabaseContract.TaskColumns.U_USER_ID).toString());
//                    mUser.setEmail(contentValues.get(DatabaseContract.TaskColumns.U_EMAIL).toString());
//                    mUser.setPassword(contentValues.get(DatabaseContract.TaskColumns.U_PASSWORD).toString());
//                    mUser.setFirstname(contentValues.get(DatabaseContract.TaskColumns.U_FIRSTNAME).toString());
//                    mUser.setLastname(contentValues.get(DatabaseContract.TaskColumns.U_LASTNAME).toString());
//                    mUser.setBirthyear(contentValues.get(DatabaseContract.TaskColumns.U_BIRTHYEAR).toString());
//                    mUser.setGender(contentValues.get(DatabaseContract.TaskColumns.U_GENDER).toString());
//                    mUser.setStatus(contentValues.get(DatabaseContract.TaskColumns.U_STATUS).toString());
//                    mUser.setLastSync(contentValues.get(DatabaseContract.TaskColumns.U_LASTSYNC).toString());
//                    mUser.setTheme(contentValues.get(DatabaseContract.TaskColumns.U_THEME).toString());
//                    mRealm.copyToRealm(mUser);
//                    mRealm.commitTransaction();
//                    returnUri=ContentUris.withAppendedId(DatabaseContract.TaskColumns.CONTENT_USER,'1');
//                    break;
//                case CARE:
//                    /*mRealm.beginTransaction();
//                    Number currCareid=mRealm.where(CarePlan1.class).max(DatabaseContract.TaskColumns.M_ID);
//                    Integer nextCareId = (currCareid == null) ? 1 : currCareid.intValue() + 1;
//                    CarePlan1 mCarePlan1=new CarePlan1();
//                    mCarePlan1.setId(nextCareId);
//                    mCarePlan1.setUserid(contentValues.get(DatabaseContract.TaskColumns.M_USERID).toString());
//                    mCarePlan1.setMedID(contentValues.get(DatabaseContract.TaskColumns.M_MED_ID).toString());
//                    mCarePlan1.setType(contentValues.get(DatabaseContract.TaskColumns.M_TYPE).toString());
//                    mCarePlan1.setName(contentValues.get(DatabaseContract.TaskColumns.M_NAME).toString());
//                    mCarePlan1.setStrength(contentValues.get(DatabaseContract.TaskColumns.M_STRENGTH).toString());
//                    mCarePlan1.setStatus(contentValues.get(DatabaseContract.TaskColumns.M_STATUs).toString());
//                    mCarePlan1.setLastSync(contentValues.get(DatabaseContract.TaskColumns.M_LAST_SYNC).toString());
//                    mCarePlan1.setTime(contentValues.get(DatabaseContract.TaskColumns.M_TIME).toString());
//                    returnUri=ContentUris.withAppendedId(DatabaseContract.TaskColumns.CONTENT_CARE,'1');
//                    mRealm.copyToRealm(mCarePlan1);
//                    mRealm.commitTransaction();
//                    break;*/
//                    mRealm.beginTransaction();
//                    Number currCareid=mRealm.where(CarePlan1.class).max(DatabaseContract.TaskColumns.M_ID);
//                    String listString=contentValues.get(DatabaseContract.TaskColumns.M_TIME).toString();
//                    Integer nextCareId = (currCareid == null) ? 1 : currCareid.intValue() + 1;
//                    CarePlan1 mCarePlan1 =new CarePlan1();
//                    mCarePlan1.setId(nextCareId);
//                    List<String> timeList = new ArrayList<String>(Arrays.asList(listString.split(",")));
//                    Log.v("DB Provider list",timeList.toString());
//                    if(mCarePlan1.timeList == null){
//                        mCarePlan1.timeList = new RealmList<Link>();
//                    }
//                    for(int i=0;i<timeList.size();i++){
//                        Link mLink=mRealm.createObject(Link.class);
//                        mLink.time=timeList.get(i);
//                        mCarePlan1.timeList.add(mLink);
//                    }
//                    mCarePlan1.setUserid(contentValues.get(DatabaseContract.TaskColumns.M_USERID).toString());
//                    mCarePlan1.setMedID(contentValues.get(DatabaseContract.TaskColumns.M_MED_ID).toString());
//                    mCarePlan1.setType(contentValues.get(DatabaseContract.TaskColumns.M_TYPE).toString());
//                    mCarePlan1.setName(contentValues.get(DatabaseContract.TaskColumns.M_NAME).toString());
//                    mCarePlan1.setStrength(contentValues.get(DatabaseContract.TaskColumns.M_STRENGTH).toString());
//                    mCarePlan1.setStatus(contentValues.get(DatabaseContract.TaskColumns.M_STATUs).toString());
//                    mCarePlan1.setLastSync(contentValues.get(DatabaseContract.TaskColumns.M_LAST_SYNC).toString());
//                  //  mCarePlan1.setTime(contentValues.get(DatabaseContract.TaskColumns.M_TIME).toString());
//                    returnUri=ContentUris.withAppendedId(DatabaseContract.TaskColumns.CONTENT_CARE,'1');
//                    mRealm.copyToRealm(mCarePlan1);
//                    mRealm.commitTransaction();
//                    break;
//                default:
//                    throw new UnsupportedOperationException("Unknown uri: " + uri);
//            }
//
//        }finally {
//            mRealm.close();
//        }
//        getContext().getContentResolver().notifyChange(uri, null);
//        return uri;
//    }
//
//    @Override
//    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
//        return 0;
//    }
//
//    @Override
//    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
//        Realm realm = Realm.getDefaultInstance();
//
//        int match = sUriMatcher.match(uri);
//        int nrUpdated = 0;
//        try
//        {
//            switch (match) {
//                case FAMILY:
//
//                    String updateID=values.get(DatabaseContract.TaskColumns.ID).toString();
//                    Log.v("sss",updateID);
//                    int uID= Integer.parseInt(String.valueOf(updateID));
//                    Family myTask = realm.where(Family.class).equalTo("id", uID).findFirst();
//                    realm.beginTransaction();
//                    myTask.setUserID(values.get(DatabaseContract.TaskColumns.USER_ID).toString());
//                    myTask.setStatus(values.get(DatabaseContract.TaskColumns.STATUS).toString());
//                    myTask.setBirthyear(values.get(DatabaseContract.TaskColumns.BIRTH_YEAR).toString());
//                    myTask.setFirstname(values.get(DatabaseContract.TaskColumns.FIRST_NAME).toString());
//                    myTask.setLastname(values.get(DatabaseContract.TaskColumns.LAST_NAME).toString());
//                    myTask.setGender(values.get(DatabaseContract.TaskColumns.GENDER).toString());
////                    myTask.setTheme(values.get(DatabaseContract.TaskColumns.THEME).toString());
//                    myTask.setLastSync(values.get(DatabaseContract.TaskColumns.LASTSYNC).toString());
//                    myTask.setUpdateTime(values.get(DatabaseContract.TaskColumns.UPDATETIME).toString());
//                    myTask.setEmail(values.get(DatabaseContract.TaskColumns.EMAIL).toString());
//                    myTask.setActive(values.get(DatabaseContract.TaskColumns.ACTIVE).toString());
//                    nrUpdated++;
//                    realm.commitTransaction();
//                    Log.v("DBProvider","Provider");
//                    break;
//                case CARE:
//                    String update=values.get(DatabaseContract.TaskColumns.M_ID).toString();
//                    Log.v("Update ID",update);
//                    int Uid=Integer.parseInt(String.valueOf(update));
//
//                    CarePlan1 mCarePlan=realm.where(CarePlan1.class).equalTo("id",Uid).findFirst();
//                    realm.beginTransaction();
//
//                    String listString=values.get(DatabaseContract.TaskColumns.M_TIME).toString();
//
//                    Integer nextCareId = (update == null) ? 1 : Integer.valueOf(update) + 1;
//                    CarePlan1 mCarePlan11 =new CarePlan1();
//                    mCarePlan11.setId(nextCareId);
//
//                    List<String> timeList = new ArrayList<String>(Arrays.asList(listString.split(",")));
//                    Log.v("DB Provider list",timeList.toString());
//
//                    if(mCarePlan.timeList == null){
//                        mCarePlan.timeList = new RealmList<Link>();
//                    }
//                    for(int i=0;i<timeList.size();i++){
//                        Link mLink=mRealm.createObject(Link.class);
//                        mLink.time=timeList.get(i);
//                        mCarePlan.timeList.add(mLink);
//                    }
//
//                    mCarePlan.setUserid(values.get(DatabaseContract.TaskColumns.M_USERID).toString());
//                    mCarePlan.setMedID(values.get(DatabaseContract.TaskColumns.M_MED_ID).toString());
////                    mCarePlan.setTime(values.get(DatabaseContract.TaskColumns.M_TIME).toString());
//                    mCarePlan.setLastSync(values.get(DatabaseContract.TaskColumns.M_LAST_SYNC).toString());
//                    mCarePlan.setStatus(values.get(DatabaseContract.TaskColumns.M_STATUs).toString());
//                    mCarePlan.setName(values.get(DatabaseContract.TaskColumns.M_NAME).toString());
//                    mCarePlan.setStrength(values.get(DatabaseContract.TaskColumns.M_STRENGTH).toString());
//                    mCarePlan.setType(values.get(DatabaseContract.TaskColumns.M_TYPE).toString());
//                    nrUpdated++;
//                    realm.commitTransaction();
//                    Log.v("DBProvider","CarePlan1");
//                    break;
//
//                default:
//                    throw new UnsupportedOperationException("Unknown uri: " + uri);
//            }
//
//
//        } finally {
//            realm.close();
//        }
//        if (nrUpdated != 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
//        }
//
//
//        return nrUpdated;
//    }
}
