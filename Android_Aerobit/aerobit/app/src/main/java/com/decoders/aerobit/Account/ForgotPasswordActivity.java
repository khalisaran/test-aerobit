package com.decoders.aerobit.Account;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aerobit.medapp.model.response.ApiResponse;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.decoders.aerobit.R;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ForgotPasswordActivity extends AppCompatActivity {

    private LinearLayout btnpass;

    private Button button;
    private Context context = this;
    private PopupWindow mPopupWindow;
    private RelativeLayout mRelativeLayout;
    private ImageView iv_backroundscreen;
    private EditText forgetEmail;
    private ProgressDialog pDialog;
    private static final String TAG = ForgotPasswordActivity.class.getName();
    private TextView textforgot1, textforgot2, textforgot3;
    private Typeface font;

    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation, parentRelative;
    private TextView toolbarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);
        initViews();
        toolbarInfo();


        iv_backroundscreen = (ImageView) findViewById(R.id.iv_backroundscreen);
        forgetEmail = (EditText) findViewById(R.id.forgetEmail);
        textforgot1 = (TextView) findViewById(R.id.textforgot1);
        textforgot2 = (TextView) findViewById(R.id.textforgot2);
        textforgot3 = (TextView) findViewById(R.id.textforgot3);
        button = (Button) findViewById(R.id.btnforgot);

        font = Typeface.createFromAsset(this.getAssets(), "Font/Avenir Roman.otf");
        textforgot1.setTypeface(font);
        textforgot2.setTypeface(font);
        textforgot3.setTypeface(font);
        button.setTypeface(font);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = forgetEmail.getText().toString();
                if (!isValidEmail(email)) {
//                    Toast.makeText(getApplicationContext(), "Invalid Email", Toast.LENGTH_SHORT).show();
                } else {

                    JSONObject jsonObject = new JSONObject();
                    try {
                        APICALL(forgetEmail.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    public void Shadowpopup(boolean isVisible) {
        if (isVisible) {
            iv_backroundscreen.setVisibility(View.VISIBLE);
        } else {
            iv_backroundscreen.setVisibility(View.GONE);
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z0-9]+\\.+[a-z]+";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.createAccountToolbar);
        mBackNavigation = (RelativeLayout) findViewById(R.id.navBackbutton);
        parentRelative = (RelativeLayout) findViewById(R.id.relativeBack);
        toolbarText = (TextView) findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);

    }

    private void toolbarInfo() {
        mToolbar.setTitle("");
        toolbarText.setText("");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ForgotPasswordActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }


    public void APICALL(String email) {
        Log.v(TAG, "onCreate: " + email);

        pDialog = new ProgressDialog(ForgotPasswordActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        ApiResponse status = PersonLoadDataService.getInstance().forgetpassword(getApplicationContext(), email);
        Log.i("Forget Password", status.toString());
        String message = status.getMessage();
        if (!message.equals("") && !message.equals(null)) {
            if (message.equals("Email Doesnt Exist")) {
//                                    Toast.makeText(getApplicationContext(), "Email Doesnt Exist", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_login, null);
                TextView textView = mView.findViewById(R.id.txtalert1);
                textView.setText("");
                textView.setText("We sent an email to" + " " + forgetEmail.getText().toString());

                Button mLogin = (Button) mView.findViewById(R.id.btnLogin);
                mBuilder.setView(mView);
                mBuilder.setCancelable(false);
                Shadowpopup(false);
                if (pDialog.isShowing())
                    pDialog.dismiss();
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                mLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                });
            }
        }

    }
}


