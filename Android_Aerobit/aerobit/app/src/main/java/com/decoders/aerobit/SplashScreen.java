package com.decoders.aerobit;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.IntroScreen.IntroScreenActivity;

public class SplashScreen extends AppCompatActivity {
    Handler handler;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        prefManager=PrefManager.getInstance();
        handler = new Handler();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String xid = prefManager.getXid(getApplicationContext());
                if (xid.equals("")) {
                    Intent intent = new Intent(SplashScreen.this, IntroScreenActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                } else {
                    Intent intent = new Intent(SplashScreen.this, BottomNavigation.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            }
        }, 3000);
    }
}

