package com.decoders.aerobit.Account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aerobit.medapp.model.response.ApiResponse;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.decoders.aerobit.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PasswordResetActivity extends AppCompatActivity {
    Button btn_reset;
    EditText OldPassword, NewPassword;

    private ProgressDialog pDialog;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        btn_reset = (Button) findViewById(R.id.btn_reset);
        OldPassword = (EditText) findViewById(R.id.edit_reset_pswd);
        NewPassword = (EditText) findViewById(R.id.etPassword);

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("calling api", "");
                if (NewPassword.getText().toString().equals(OldPassword.getText().toString())) {
                    try {
                        APICALL(NewPassword.getText().toString().trim(), token);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
//                    Toast.makeText(getApplicationContext(), "Password mismatch", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void APICALL(String password, String token) {
        pDialog = new ProgressDialog(PasswordResetActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(true);
        pDialog.show();
        ApiResponse result = PersonLoadDataService.getInstance().resetpassword(getApplicationContext(), password, token);
        if (!result.equals(null)) {
            String msg = result.getMessage();
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (msg.equals("Success! Your password has been changed.")) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
                if (msg.equals("Password reset token is invalid or has expired.")) {
                    Toast.makeText(getApplicationContext(), "Token is expired", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Uri appLinkData = appLinkIntent.getData();
            String option = appLinkData.getQuery().toString();
            String[] myArray = option.toString().split("=");
            token = myArray[1];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
