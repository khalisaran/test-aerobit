package com.decoders.aerobit;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.response.RegisterResponse;
import com.aerobit.medapp.service.intent.DownloadResultReceiver;
import com.aerobit.medapp.service.intent.DownloadService;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.service.remote.PreferencesDataService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.Account.GenderSelectionActivity;
import com.decoders.aerobit.Account.MedicineActivity;
import com.decoders.aerobit.Family.AddFamilyGenderActivity;
import com.decoders.aerobit.Utility.ThemeSelection;

import java.util.Date;

public class ThemeActivity extends AppCompatActivity implements View.OnClickListener, DownloadResultReceiver.Receiver {
    public static final String TAG = ThemeActivity.class.getName();
    private Button btnSave;
    private Button checkbtncoloracent;

    private Toolbar mToolbar;

    private RelativeLayout mRelativeLayout;
    private LinearLayout mback;

    private Typeface font;
    private String mainScreen, gender = "empty";

    private PrefManager prefManager;
    private String color_code;

    private ImageView checkskyblue;
    private ImageView checkgreen;
    private ImageView checklightgreen;
    private ImageView checkyellow;
    private ImageView checkbrblue;
    private ImageView checkred;
    private ImageView checkdarkblue;
    private ImageView checkwhite;
    private ImageView femaleImage;
    private ImageView genderImage;

    private LinearLayout wleftarrow;
    private LinearLayout bleftarrow;

    private TextView txttheme;
    private TextView txtsave;
    private TextView txtannysmith;

    private ProgressBar gender_progress_bar;
    private static final String PERSON_REF_DATA = "person";

    private static final String COLOR_WHITE = "#FFFFFF";
    private static final String COLOR_SKY_BLUE = "#FF3498E4";
    private static final String COLOR_LIGHT_GREEN = "#FF6CCC6D";
    private static final String COLOR_LIGHT_BLUE = "#FF35CCB5";
    private static final String COLOR_LIGHT_YELLOW = "#FFDA9F2F";
    private static final String COLOR_ACCENT = "#FF4081";
    private static final String COLOR_BR_BLUE = "#FF614EDA";
    private static final String COLOR_RED = "#FFD23737";
    private static final String COLOR_PINK = "#FF8D3CBF";

    private static final String FAMILY_REF_DATA = "familyMember";

    private Person familyMember;

    private Person person;

    private DownloadResultReceiver mReceiver;

    // private Bundle userBundle,userInfoBundle,genderBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            System.out.println("I am restoring the person");
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            System.out.println("I am restoring the person --- " + person);
        }
        setContentView(R.layout.activity_theme);
        initViews();
        wleftarrow.setVisibility(View.VISIBLE);
        prefManager = PrefManager.getInstance();
        font = Typeface.createFromAsset(this.getAssets(), "Font/Avenir Roman.otf");
        btnSave.setTypeface(font);

        InitToolbar();                  //Init toolbar
        Screen();                       // screen selection
        initializeOnclickLister();       // Initialize onclick listener for button
        getBundle(getIntent());
        mback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressedFunction();
            }
        });
    }

    public void onClickSaveFunction(View v) {
        registerOrSaveFamilyMember();

    }

    // Onclick save theme button
    public void onClickSave(View v) {
        registerOrSaveFamilyMember();
    }

    private void initializeOnclickLister() {
        findViewById(R.id.white).setOnClickListener(this);
        findViewById(R.id.skyblue).setOnClickListener(this);
        findViewById(R.id.green).setOnClickListener(this);
        findViewById(R.id.lightgreen).setOnClickListener(this);
        findViewById(R.id.btncoloracent).setOnClickListener(this);
        findViewById(R.id.yellow).setOnClickListener(this);
        findViewById(R.id.red).setOnClickListener(this);
        findViewById(R.id.darkblue).setOnClickListener(this);
        findViewById(R.id.brblue).setOnClickListener(this);
    }

    private void initViews() {
        femaleImage = (ImageView) findViewById(R.id.femaleImage);
        genderImage = (ImageView) findViewById(R.id.genderImage);

        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.themescolor);
        mback = (LinearLayout) findViewById(R.id.ThemeBack);

        btnSave = (Button) findViewById(R.id.btnSave);

        checkskyblue = (ImageView) findViewById(R.id.checkskyblue);
        checkgreen = (ImageView) findViewById(R.id.checkgreen);
        checklightgreen = (ImageView) findViewById(R.id.checklightgreen);
        checkyellow = (ImageView) findViewById(R.id.checkyellow);
        checkbtncoloracent = (Button) findViewById(R.id.checkbtncoloracent);
        checkbrblue = (ImageView) findViewById(R.id.checkbrblue);
        checkred = (ImageView) findViewById(R.id.checkred);
        checkdarkblue = (ImageView) findViewById(R.id.checkdarkblue);

        wleftarrow = (LinearLayout) findViewById(R.id.wleftarrow);
        bleftarrow = (LinearLayout) findViewById(R.id.bleftarrow);

        txttheme = (TextView) findViewById(R.id.txttheme);
        txtsave = (TextView) findViewById(R.id.txtsave);
        txtannysmith = (TextView) findViewById(R.id.txtannysmith);

        checkwhite = (ImageView) findViewById(R.id.checkwhite);
        mback = (LinearLayout) findViewById(R.id.ThemeBack);
        gender_progress_bar = (ProgressBar) findViewById(R.id.gender_progress_bar);
    }

    @Override
    public void onBackPressed() {
        onBackPressedFunction();
    }

    public void InitToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");

    }

    public void registerOrSaveFamilyMember() {

        //check family member flow to Save Family Member and Move to Dashboard screen
        if (familyMember != null) {
            familyMember.setGuardian(PrefManager.getInstance().getGuardianPerson(getApplicationContext()));
            Preferences preferences = new Preferences();
            preferences.setAvatarColor(color_code);
            familyMember.setId(AerobitUtil.genUUID());
            familyMember.setActive(true);
            familyMember.setCreatedDate(new Date());
            familyMember = PersonService.getInstance().createPerson(getContentResolver(), familyMember);

            //Save Preference;
            preferences.setPerformer(familyMember);
            PreferenceService.getInstance().createPreference(getContentResolver(), preferences);

            PrefManager.getInstance().setSelectedUser(getApplicationContext(), familyMember);

            Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(PERSON_REF_DATA, person);
            intent.putExtra(FAMILY_REF_DATA, familyMember);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

        // check Registration Flow to Register and Move to add Family screen
        if (person != null && familyMember == null) {
            registerAndSaveData();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.white:
                color_code = ThemeSelection.color_biscuit;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                bleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.BLACK);
                txtsave.setTextColor(Color.BLACK);
                txtannysmith.setTextColor(Color.BLACK);
                wleftarrow.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.VISIBLE);
                break;
            case R.id.skyblue:
                color_code = ThemeSelection.code_skyBlue;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                checkskyblue.setVisibility(View.VISIBLE);
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.green:
                color_code = ThemeSelection.code_ightGreen;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkgreen.setVisibility(View.VISIBLE);
                checkskyblue.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.lightgreen:
                color_code = ThemeSelection.code_lightBlue;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.VISIBLE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.btncoloracent:
                color_code = ThemeSelection.code_ascent;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.VISIBLE);
                checklightgreen.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.yellow:
                color_code = ThemeSelection.code_yellow;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkyellow.setVisibility(View.VISIBLE);
                checkbtncoloracent.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkred.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.red:
                color_code = ThemeSelection.code_red;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkred.setVisibility(View.VISIBLE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.brblue:
                color_code = ThemeSelection.code_lightPink;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.VISIBLE);
                checkred.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
            case R.id.darkblue:
                color_code = ThemeSelection.code_brBlue;
                mToolbar.setBackgroundColor(Color.parseColor(color_code));
                mRelativeLayout.setBackgroundColor(Color.parseColor(color_code));
                wleftarrow.setVisibility(View.VISIBLE);
                txttheme.setTextColor(Color.WHITE);
                txtsave.setTextColor(Color.WHITE);
                txtannysmith.setTextColor(Color.WHITE);
                bleftarrow.setVisibility(View.GONE);
                checkbrblue.setVisibility(View.GONE);
                checkdarkblue.setVisibility(View.VISIBLE);
                checkred.setVisibility(View.GONE);
                checkyellow.setVisibility(View.GONE);
                checkbtncoloracent.setVisibility(View.GONE);
                checklightgreen.setVisibility(View.GONE);
                checkgreen.setVisibility(View.GONE);
                checkskyblue.setVisibility(View.GONE);
                checkwhite.setVisibility(View.GONE);
                break;
        }

        if(familyMember != null){

            familyMember.setThemeColor(color_code);
        } else if(familyMember == null && person!= null){
            person.setThemeColor(color_code);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    //Found which activity called
    public void Screen() {
        try {
            Bundle extras = getIntent().getExtras();
            mainScreen = extras.getString("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void restoreColorChoice() {
        if (color_code == null) {
            color_code = COLOR_ACCENT;
        }
        switch (color_code) {
            default:
            case COLOR_WHITE:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.purewhite));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.purewhite));
                txtannysmith.setTextColor(Color.BLACK);
                txttheme.setTextColor(Color.BLACK);
                txtsave.setTextColor(Color.BLACK);
                wleftarrow.setVisibility(View.GONE);
                bleftarrow.setVisibility(View.VISIBLE);
                checkwhite.setVisibility(View.VISIBLE);
                break;
            case COLOR_SKY_BLUE:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.skyblue));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.skyblue));
                checkskyblue.setVisibility(View.VISIBLE);
                break;
            case COLOR_LIGHT_GREEN:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.lightgreen));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.lightgreen));
                checkgreen.setVisibility(View.VISIBLE);
                break;
            case COLOR_LIGHT_BLUE:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.lightblu));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.lightblu));
                checklightgreen.setVisibility(View.VISIBLE);
                break;
            case COLOR_LIGHT_YELLOW:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.lightyellow));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.lightyellow));
                checkyellow.setVisibility(View.VISIBLE);
                break;

            case COLOR_ACCENT:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                checkbtncoloracent.setVisibility(View.VISIBLE);
                checkbtncoloracent.setSelected(true);
                break;
            case COLOR_BR_BLUE:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.brblue));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.brblue));
                checkdarkblue.setVisibility(View.VISIBLE);
                break;
            case COLOR_RED:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.red));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.red));
                checkred.setVisibility(View.VISIBLE);
                break;
            case COLOR_PINK:
                mToolbar.setBackgroundColor(getResources().getColor(R.color.lightpnk));
                mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.lightpnk));
                checkbrblue.setVisibility(View.VISIBLE);
                break;
        }
    }


    private void registerAndSaveData() {

        btnSave.setVisibility(View.VISIBLE);
        btnSave.getBackground().setAlpha(100);

        RegisterResponse registerResponse = PersonLoadDataService.getInstance().registerPerson(getApplicationContext(), person);

        if (registerResponse.getUserId() != null) {

            prefManager.setXid(getApplicationContext(), registerResponse.getXid());

            // Call load User and save to DB

            /* Starting Download Service */
            mReceiver = new DownloadResultReceiver(new Handler());
            mReceiver.setReceiver(this);
            Intent intent = new Intent(Intent.ACTION_SYNC, null, this, DownloadService.class);

            /* Send optional extras to Download IntentService */
            intent.putExtra("userId", registerResponse.getUserId());

            intent.putExtra("receiver", mReceiver);
            startService(intent);

        } else {
            // show error in register in UI
        }

    }

    public void onBackPressedFunction() {
        // TODO : Can we remove this main screen thing? lets go by object
        System.out.println("Scree " + mainScreen);
        if (mainScreen.equals("user")) {

            Intent intent = new Intent(getApplicationContext(), GenderSelectionActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            if (person != null) {
                person.setThemeColor(color_code);
                person.setGender(gender);
            }
            intent.putExtra(PERSON_REF_DATA, person);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            finish();

        }
        if (mainScreen.equals("family")) {

            Intent intent = new Intent(getApplicationContext(), AddFamilyGenderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            if (familyMember != null) {
                familyMember.setThemeColor(color_code);
                familyMember.setGender(gender);
            }
            intent.putExtra(FAMILY_REF_DATA, familyMember);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            finish();
        }

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK) {
            getBundle(data);
        }
    }

    void getBundle(Intent intent) {
        person = (Person) getIntent().getExtras()
                .getSerializable(PERSON_REF_DATA);

        if (person != null) {
            System.out.println("In the flow of person not family");
            gender = person.getGender();
            if (gender != null) {
                if (gender.equals("female")) {
                    femaleImage.setVisibility(View.VISIBLE);
                }
                if (gender.equals("male")) {
                    genderImage.setVisibility(View.VISIBLE);
                }

            } else {
                gender = "empty";
            }
            System.out.println("Gender in on activity result page - " + gender);

            color_code = person.getThemeColor();
            Log.v("color_code", "------------>" + color_code);

            restoreColorChoice();

            System.out.println("Gender in theme page - " + gender);

            txtannysmith.setText(person.getFirstName() + " " + person.getLastName());

        }
        familyMember = (Person) getIntent().getExtras()
                .getSerializable(FAMILY_REF_DATA);
        if (familyMember != null) {
            System.out.println("In the flow of family");
            gender = familyMember.getGender();
            if (gender != null) {
                if (gender.equals("female")) {
                    femaleImage.setVisibility(View.VISIBLE);
                }
                if (gender.equals("male")) {
                    genderImage.setVisibility(View.VISIBLE);
                }

            } else {
                gender = "empty";
            }
            System.out.println("Gender in on activity result page - " + gender);

            color_code = familyMember.getThemeColor();
            Log.v("color_code", "------------>" + color_code);

            restoreColorChoice();

            System.out.println("Gender in theme page - " + gender);

            txtannysmith.setText(familyMember.getFirstName() + " " + familyMember.getLastName());
        }

    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_RUNNING:

                gender_progress_bar.setVisibility(View.VISIBLE);
                btnSave.setBackground(getResources().getDrawable(R.drawable.shapes));

                break;
            case DownloadService.STATUS_FINISHED:
                gender_progress_bar.setVisibility(View.INVISIBLE);
                btnSave.setBackground(getResources().getDrawable(R.drawable.shapes));

                // Save User Preference to DB and Sync Service will save to Remote
                Preferences preferences = new Preferences();
                preferences.setAvatarColor(color_code);
                preferences.setPerformer(PrefManager.getInstance().getGuardianPerson(getApplicationContext()));

                PreferenceService.getInstance().createPreference(getContentResolver(), preferences);

                //PreferencesDataService.getInstance().Save(getApplicationContext(), preferences.getPerformer().getId(), preferences);

                /* Navigate to the Family Member Add screen */
                Intent intent = new Intent(getApplicationContext(), MedicineActivity.class);
                intent.putExtra(PERSON_REF_DATA, person);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


                break;
            case DownloadService.STATUS_ERROR:

                gender_progress_bar.setVisibility(View.GONE);
                btnSave.setBackground(getResources().getDrawable(R.drawable.shapes));

                /* Handle the error */
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, error, Toast.LENGTH_LONG).show();

                break;
        }
    }
}
