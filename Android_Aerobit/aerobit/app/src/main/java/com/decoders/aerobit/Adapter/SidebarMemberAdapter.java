package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Gender;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.R;

import java.util.List;

/**
 * Created by 10DECODERS on 2/21/2018.
 */

public class SidebarMemberAdapter extends RecyclerView.Adapter<SidebarMemberAdapter.MyViewHolder> {

    public List<Person> sidebarMembersList;

    private ListAdapterListener mListener;

    Context context;

    public interface ListAdapterListener {
        void onReceiveUser(Person person);
    }
    public SidebarMemberAdapter(List<Person> sidebarMembersList, ListAdapterListener mListener, Context context) {
        this.sidebarMembersList = sidebarMembersList;
        this.mListener = mListener;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sidebar_profile_member, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Person familyMember = sidebarMembersList.get(position);
        String selectedPerson = PrefManager.getInstance().getCurrentUser(context);
        Person objPerson = (Person) AerobitUtil.fromJson(selectedPerson, Person.class);
        System.out.println("from json -- "+selectedPerson);
        // if we are coming to family member,
        //final Person person = objPerson.getGuardian();

        if(objPerson.getId()!=null && familyMember!=null && !objPerson.getId().equals(familyMember.getId())) {
//            Log.v("person","---"+person.getId());
//            Log.v("guardian","---"+person.getGuardian().getId());
            holder.name.setVisibility(View.VISIBLE);
            holder.thumbnail.setVisibility(View.VISIBLE);
            holder.name.setText(familyMember.getFirstName() + " " + familyMember.getLastName());

            if(Gender.MALE.getValue().equalsIgnoreCase(familyMember.getGender())) {
                holder.thumbnail.setImageResource(R.drawable.male);
            }else{
                holder.thumbnail.setImageResource(R.drawable.female);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mListener.onReceiveUser(familyMember);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return sidebarMembersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.sidebar_member_name);
            thumbnail = (ImageView) view.findViewById(R.id.sidebar_member_image);
        }
    }

}
