package com.decoders.aerobit.Account;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.Family.FamilyMemberActivity;
import com.decoders.aerobit.R;
import com.decoders.aerobit.Utility.ThemeSelection;

import io.realm.Realm;

public class MedicineActivity extends AppCompatActivity {

    private ImageView btnss;
    private ImageView btnimg;
    private Realm mRelam;
    private ImageView ForBoth;
    private TextView txtvw5;
    private ImageView img_both;
    private static final String PERSON_REF_DATA="person";
    private static final String FAMILY_REF_DATA="familyMember";
    private Person person;
    private Person familyMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine);

        btnss =(ImageView) findViewById(R.id.Imgs2);
        btnimg = (ImageView) findViewById(R.id.Imgs);
        ForBoth=(ImageView)findViewById(R.id.img_both);
        img_both=(ImageView)findViewById(R.id.img_both);
        txtvw5=(TextView)findViewById(R.id.txtvw5);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            System.out.println("I am restoring the person");
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
        }
        Person guardian = person;
        txtvw5.setText("Hey "+ guardian.getFirstName() +",welcome to Aerobit");
        ForBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRelam= Realm.getDefaultInstance();
                //TODO: need to change
            }
        });

        getBundle(getIntent());

        btnss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                familyMemberScreen();
            }
        });

        btnimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dashboard();
            }
        });
        img_both.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                familyMemberScreen();
            }
        });
    }
    private void familyMemberScreen(){
        Intent intent = new Intent(getApplicationContext(), FamilyMemberActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(PERSON_REF_DATA, person);
        intent.putExtra(FAMILY_REF_DATA, familyMember);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private void dashboard(){
        Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(PERSON_REF_DATA, person);
        intent.putExtra(FAMILY_REF_DATA, familyMember);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    @Override
    public void onBackPressed() {
        Intent gotToDashBoard = new Intent(getApplication(), BottomNavigation.class);
        gotToDashBoard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        gotToDashBoard.putExtra(PERSON_REF_DATA, person);
        gotToDashBoard.putExtra(FAMILY_REF_DATA, familyMember);
        startActivity(gotToDashBoard);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }

    void getBundle(Intent intent) {
        Bundle b = getIntent().getExtras();
        if (b != null) {
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            // this needs to be refined. we need to have separate ui element or activity for family
            familyMember = (Person) getIntent().getExtras()
                    .getSerializable(FAMILY_REF_DATA);
        }
    }


}