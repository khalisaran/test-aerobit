package com.decoders.aerobit.Utility;

/**
 * Created by crosssales on 3/10/2018.
 */

public class Constants {
    public static final String user_bundle="account";
    public static final String user_info="userInfoBundle";
    public static final String user_gender="genderBundle";

    public static final String email="email";
    public static final String password="password";
    public static final String firstName="firstname";
    public static final String lastName="lastname";
    public static final String birthYear="birthYear";
    public static final String gender="gender";
    public static final String color="color";


}
