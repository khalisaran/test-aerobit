package com.decoders.aerobit.Account;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.response.RegisterResponse;
import com.aerobit.medapp.service.intent.DownloadResultReceiver;
import com.aerobit.medapp.service.intent.DownloadService;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.R;
import com.decoders.aerobit.ThemeActivity;
import com.decoders.aerobit.Utility.ThemeSelection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenderSelectionActivity extends AppCompatActivity implements DownloadResultReceiver.Receiver{
    @BindView(R.id.rbtn2)
    Button btns;

    @BindView(R.id.user_gender_male)
    LinearLayout linearmale;
    @BindView(R.id.radi1)
    LinearLayout male;
    @BindView(R.id.radi2)
    LinearLayout female;
    TextView textgender;
    @BindView(R.id.gender_progress_bar)
    ProgressBar gender_progress_bar;
    @BindView(R.id.textv4)
    TextView textmale;
    @BindView(R.id.textv3)
    TextView textfemale;
    @BindView(R.id.themeselection)
    RelativeLayout themeselection;
    @BindView(R.id.dropname)
    TextView dropName;

    private boolean check;
    ImageView dropImage;

    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;

    private PersonService mPersonService;
    private PrefManager prefManager;
    private PreferenceService mPreferenceService;

    private static final String TAG = GenderSelectionActivity.class.getName();
    private String color_code;


    private static final String PERSON_REF_DATA="person";
    private static final String FAMILY_REF_DATA="familyMember";

    private static final String SELECT_GENDER="Please Select the gender";

    private Person person;
    private Person familyMember;

    String selection = "empty";
    String[] themename = {"#FFFFFF", "#FF4081", "#FF3498E4", "#FF6CCC6D",
            "#FFDA9F2F", "#FFD23737", "#FF614EDA", "#FF8D3CBF", "#FF35CCB5"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);
        ButterKnife.bind(this);
        initViews();
        toolbarInfo();

        prefManager = PrefManager.getInstance();
        mPersonService = PersonService.getInstance();
        mPreferenceService = PreferenceService.getInstance();

       // Update_Data();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            familyMember = (Person) getIntent().getExtras()
                    .getSerializable(FAMILY_REF_DATA);
           getBundle(getIntent());
        }
        checkGender();
    }

    private void initViews(){
        dropImage=(ImageView) findViewById(R.id.dropimage);
        mToolbar=(Toolbar)findViewById(R.id.createAccountToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);
    }

    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Create Account");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intentParent = new Intent(getApplicationContext(),BirthyearAccountActivity.class);
        person.setThemeColor(color_code);
        person.setGender(selection);
        intentParent.putExtra(PERSON_REF_DATA, person);
        intentParent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentParent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    //GenderSelectionActivity selection function
    @OnClick(R.id.rbtn2)
    public void onClickGender() {
        if (check) {

            registerAndSaveData();

        } else {
            Toast.makeText(getApplicationContext(), SELECT_GENDER, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.gender_female)
    public void onClickFemale() {

        check = true;
        male.setBackgroundResource(R.drawable.maleblur);
        female.setBackgroundResource(R.drawable.female);
        textmale.setTextColor(Color.BLACK);
        textfemale.setTextColor(Color.GRAY);
        selection = "female";
        person.setGender("female");

    }

    @OnClick(R.id.user_gender_male)
    public void onClickMale() {

        check = true;
        female.setBackgroundResource(R.drawable.femaleblur);
        male.setBackgroundResource(R.drawable.male);
        textfemale.setTextColor(Color.BLACK);
        textmale.setTextColor(Color.GRAY);
        selection = "male";
        person.setGender("male");
    }
    // Set gender bundle on click

    @OnClick(R.id.themeselection)
    public void onClickTheme() {

        Intent intent = new Intent(getApplicationContext(), ThemeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        person.setThemeColor(color_code);
        person.setGender(selection);

        intent.putExtra("screen", "user");
        intent.putExtra(PERSON_REF_DATA, person);

        startActivityForResult(intent,1);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    //Prev data
    private void Update_Data() {
     //   String gen = prefManager.getPrefGender();
        String gen="male";
        if (gen.equals("male")) {

            check = true;
            female.setBackgroundResource(R.drawable.femaleblur);
            male.setBackgroundResource(R.drawable.male);
            textfemale.setTextColor(Color.BLACK);
            textmale.setTextColor(Color.GRAY);
            Log.v("prefManager", "male");

        }
        if (gen.equals("female")) {
            check = true;
            male.setBackgroundResource(R.drawable.maleblur);
            female.setBackgroundResource(R.drawable.female);
            textmale.setTextColor(Color.BLACK);
            //textfemale.setTextColor(Color.BLACK);
            textfemale.setTextColor(Color.GRAY);
            Log.v("preManager", "female");

        }
    }

    //Radio button pressed state
    public void checkGender() {
        String genderOpt = person.getGender();
        if(genderOpt==null){
            genderOpt = "empty";
        }
        if (genderOpt.equals("male")) {
            check = true;
            female.setBackgroundResource(R.drawable.femaleblur);
            male.setBackgroundResource(R.drawable.male);
            textfemale.setTextColor(Color.BLACK);
            textmale.setTextColor(Color.GRAY);
            selection="male";
            Log.v("getString", "male");

        }
        if (genderOpt.equals("female")) {
            check = true;
            male.setBackgroundResource(R.drawable.maleblur);
            female.setBackgroundResource(R.drawable.female);
            textmale.setTextColor(Color.BLACK);
            textfemale.setTextColor(Color.GRAY);
            selection="female";
            Log.v("getString", "female");
        }

    }

    private DownloadResultReceiver mReceiver;

    private void registerAndSaveData() {

        gender_progress_bar.setVisibility(View.VISIBLE);
        btns.getBackground().setAlpha(100);

        RegisterResponse registerResponse = PersonLoadDataService.getInstance().registerPerson(getApplicationContext(), person);

        if(registerResponse.getUserId() != null){

            prefManager.setXid(getApplicationContext(),registerResponse.getXid());

            // Call load User and save to DB

            /* Starting Download Service */
            mReceiver = new DownloadResultReceiver(new Handler());
            mReceiver.setReceiver(this);
            Intent intent = new Intent(Intent.ACTION_SYNC, null, this, DownloadService.class);

            /* Send optional extras to Download IntentService */
            intent.putExtra("userId", registerResponse.getUserId());

            intent.putExtra("receiver", mReceiver);
            startService(intent);

        } else{
            //TODO: need to show error message
            // show error in register in UI
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK) {
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            getBundle(data);
        }
    }
    void getBundle(Intent intent) {

        Bundle b = getIntent().getExtras();

        if (b != null) {
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            if(person!=null){
                String gender = person.getGender();
                if(gender == null){
                    gender="empty";
                }
                color_code = person.getThemeColor();

                if(color_code!=null){
                    Log.v(TAG,color_code);
                    ThemeSelection.setImage(dropImage,color_code);
                }
                if (gender.equals("male")) {
                    check = true;
                    selection = "male";
                    female.setBackgroundResource(R.drawable.femaleblur);
                    male.setBackgroundResource(R.drawable.male);
                    textfemale.setTextColor(Color.BLACK);
                    textmale.setTextColor(Color.GRAY);
                }
                if (gender.equals("female")) {
                    selection = "female";
                    check = true;
                    male.setBackgroundResource(R.drawable.maleblur);
                    female.setBackgroundResource(R.drawable.female);
                    textmale.setTextColor(Color.BLACK);
                    textfemale.setTextColor(Color.GRAY);
                }
            }else{
                selection="empty";
                color_code="#FF4081";
                ThemeSelection.setImage(dropImage,"#FF4081");
            }

        }
        // this needs to be refined. we need to have separate ui element or activity for family
        familyMember = (Person) getIntent().getExtras()
                .getSerializable(FAMILY_REF_DATA);
        if(familyMember!=null){
            selection = familyMember.getGender();
            if(selection == null){
                selection = "empty";
            }
            String code=familyMember.getThemeColor();
            if(code!=null){
                color_code=code;
            }else{
                color_code="#FF4081";
            }
            if (selection.equals("male")) {
                selection = "male";
                female.setBackgroundResource(R.drawable.femaleblur);
                male.setBackgroundResource(R.drawable.male);
                textfemale.setTextColor(Color.BLACK);
                textmale.setTextColor(Color.GRAY);
            }
            if (selection.equals("female")) {
                selection = "female";
                male.setBackgroundResource(R.drawable.maleblur);
                female.setBackgroundResource(R.drawable.female);
                textmale.setTextColor(Color.BLACK);
                textfemale.setTextColor(Color.GRAY);
            }
        }else{
            color_code="#FF4081";
            ThemeSelection.setImage(dropImage,"#FF4081");
            selection="empty";
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_RUNNING:

                gender_progress_bar.setVisibility(View.VISIBLE);
                btns.setBackground(getResources().getDrawable(R.drawable.shapes));

                break;
            case DownloadService.STATUS_FINISHED:
                gender_progress_bar.setVisibility(View.INVISIBLE);
                btns.setBackground(getResources().getDrawable(R.drawable.shapes));

                // Save User Preference to DB and Sync Service will save to Remote
                Preferences preferences = new Preferences();
                preferences.setAvatarColor(color_code);
                preferences.setPerformer(PrefManager.getInstance().getGuardianPerson(getApplicationContext()));

                PreferenceService.getInstance().createPreference(getContentResolver(), preferences);
                /* Navigate to the Family Member Add screen */
                Intent intent = new Intent(getApplicationContext(),MedicineActivity.class);
                intent.putExtra(PERSON_REF_DATA, person);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


                break;
            case DownloadService.STATUS_ERROR:

                gender_progress_bar.setVisibility(View.GONE);
                btns.setBackground(getResources().getDrawable(R.drawable.shapes));

                /* Handle the error */
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, error, Toast.LENGTH_LONG).show();

                break;
        }
    }
}
