package com.decoders.aerobit;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

//import com.decoders.aerobit.Parcer.FamilyParcer;
//import com.decoders.aerobit.Parcer.MedicineParcer;
//import com.decoders.aerobit.Parcer.UserParcer;


/**
 * Created by crosssales on 2/10/2018.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter{
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

    }
//    private static final String TAG = "SYNC_ADAPTER";
//    /**
//     * This gives us access to our local data source.
//     */
//    private final ContentResolver resolver;
//    //private JSONObject jsonObject;
//    SyncResult syncResult;
//    PrefManager prefManager;
//    JSONArray jsonArray;
//    JSONObject jsonObject1;
//    //Map<String, Family> networkEntries;
//    ArrayList<ContentProviderOperation> batchDummy = new ArrayList<>();
//    JSONObject postDataParams = new JSONObject();
//    Context c;
//    Intent intent;
//    Realm mRealm;
//
//  //  public static String str_receiver = "servicetutorial.service.receiver";
//
//    public SyncAdapter(Context c, boolean autoInit) {
//        this(c, autoInit, false);
//        this.c=c;
//    }
//
//    public SyncAdapter(Context c, boolean autoInit, boolean parallelSync) {
//        super(c, autoInit, parallelSync);
//        this.resolver = c.getContentResolver();
//        prefManager=new PrefManager(c);
//        mRealm=Realm.getDefaultInstance();
//        this.c=c;
//      //  intent = new Intent(str_receiver);
//    }
//
//    /**
//     * This method is run by the Android framework, on a new Thread, to perform a sync.
//     * @param account Current account
//     * @param extras Bundle extras
//     * @param authority Content authority
//     * @param provider {@link ContentProviderClient}
//     * @param syncResult Object to write stats to
//     */
//    @Override
//    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
///*
//        long delay=3000;
//        long lastSync =prefManager.getSync() ;
//        long now=System.currentTimeMillis();
//        Log.v("before",String.valueOf(lastSync));
//        if(now>lastSync+3000){
//            prefManager.setSync(System.currentTimeMillis());
//            Log.v(TAG, "Starting synchronization...");
//            try {
//                this.syncResult=syncResult;
//                syncUserInfo(syncResult);
//                syncFamilyTable(syncResult);
//            } catch (IOException ex) {
//                Log.v(TAG, "Error synchronizing!", ex);
//                syncResult.stats.numIoExceptions++;
//            } catch (JSONException ex) {
//                Log.v(TAG, "Error synchronizing!", ex);
//                syncResult.stats.numParseExceptions++;
//            } catch (RemoteException |OperationApplicationException ex) {
//                Log.v(TAG, "Error synchronizing!", ex);
//                syncResult.stats.numAuthExceptions++;
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//
//            Log.v(TAG, "Finished synchronization!");
//        }else
//        {
//            Log.v("Sync","Protect Synchronization!");
//        }*/
//    }
//
//    //Sync user information
//
//    private void syncUserInfo(SyncResult syncResult) throws IOException, JSONException, RemoteException, OperationApplicationException, ExecutionException, InterruptedException {
//        String url=liveAPI.Person+prefManager.getUser();
//        Log.v("User live API",url);
//
//        String data=download(url);
//        JSONObject jsonObjUser=new JSONObject(data);
//        Map<String, User> networkUser=new HashMap<>();
//
//            Log.v("Network JSON",jsonObjUser.toString());
//            User mUser = UserParcer.parse(jsonObjUser,c);
//            networkUser.put(prefManager.getUser(), mUser);
//            Log.v("Server Firstname",mUser.getFirstname()+" "+mUser.getLastname()+" "+mUser.getUserid()
//                    +" "+mUser.getBirthyear()+" "+" "+mUser.getGender()+" "+mUser.getEmail());
//
//        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
//        Log.v(TAG, "Fetching local entries...");
//        Cursor c=resolver.query(DatabaseContract.TaskColumns.CONTENT_USER,null,null,null,null,null);
//        assert c!=null;
//        c.moveToFirst();
//        String fname,lname,gender,byear,status,id,userID,lastsync,email,password;
//        User user;
//        for(int i=0;i<c.getCount();i++)
//        {
//            syncResult.stats.numEntries++;
//
//            fname=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_FIRSTNAME));
//            lname=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_LASTNAME));
//            id=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_ID));
//            lastsync=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_LASTSYNC));
//            gender=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_GENDER));
//            byear=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_BIRTHYEAR));
//            userID=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_USER_ID));
//            status=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_STATUS));
//            email=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_EMAIL));
//            password=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.U_PASSWORD));
//
//            Log.v("Local userID",userID+" "+fname+" "+lname+" "+gender+" "+byear+" "+lastsync+" "+status+" "+id);
//            user=networkUser.get(userID.trim());
//            if(user!=null)
//            {
//                Log.v("Hey! its firing","----------------------------------->"+"************");
//                networkUser.remove(userID.trim());
//
//                 /*   if(status.trim().equals("NotSynced")) {
//
//                    }
//                }*/
//
//            }else
//            {
//
//            }
//            c.moveToNext();
//        }
//        c.close();
//
//        // Add all the new entries
//        for (User muser : networkUser.values()) {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//            String currentDateandTime = sdf.format(new Date());
//            Log.i(TAG, "Scheduling insert: " + muser.getFirstname());
//            batch.add(ContentProviderOperation.newInsert(DatabaseContract.TaskColumns.CONTENT_USER)
//                    .withValue(DatabaseContract.TaskColumns.U_USER_ID,muser.getUserid())
//                    .withValue(DatabaseContract.TaskColumns.U_FIRSTNAME,muser.getFirstname())
//                    .withValue(DatabaseContract.TaskColumns.U_LASTNAME,muser.getLastname())
//                    .withValue(DatabaseContract.TaskColumns.U_BIRTHYEAR, muser.getBirthyear())
//                    .withValue(DatabaseContract.TaskColumns.U_GENDER,muser.getGender())
//                    .withValue(DatabaseContract.TaskColumns.U_THEME,"5")
//                    .withValue(DatabaseContract.TaskColumns.U_STATUS,"Synced")
//                    .withValue(DatabaseContract.TaskColumns.U_LASTSYNC,currentDateandTime)
//                    .withValue(DatabaseContract.TaskColumns.EMAIL,muser.getEmail())
//                    .withValue(DatabaseContract.TaskColumns.U_PASSWORD,prefManager.getUserPassword())
//                    .build());
//            syncResult.stats.numInserts++;
//            prefManager.selectedUserInfo(muser.getUserid(),5,muser.getFirstname(),muser.getGender(),false);
//        }
//
//
//        // Synchronize by performing batch update
//        Log.i(TAG, "Merge solution ready, applying batch update...");
//        resolver.applyBatch(DatabaseContract.CONTENT_AUTHORITY, batch);
//        resolver.notifyChange(DatabaseContract.TaskColumns.CONTENT_USER,null,false);
//
//       // getContext().getContentResolver().notifyChange(DatabaseContract.TaskColumns.CONTENT_USER,null,false);
//        //intent.putExtra("ui","user");
//        //intent.putExtra("longitude","family");
//
//
//
//
//    }
//
//// Sync care plan
//
//    private void syncCarePlan(SyncResult syncResult) throws IOException, JSONException, RemoteException, OperationApplicationException, ExecutionException, InterruptedException {
//    Log.v(TAG,"CarePlan1"+liveAPI.getMedicine);
//    String base=liveAPI.getMedicine;
//        ArrayList<ContentProviderOperation> batch = null;
//        Cursor c=resolver.query(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,null,null,null,null);
//        assert c!=null;
//        c.moveToFirst();
//        String userID;
//        Log.v(TAG,String.valueOf(c.getCount()));
//        Map<String,CarePlan1> MedicineEntries = new HashMap<>();
//        for(int i=0;i<c.getCount();i++) {
//            syncResult.stats.numEntries++;
//            userID=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.USER_ID));
//
//            String response=download(base+userID);
//            JSONArray jsonArrayMedicine=new JSONArray(response);
//
//            Log.v("JSON ARRAY",String.valueOf(jsonArrayMedicine.length()));
//            for(int j=0;j<jsonArrayMedicine.length();j++) {
//                JSONObject jsonOb=jsonArrayMedicine.getJSONObject(j);
//                Log.v("JSON Objec",jsonOb.toString());
//                CarePlan1 carePlan1 = MedicineParcer.parse(jsonOb);
//                MedicineEntries.put(carePlan1.getUserid(), carePlan1);
//                Log.v("Download medicine", carePlan1.getName()+" "+ carePlan1.getUserid()+" "+ carePlan1.getMedID()
//                +" "+ carePlan1.getTime()+"Strength"+ carePlan1.getStrength());
//            }
//
//            batch = new ArrayList<>();
//            Log.v(TAG, "Fetching local entries...");
//            CarePlan1 mCare;
//            Cursor mCursor=resolver.query(DatabaseContract.TaskColumns.CONTENT_CARE,null,null,null,null,null);
//            assert mCursor!=null;
//            mCursor.moveToFirst();
//            String medicineName,medicineType,medicineUser,medicineStrength,medicineId,medicineStatus,medicineLastSync,medicineTime;
//            for(int k=0;k<mCursor.getCount();k++) {
//                syncResult.stats.numEntries++;
//                medicineUser=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_USERID));
//                medicineName=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_NAME));
//                medicineType=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_TYPE));
//                medicineStrength=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_STRENGTH));
//                medicineId=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_MED_ID));
//                medicineStatus=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_STATUs));
//                medicineLastSync=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_LAST_SYNC));
//                medicineTime=mCursor.getString(mCursor.getColumnIndex(DatabaseContract.TaskColumns.M_TIME));
//
//                Log.v(TAG,medicineName+medicineType+medicineStrength+medicineStatus+medicineLastSync);
//
//                mCare=MedicineEntries.get(medicineUser.trim());
//                if(mCare!=null)
//                {
//                    Log.v(TAG,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
//                    MedicineEntries.remove(medicineUser.trim());
//
//                }/*else{
//                  //  Log.v(TAG,"***************************************************************************"+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
//                  //  String url=liveAPI.AddMedicine;
//                    //String url,String medicineName,String medicineId,String medicineStrength,String userID,String medicineTime
//                  //  postJsonObject(url,medicineName,medicineId,medicineStrength,medicineUser,medicineTime);
//
//                }*/
//                mCursor.moveToNext();
//            }
//            mCursor.close();
//
//            for (CarePlan1 carePlan1 : MedicineEntries.values()) {
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//                String currentDateandTime = sdf.format(new Date());
//                Log.v(TAG, "Scheduling insert: " + carePlan1.getName());
//                batch.add(ContentProviderOperation.newInsert(DatabaseContract.TaskColumns.CONTENT_CARE)
//                        .withValue(DatabaseContract.TaskColumns.M_TYPE, carePlan1.getType())
//                        .withValue(DatabaseContract.TaskColumns.M_NAME, carePlan1.getName())
//                        .withValue(DatabaseContract.TaskColumns.M_MED_ID, carePlan1.getMedID())
//                        .withValue(DatabaseContract.TaskColumns.M_STRENGTH, carePlan1.getStrength())
//                        .withValue(DatabaseContract.TaskColumns.M_TIME, carePlan1.getTime())
//                        .withValue(DatabaseContract.TaskColumns.M_STATUs,"Synced")
//                        .withValue(DatabaseContract.TaskColumns.M_LAST_SYNC,currentDateandTime)
//                        .withValue(DatabaseContract.TaskColumns.M_USERID, carePlan1.getUserid())
//                        .build());
//                        syncResult.stats.numInserts++;
//            }
//            // Synchronize by performing batch update
//            Log.i(TAG, "Merge solution ready, applying batch update...");
//            resolver.applyBatch(DatabaseContract.CONTENT_AUTHORITY, batch);
//            resolver.notifyChange(DatabaseContract.TaskColumns.CONTENT_CARE,null,false);
//
//        c.moveToNext();
//        }
//        c.close();
//    }
//
//
//
//// Sync family table
//    private void syncFamilyTable(SyncResult syncResult) throws IOException, JSONException, RemoteException, OperationApplicationException, ExecutionException, InterruptedException {
//        boolean b=false;
//
//        Log.v(TAG,"FamilyGroup"+ liveAPI.hisRelatedPerson+prefManager.getUser());
//        Log.v(TAG,"Authorize"+prefManager.getAuthorize());
//        String url=liveAPI.hisRelatedPerson+prefManager.getUser();
//
//        Log.v("Network entries",url.toString());
//        networkEntries = new HashMap<>();
//
//        String data=download(url);
//        jsonArray=new JSONArray(data);
//        Log.v("jsonArraySize","--------"+jsonArray.length());
//
//        for (int i=0;i<jsonArray.length();i++)
//        {
//                JSONObject jsonObject=jsonArray.getJSONObject(i);
//                Log.v("Network JSON",jsonObject.toString());
//                Family mStud = FamilyParcer.parse(jsonArray.optJSONObject(i));
//                networkEntries.put(mStud.getUserID(), mStud);
//                Log.v("Server Firstname",mStud.getFirstname()+" "+mStud.getLastname()+" "+mStud.getUserID()
//                +" "+mStud.getBirthyear()+" "+mStud.getBirthyear()+" "+mStud.getGender()+" "+mStud.getEmail()+" "+mStud.isActive());
//        }
//
//        ArrayList<ContentProviderOperation> batch = new ArrayList<>();
//        Log.v(TAG, "Fetching local entries...");
//        Cursor c=resolver.query(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,null,null,null,null);
//        assert c!=null;
//        c.moveToFirst();
//
//        String fname,lname,gender,byear,status,id,userID,lastsync,update,email,active;
//
//        Log.v("Count Local database",String.valueOf(c.getCount()));
//        Family family;
//        for(int i=0;i<c.getCount();i++)
//        {
//            syncResult.stats.numEntries++;
//
//            fname=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.FIRST_NAME));
//            lname=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.LAST_NAME));
//            id=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.ID));
//            lastsync=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.LASTSYNC));
//            gender=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.GENDER));
//            byear=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.BIRTH_YEAR));
//            userID=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.USER_ID));
//            status=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.STATUS));
//            update=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.UPDATETIME));
//            email=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.EMAIL));
//            active=c.getString(c.getColumnIndex(DatabaseContract.TaskColumns.ACTIVE));
//
//            Log.v("Local userID",userID+" "+fname+" "+lname+" "+gender+" "+byear+" "+lastsync+" "+status+" "+id);
//            family=networkEntries.get(userID.trim());
//            if(family!=null)
//            {
//                Log.v("Hey! its firing","----------------------------------->"+"************");
//                networkEntries.remove(userID.trim());
//                if(active.equals("true")){
//                    if(status.trim().equals("NotSynced")) {
//                        String urlAdd = liveAPI.UpdateFamily + userID;
//                        Log.v("Update Family", urlAdd);
//                        createJsonObject(fname, lname, gender, byear, id, urlAdd, "update", update, email,"true");
//                    }
//                }
//                if(active.equals("false"))
//                {
//                    if(status.trim().equals("NotSynced")) {
//                        String urlAdd = liveAPI.UpdateFamily + userID;
//                        Log.v("Update Family", urlAdd);
//                        createJsonObject(fname, lname, gender, byear, id, urlAdd, "update", update, email,"false");
//                    }
//                }
//            }else
//            {
//            String user_ID = prefManager.getUser();
//            String urlAdd = liveAPI.family + user_ID;
//            Log.v("URL", urlAdd);
//            createJsonObject(fname,lname,gender,byear,id,urlAdd,"insert",update,email,"true");
//            }
//            c.moveToNext();
//        }
//        c.close();
//
//            // Add all the new entries
//            for (Family family1 : networkEntries.values()) {
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//                String currentDateandTime = sdf.format(new Date());
//                Log.i(TAG, "Scheduling insert: " + family1.getFirstname()+USER_ID);
//                batch.add(ContentProviderOperation.newInsert(DatabaseContract.TaskColumns.CONTENT_FAMILY)
//                        .withValue(DatabaseContract.TaskColumns.USER_ID,family1.getUserID())
//                        .withValue(DatabaseContract.TaskColumns.FIRST_NAME,family1.getFirstname())
//                        .withValue(DatabaseContract.TaskColumns.LAST_NAME,family1.getLastname())
//                        .withValue(DatabaseContract.TaskColumns.BIRTH_YEAR, family1.getBirthyear())
//                        .withValue(DatabaseContract.TaskColumns.THEME,"5")
//                        .withValue(DatabaseContract.TaskColumns.GENDER,family1.getGender())
//                        .withValue(DatabaseContract.TaskColumns.STATUS,"Synced")
//                        .withValue(DatabaseContract.TaskColumns.LASTSYNC,currentDateandTime)
//                        .withValue(DatabaseContract.TaskColumns.UPDATETIME,currentDateandTime)
//                        .withValue(DatabaseContract.TaskColumns.EMAIL,family1.getEmail())
//                        .withValue(DatabaseContract.TaskColumns.ACTIVE,family1.isActive())
//                        .build());
//                syncResult.stats.numInserts++;
//                b=true;
//            }
//
//            // Synchronize by performing batch update
//            Log.i(TAG, "Merge solution ready, applying batch update...");
//            resolver.applyBatch(DatabaseContract.CONTENT_AUTHORITY, batch);
//            if(b)
//            resolver.notifyChange(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,false);
//          //  getContext().getContentResolver().notifyChange(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,false);
//      //  syncCarePlan(syncResult);
//
//    }
//    // post medication details
//    private void postJsonObject(String url,String medicineName,String medicineId,String medicineStrength,String userID,String medicineTime)
//    {
//        JSONObject baseObj=new JSONObject();
//
//
//        try {
//            JSONObject timeObj=new JSONObject();
//            timeObj.put("timing",medicineTime);
//            JSONArray timeArray=new JSONArray();
//            timeArray.put(timeObj);
//            timeObj.put("dosageInstruction",jsonArray);
//
//            baseObj.put("status","ACTIVE");
//
//            JSONObject identifyObj=new JSONObject();
//            identifyObj.put("value","");
//            identifyObj.put("system","phone");
//            identifyObj.put("use","home");
//
//            baseObj.put("identifier",identifyObj);
//            baseObj.put("category","Scheduled Dose");
//
//            JSONObject performerObj=new JSONObject();
//            performerObj.put("id",userID);
//            baseObj.put("performer",performerObj);
//            baseObj.put("strength",medicineStrength);
//
//            JSONObject medRef=new JSONObject();
//            medRef.put("id",medicineId);
//            baseObj.put("active",true);
//
//            JSONObject objText=new JSONObject();
//            objText.put("div","null");
//            objText.put("status","null");
//            baseObj.put("text",objText);
//            baseObj.put("resourceType","MedicationRequest");
//
//            String response=new SendPostRequest().execute(url,"medicine").get();
//            Log.v("Response Success","Add medicine"+response);
//
//        }catch (Exception e){
//
//        }
//
//    }
//
//
//    private String download(String url) throws IOException {
//
//        String access=prefManager.getAuthorize();
//        String xid=prefManager.getXid();
//        // Ensure we ALWAYS close these!
//        HttpURLConnection client = null;
//        InputStream is = null;
//
//        try {
//            // Connect to the server using GET protocol
//            URL server = new URL(url);
//            client = (HttpURLConnection)server.openConnection();
//            client.setRequestProperty("Content-Type", "application/json");
//            client.setRequestProperty("Accpet", "application/json");
//            client.setRequestProperty("Authorization", "Bearer " + access);
//            client.setRequestProperty("xid",xid);
//
//            client.connect();
//
//            // Check for valid response code from the server
//            int status = client.getResponseCode();
//            is = (status == HttpURLConnection.HTTP_OK)
//                    ? client.getInputStream() : client.getErrorStream();
//
//            // Build the response or error as a string
//            BufferedReader br = new BufferedReader(new InputStreamReader(is));
//            StringBuilder sb = new StringBuilder();
//            for (String temp; ((temp = br.readLine()) != null);) {
//                sb.append(temp);
//            }
//
//            return sb.toString();
//        } finally {
//            if (is != null) { is.close(); }
//            if (client != null) { client.disconnect(); }
//        }
//    }
//
//    public void createJsonObject(String fname,String lname,String gender,String byear,String id,String url,String action,String update,String email,String act) {
//
//        boolean active_boolean=true;
//        if(act.equals("true"))
//        {
//            active_boolean=true;
//        }
//        if(act.equals("false"))
//        {
//            active_boolean=false;
//        }
//
//
//        //  jsonObject = new JSONObject();
//        JSONObject objectJSon = new JSONObject();
//        try {
//
//            //Text objectJSon
//            JSONObject txtObject = new JSONObject();
//            objectJSon.put("active",active_boolean);
//
//            txtObject.put("status", "");
//            txtObject.put("div", "");
//            objectJSon.put("text", txtObject);   //parent
//
//            JSONArray idenfier = new JSONArray();     // identifier Array
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("use", "");
//
//            JSONArray coding = new JSONArray();
//            JSONObject jsonObject1 = new JSONObject();
//            jsonObject1.put("system", "");
//            jsonObject1.put("code", "");
//            coding.put(jsonObject1);
//            jsonObject.put("coding", coding);
//            idenfier.put(jsonObject);
//            objectJSon.put("identifier", idenfier);             //Parent
//
//            JSONObject nameObj = new JSONObject();
//            nameObj.put("use", "dummy");
//            nameObj.put("family", "dummy");
//
//            JSONArray familyArr = new JSONArray();
//
//            familyArr.put(fname);                      // family fname
//            familyArr.put(lname);                     // family lname
//            nameObj.put("given", familyArr);
//            objectJSon.put("name", nameObj);              //parent
//
//            JSONArray telecomArr = new JSONArray();
//            JSONObject jsonObject2 = new JSONObject();
//
//            JSONObject jsonObject3 = new JSONObject();
//
//            jsonObject3.put("system", "email");
//            jsonObject3.put("value", email);         //User email
//            jsonObject3.put("use", "work");
//            telecomArr.put(jsonObject3);
//
//
//            objectJSon.put("telecom", telecomArr);   //parent
//            objectJSon.put("gender", gender);                  // Family gender
//            objectJSon.put("birthDate", byear);                //Family birth year
//
//            JSONArray addArr = new JSONArray();
//
//            JSONObject jsonObject4 = new JSONObject();
//            jsonObject4.put("country", "");
//            jsonObject4.put("use", "");
//
//            JSONArray lineArr = new JSONArray();
//            lineArr.put("");
//            jsonObject4.put("line", lineArr);
//            jsonObject4.put("state", "");
//            jsonObject4.put("city", "");
//            jsonObject4.put("postalCode", "");
//            addArr.put(jsonObject4);
//            objectJSon.put("address", addArr);        //working
//
//            JSONObject objPhoto = new JSONObject();
//            objPhoto.put("contentType", "PNG");
//            objPhoto.put("data", "imagetoString");
//            objectJSon.put("photo", objPhoto);
//            objectJSon.put("type", "patient");
//            objectJSon.put("status", "active");
//
//            JSONArray contactArr = new JSONArray();
//            JSONObject jsonObject5 = new JSONObject();
//            JSONObject jsonObject6 = new JSONObject();
//            jsonObject6.put("use", "dummy");
//            jsonObject6.put("family", "dummy");
//
//            JSONArray givenArr = new JSONArray();
//            givenArr.put("Peter");
//            givenArr.put("James");
//            jsonObject6.put("given", givenArr);
//            jsonObject5.put("name", jsonObject6);
//
//            JSONArray telArr = new JSONArray();                    //user phone number
//
//            telArr.put(jsonObject3);
//            jsonObject5.put("telecom",telArr);
//
//            JSONObject jsnAddress = new JSONObject();
//            jsnAddress.put("country", "");
//            jsnAddress.put("use", "");
//
//            JSONArray jsnArr = new JSONArray();
//            jsnArr.put("534 Erewhon St");
//            jsnAddress.put("line", jsnArr);
//            jsnAddress.put("state", "");
//            jsnAddress.put("city", "");
//            jsnAddress.put("postalCode", "");
//            jsonObject5.put("address", jsnAddress);
//            jsonObject5.put("gender", gender);
//            contactArr.put(jsonObject5);
//
//            objectJSon.put("contact",contactArr);
//            objectJSon.put("password", "");
//
//
//            Log.v(TAG, "onCreate: " + objectJSon.toString());
//
//           postDataParams=  objectJSon;
//           String ex =new SendPostRequest().execute(url,action).get();
//
//            Log.v("Ex",ex);
//
//           JSONObject obj=new JSONObject(ex);
//            String userID=obj.getString("_id");
//            boolean b=obj.getBoolean("active");
//            String active="";
//            if(b){
//                active="true";
//
//            }else{
//                active="false";
//            }
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//
//            String currentDateandTime = sdf.format(new Date());
//
//            prefManager.selectedUserInfo(userID,6,fname,gender,false);
//
//            batchDummy.clear();
//            batchDummy.add(ContentProviderOperation.newUpdate(DatabaseContract.TaskColumns.CONTENT_FAMILY)
//                    .withSelection(DatabaseContract.TaskColumns.ID + "='" + id + "'", null)
//                    .withValue(DatabaseContract.TaskColumns.ID,id)
//                    .withValue(DatabaseContract.TaskColumns.USER_ID, userID)
//                    .withValue(DatabaseContract.TaskColumns.STATUS,"Synced")
//                    .withValue(DatabaseContract.TaskColumns.FIRST_NAME,fname)
//                    .withValue(DatabaseContract.TaskColumns.LAST_NAME,lname)
//                    //.withValue(DatabaseContract.TaskColumns.THEME,"5")
//                    .withValue(DatabaseContract.TaskColumns.BIRTH_YEAR,byear)
//                    .withValue(DatabaseContract.TaskColumns.GENDER,gender)
//                    .withValue(DatabaseContract.TaskColumns.UPDATETIME,update)
//                    .withValue(DatabaseContract.TaskColumns.LASTSYNC,currentDateandTime)
//                    .withValue(DatabaseContract.TaskColumns.EMAIL,email)
//                    .withValue(DatabaseContract.TaskColumns.ACTIVE,active)
//
//                    .build());
//            syncResult.stats.numUpdates++;
//            resolver.applyBatch(DatabaseContract.CONTENT_AUTHORITY, batchDummy);
//            resolver.notifyChange(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,false);
//
//
//
//        } catch (JSONException e) {
//            Log.e("Error:", e.toString());
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        } catch (OperationApplicationException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * Manual force Android to perform a sync with our SyncAdapter.
//     */
//    public static void performSync() {
//
//        Bundle b = new Bundle();
//        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
//        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
//        ContentResolver.requestSync(AccountGeneral.getAccount(),
//                DatabaseContract.CONTENT_AUTHORITY, b);
//    }
//
//    public class SendPostRequest extends AsyncTask<String, Void, String> {
//
//        protected void onPreExecute() {
//
//        }
//
//        protected String doInBackground(String... arg0) {
//            String urlAdd=arg0[0];
//            String action=arg0[1];
//            String access = prefManager.getAuthorize();
//            String xid = prefManager.getXid();
//            String response=null;
//
//            Log.v("Access token",access);
//            Log.v("Xid",xid);
//            if(action.equals("insert")) {
//            response = postJsonObject(urlAdd, postDataParams, access, xid);
//            }
//            if(action.equals("update"))
//            {
//                response = postJsonObjectUPDate(urlAdd, postDataParams, access, xid);
//            }
//            if(access.equals("medicine")){
//
//            }
//
//            return response;
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
////           Log.v("onPostExecute",result.toString());
//
//
//
//        }
//    }
//
//    public String postJsonObject(String url, JSONObject addJobj,String access,String xid) {
//
//        InputStream inputStream = null;
//        String result = "";
//
//        try {
//
//            HttpClient httpClient = new DefaultHttpClient();
//
//            HttpPost httpPost = new HttpPost(url);
//
//            //System.out.println(url);
//            String json = "";
//
//            json = addJobj.toString();
//            //System.out.println(json);
//
//            StringEntity se = new StringEntity(json);
//
//            httpPost.setEntity(se);
//
//            httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-Type", "application/json");
//
//            httpPost.setHeader("Authorization", "Bearer " + access);
//            httpPost.setHeader("xid",xid);
//
//            HttpResponse httpResponse = httpClient.execute(httpPost);
//
//            inputStream = httpResponse.getEntity().getContent();
//
//            if (inputStream != null) {
//                result = convertInputStreamToString(inputStream);
//                //System.out.print(result);
//            } else {
//                result = "Did not work.";
//            }
//        } catch (Exception e) {
//            Log.e("InputStream ", e.getLocalizedMessage());
//        }
//         return result;
//    }
//
//    public String postJsonObjectUPDate(String url, JSONObject addJobj,String access,String xid) {
//
//        InputStream inputStream = null;
//        String result = "";
//
//        try {
//
//            HttpClient httpClient = new DefaultHttpClient();
//
//            HttpPut httpPut = new HttpPut(url);
//
//            //System.out.println(url);
//            String json = "";
//
//            json = addJobj.toString();
//            //System.out.println(json);
//
//            StringEntity se = new StringEntity(json);
//
//            httpPut.setEntity(se);
//
//            httpPut.setHeader("Accept", "application/json");
//            httpPut.setHeader("Content-Type", "application/json");
//
//            httpPut.setHeader("Authorization", "Bearer " + access);
//            httpPut.setHeader("xid",xid);
//
//            HttpResponse httpResponse = httpClient.execute(httpPut);
//
//            inputStream = httpResponse.getEntity().getContent();
//
//            if (inputStream != null) {
//                result = convertInputStreamToString(inputStream);
//                //System.out.print(result);
//            } else {
//                result = "Did not work.";
//            }
//        } catch (Exception e) {
//            Log.e("InputStream ", e.getLocalizedMessage());
//        }
//        return result;
//    }
//
//    private String convertInputStreamToString(InputStream inputStream) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//        String line = "";
//        String result = "";
//        while ((line = bufferedReader.readLine()) != null)
//            result += line;
//        inputStream.close();
//        return result;
//    }
//
//    public JSONObject FetchData(JSONObject jsonObj,final String id)
//    {
//        String userID=prefManager.getUser();
//        String urlAdd=liveAPI.family+userID;
//        Log.v("URL",urlAdd);
//
//        Log.v("POST JSONOBJECT",jsonObj.toString());
//        String access=prefManager.getAuthorize();
//        String xid=prefManager.getXid();
//
//        //String userxid=prefManager.getUserXID();
//        String userxid=prefManager.getXid();
//
//
//        Log.v("Authorize","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+access);
//        Log.v("xid","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+userxid);
//
//        AndroidNetworking.post(urlAdd)
//                .addJSONObjectBody(jsonObj)
//                .addHeaders("Content-Type", "application/json")
//                .addHeaders("Accpet", "application/json")
//                .addHeaders("Authorization", "Bearer " + access)
//                .addHeaders("xid",xid)
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        jsonObject1 = response;
//                        try {
//                            Log.v("Result", response.toString());
//                            String userID=response.getString("_id");
//
//                            batchDummy.clear();
//                            batchDummy.add(ContentProviderOperation.newUpdate(DatabaseContract.TaskColumns.CONTENT_FAMILY)
//                                    .withSelection(DatabaseContract.TaskColumns.ID + "='" + id + "'", null)
//                                    .withValue(DatabaseContract.TaskColumns.ID,id)
//                                    .withValue(DatabaseContract.TaskColumns.USER_ID, userID)
//                                    .withValue(DatabaseContract.TaskColumns.STATUS,"Synced")
//                                    .build());
//                            syncResult.stats.numUpdates++;
//                            resolver.applyBatch(DatabaseContract.CONTENT_AUTHORITY, batchDummy);
//                            resolver.notifyChange(DatabaseContract.TaskColumns.CONTENT_FAMILY,null,false);
//
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    @Override
//                    public void onError(ANError anError) {
//                        Log.v("Error in FamilyMember",anError.toString());
//                    }
//                });
//        return jsonObject1;
//
//    }

}
