package com.decoders.aerobit.Family;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.R;
import com.decoders.aerobit.ThemeActivity;
import com.decoders.aerobit.Utility.ThemeSelection;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AddFamilyGenderActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = AddFamilyGenderActivity.class.getName();

    private Button btnsave;
    private LinearLayout linearmale, linearfemale, male, female;
    private TextView textfemale, textmale, dropName;
    private ImageView blurImage, dropImage;
    private RelativeLayout themeselection;

    private PersonService mPersonService;
    private PreferenceService mPreferenceService;

    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;

    private TextView feedbackMsg;

    private String randomUUIDString, currentDateandTime, gender;
    private SimpleDateFormat sdf;
    private String color_code;

    private static final String GENDER_MALE="male";
    private static final String GENDER_FEMALE="female";

    private static final String GENDER_EMPTY="empty";
    private static final String PERSON_REF_DATA="person";
    private static final String FAMILY_REF_DATA="familyMember";

    private Person familyMember;
    private Person person;

    /*String[] themename = {"#FFFFFF", "#FF4081", "#FF3498E4", "#FF6CCC6D",
            "#FFDA9F2F", "#FFD23737", "#FF614EDA", "#FF8D3CBF", "#FF35CCB5"};*/
    String[] themename = {"White", "Skyblue", "Green", "Lightgreen", "Yellow", "Pink", "Blurblue", "Red", "Darkblue"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addfamily_gender);
        InitiVews();                  // initialize all views
        Bundle b = getIntent().getExtras();
        if (b != null) {
            System.out.println("I am restoring the person");
            familyMember = (Person) getIntent().getExtras()
                    .getSerializable(FAMILY_REF_DATA);
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            System.out.println("I am restoring the person s "+person);
            System.out.println("I am restoring the family s "+familyMember);
            getBundle(getIntent());
        }
        mPersonService = PersonService.getInstance();
        mPreferenceService = PreferenceService.getInstance();

        toolbarInfo();                // toolbar title information
        checkGender();                // retain gender
    }

    public void InitiVews() {

        btnsave = (Button) findViewById(R.id.btnsave);
        textfemale = (TextView) findViewById(R.id.textv3);
        textmale = (TextView) findViewById(R.id.textv4);

        linearmale = (LinearLayout) findViewById(R.id.gender_male);
        linearfemale = (LinearLayout) findViewById(R.id.gender_female);
        female = (LinearLayout) findViewById(R.id.radifemale);
        male = (LinearLayout) findViewById(R.id.radimale);
        dropImage = (ImageView) findViewById(R.id.dropimage);
        dropName = (TextView) findViewById(R.id.dropname);
        themeselection = (RelativeLayout) findViewById(R.id.themeselection);

        mToolbar=(Toolbar)findViewById(R.id.registerFamilyToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        parentRelative.setVisibility(View.VISIBLE);
        feedbackMsg = (TextView) findViewById(R.id.textv8);
        feedbackMsg.setVisibility(View.INVISIBLE);

        btnsave.setOnClickListener(this);
        linearmale.setOnClickListener(this);
        linearfemale.setOnClickListener(this);
        themeselection.setOnClickListener(this);

    }

    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Add Family Member");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                AddFamilyGenderActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        Intent intentParent = new Intent(getApplicationContext(),AddFamilyNameActivity.class);
        intentParent.putExtra(FAMILY_REF_DATA, familyMember);
        intentParent.putExtra(PERSON_REF_DATA, person);
        intentParent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentParent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnsave: {
                //insert family data into local db
                //System.out.println("gender value "+gender);
                if(GENDER_EMPTY.equalsIgnoreCase(gender)){
                    //System.out.println("No gender selected. Returning back to screen");
                    feedbackMsg.setVisibility(View.VISIBLE);
                    break;
                }else{
                    feedbackMsg.setVisibility(View.INVISIBLE);
                    // here the famil member data should be saved to DB
                    System.out.println("In the flow before retuning to screen === "+PrefManager.getInstance().getGuardianPerson(getApplicationContext()));
                    System.out.println("In the flow before retuning to screen === "+person);
                    Preferences preferences = new Preferences();
                    preferences.setAvatarColor(color_code);
                    familyMember.setGuardian(PrefManager.getInstance().getGuardianPerson(getApplicationContext()));
                    familyMember.setId(AerobitUtil.genUUID());
                    familyMember.setActive(true);
                    familyMember.setCreatedDate(new Date());
                    familyMember.setThemeColor(color_code);
                    familyMember = PersonService.getInstance().createPerson(getContentResolver(), familyMember);
                    System.out.println("In family -- guardian "+familyMember.getGuardian());

                    PrefManager.getInstance().setSelectedUser(getApplicationContext(), familyMember);

                    //Save Preference;
                    preferences.setPerformer(familyMember);
                    PreferenceService.getInstance().createPreference(getContentResolver(), preferences);


                    //navigate dashboard screen
                    Intent intent = new Intent(getApplicationContext(), BottomNavigation.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(PERSON_REF_DATA, person);
                    intent.putExtra(FAMILY_REF_DATA, familyMember);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    break;
                }
            }
            case R.id.gender_male: {
                female.setBackgroundResource(R.drawable.femaleblur);
                male.setBackgroundResource(R.drawable.male);
                textfemale.setTextColor(Color.BLACK);
                textmale.setTextColor(Color.GRAY);
                gender = GENDER_MALE;
                feedbackMsg.setVisibility(View.INVISIBLE);
                male.setPressed(true);
                familyMember.setGender("male");
               // setGenderBundle();
                break;
            }
            case R.id.gender_female: {
                male.setBackgroundResource(R.drawable.maleblur);
                female.setBackgroundResource(R.drawable.female);
                textmale.setTextColor(Color.BLACK);
                textfemale.setTextColor(Color.GRAY);
                gender = GENDER_FEMALE;
                feedbackMsg.setVisibility(View.INVISIBLE);
                familyMember.setGender("female");
               // setGenderBundle();
                break;
            }
            case R.id.themeselection: {
                Intent intent = new Intent(getApplicationContext(), ThemeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("screen", "family");
                if(gender==null){
                    gender = "empty";
                }
                familyMember.setGender(gender);
                intent.putExtra(FAMILY_REF_DATA, familyMember);
                intent.putExtra(PERSON_REF_DATA, person);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            }
        }
    }

    private void checkGender() {


        if (gender.equals(GENDER_MALE)) {
            female.setBackgroundResource(R.drawable.femaleblur);
            male.setBackgroundResource(R.drawable.male);
            textfemale.setTextColor(Color.BLACK);
            textmale.setTextColor(Color.GRAY);
            gender = GENDER_MALE;
        }
        if (gender.equals(GENDER_FEMALE)) {
            male.setBackgroundResource(R.drawable.maleblur);
            female.setBackgroundResource(R.drawable.female);
            textmale.setTextColor(Color.BLACK);
            //textfemale.setTextColor(Color.BLACK);
            textfemale.setTextColor(Color.GRAY);
            gender = GENDER_FEMALE;
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK) {
            getBundle(data);
        }
    }
    void getBundle(Intent intent){
        familyMember = (Person) getIntent().getExtras()
                .getSerializable(FAMILY_REF_DATA);
        if(familyMember!=null){
            gender = familyMember.getGender();
            if(gender == null){
                gender = "empty";
            }
            String code=familyMember.getThemeColor();
            if(code!=null){
                color_code=code;
            }else{
                color_code="#FF4081";
            }
            if (gender.equals("male")) {
                    gender = "male";
                    female.setBackgroundResource(R.drawable.femaleblur);
                    male.setBackgroundResource(R.drawable.male);
                    textfemale.setTextColor(Color.BLACK);
                    textmale.setTextColor(Color.GRAY);
            }
            if (gender.equals("female")) {
                    gender = "female";
                    male.setBackgroundResource(R.drawable.maleblur);
                    female.setBackgroundResource(R.drawable.female);
                    textmale.setTextColor(Color.BLACK);
                    textfemale.setTextColor(Color.GRAY);
            }
        }else{
            color_code="#FF4081";
            ThemeSelection.setImage(dropImage,"#FF4081");
            gender="empty";
        }

    }



}
