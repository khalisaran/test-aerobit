package com.decoders.aerobit.MedicationScreen;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.util.AerobitUtil;
import com.decoders.aerobit.R;

public class DoseTimeActivity extends AppCompatActivity {

    private RelativeLayout  crossbtn;
    private TimePicker timePicker;
    private Button selectTime;
    private String getCarePlan;
    int hour;

    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;
    private ImageView imageView;

      private int hours, minutes;
      private String type;
      private String timeFormatted = "";
      private CarePlan carePlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dose_time);

        initViews();
        toolbarInfo();

        final Bundle extras = getIntent().getExtras();
        getCarePlan = extras.getString("carePlan");
        type = extras.getString("type");
        carePlan = (CarePlan) AerobitUtil.fromJson(getCarePlan, CarePlan.class);
        crossbtn = (RelativeLayout) findViewById(R.id.navBackbutton5);
        timePicker = (TimePicker)findViewById(R.id.TimePicker);
        selectTime = (Button)findViewById(R.id.selectTime);

        timePicker.setIs24HourView(false);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hours = hourOfDay;
                minutes = minute;
                hour = hours % 12;
                if (hour == 0) {
                    hour = 12;
                }
                timeFormatted = String.format("%02d:%02d %s", hour, minutes,
                        hours < 12 ? "AM" : "PM");
            }
        });

        hours = timePicker.getHour();
        minutes = timePicker.getMinute();

        hour = hours % 12;
        if (hour == 0) {
            hour = 12;
        }
        timeFormatted = String.format("%02d:%02d %s", hour, minutes,
                hours < 12 ? "AM" : "PM");

        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carePlan.dosages.add(timeFormatted);
                Intent intent = new Intent(getApplicationContext(), InhalerDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("carePlan", AerobitUtil.toJson(carePlan));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("activityName", "doseTime");
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
            }
        });

        crossbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), SelectMedicationTypeActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                intent.putExtra("UserID",carePlan.getPerformer().getId());
//                startActivity(intent);
//                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                DoseTimeActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();

            }
        });
    }

    private void initViews() {
        mToolbar=(Toolbar)findViewById(R.id.createAccountToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.editMedText);
        imageView=(ImageView)findViewById(R.id.toolbarClose);
        imageView.setVisibility(View.VISIBLE);
        parentRelative.setVisibility(View.VISIBLE);

    }
    private void toolbarInfo() {

       toolbarText.setVisibility(View.VISIBLE);
        mToolbar.setTitle("");
        toolbarText.setText("Dose Time");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                DoseTimeActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }
}
