package com.decoders.aerobit.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decoders.aerobit.R;

/**
 * Created by 10decoders on 3/2/18.
 */

public class DashboardImage extends Fragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dashboard_imagefragment, container, false);
        return view;
    }
}
