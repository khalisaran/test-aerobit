package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.decoders.aerobit.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList years;
    LayoutInflater inflter;

    public CustomAdapter(Context applicationContext, ArrayList years) {
        this.context = applicationContext;
        this.years = years;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {

        return years.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_custom_adapter, null);
        TextView names = (TextView) view.findViewById(R.id.genderid);
        names.setText(String.valueOf(years.get(i)));
        return view;
    }
}