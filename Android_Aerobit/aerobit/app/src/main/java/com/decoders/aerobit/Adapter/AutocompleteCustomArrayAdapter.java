package com.decoders.aerobit.Adapter;

import android.content.Context;

/**
 * Created by Saravanakumar on 3/7/2018.
 */

import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aerobit.medapp.model.local.Medication;
import com.decoders.aerobit.MedicationScreen.InhalerDetailsActivity;
import com.decoders.aerobit.R;

import java.util.List;

public class AutocompleteCustomArrayAdapter extends ArrayAdapter<Medication> {

    Context mContext;
    int layoutResourceId;
    List<Medication> medicationList = null;
    private AutoListAdapterListener mListener;
    private int count;
    Medication medication;
    private String text;

    public interface AutoListAdapterListener {
        void onReceiveUser(Medication person);
    }

    public AutocompleteCustomArrayAdapter(Context mContext, int layoutResourceId, List<Medication> medication,
                                          AutoListAdapterListener listAdapterListener, int count,String text) {
        super(mContext, layoutResourceId, medication);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.medicationList = medication;
        mListener=listAdapterListener;
        this.count = count;
        this.text = text;
    }

    public AutocompleteCustomArrayAdapter(Context mContext, int layoutResourceId, List<Medication> medication,
                                          AutoListAdapterListener listAdapterListener) {
        super(mContext, layoutResourceId, medication);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.medicationList = medication;
        mListener=listAdapterListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((InhalerDetailsActivity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            medication=new Medication();
            medication = medicationList.get(position);

            TextView name = (TextView) convertView.findViewById(R.id.autoCompleteMedName);
            TextView strength = (TextView) convertView.findViewById(R.id.autoCompleteStrength);

            String medName = medication.getName().toLowerCase().replace(text, "<b>" + text + "</b>");
            name.setText(Html.fromHtml(medName));
            strength.setText(medication.getStrength());

        } catch (Exception e) {
            e.printStackTrace();
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medication = medicationList.get(position);
                mListener.onReceiveUser(medication);
            }
        });
        return convertView;
    }

}