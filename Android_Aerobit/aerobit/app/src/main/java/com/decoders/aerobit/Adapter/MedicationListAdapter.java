package com.decoders.aerobit.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.util.AerobitUtil;
import com.decoders.aerobit.R;
import com.decoders.aerobit.CustomComponents.CustomBottomSheetDialogFragment;

import java.util.List;

/**
 * Created by 10DECODERS on 2/19/2018.
 */

public class MedicationListAdapter extends RecyclerView.Adapter<MedicationListAdapter.MyViewHolder> {

    private List<CarePlan> carePlanModelList;
    private Context context;
    CustomBottomSheetDialogFragment.OnDissmissListener listener;
    CarePlan carePlanModel;

    public MedicationListAdapter(List<CarePlan> carePlanModelList, CustomBottomSheetDialogFragment.OnDissmissListener listener) {
        this.listener = listener;
        this.carePlanModelList = carePlanModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medication_list_card_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        context = holder.itemView.getContext();
        carePlanModel = carePlanModelList.get(position);
        Medication objMedication=carePlanModel.getMedication();
        if(carePlanModel.getMedication().getType().equals("TABLET")) {
            holder.title.setText(objMedication.getName() + " " + objMedication.getStrength());
        } else {
            holder.title.setText(objMedication.getName());
        }

        holder.lastSync.setText(carePlanModel.getSyncStatus());
        holder.thumbnail.setVisibility(View.VISIBLE);

        holder.morning.setText(getFormatedDoseTimes(carePlanModel));

        Log.v("Activity",objMedication.getName()+objMedication.getType());

        holder.thumbnail.setImageResource(getIcon(objMedication.getName(),objMedication.getType()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CarePlan objCarePlan= carePlanModelList.get(position);
                String selectedCarePlan= AerobitUtil.toJson(objCarePlan);
                try {
                   BottomSheetDialogFragment bottomSheetDialogFragment = new CustomBottomSheetDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("carePlan",selectedCarePlan);
                    bottomSheetDialogFragment.setArguments(bundle);
                    listener.doShow(bundle);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return carePlanModelList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, lastSync, morning, evening;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.titleInhaler);
            lastSync = (TextView) view.findViewById(R.id.lastSync);
            morning = (TextView) view.findViewById(R.id.morning);
            evening = (TextView) view.findViewById(R.id.evening);
            thumbnail = (ImageView) view.findViewById(R.id.inhalerImage);
        }
    }

    public String getFormatedDoseTimes(CarePlan mCarePlay) {
        StringBuffer formatedTime = new StringBuffer();
        for (String strtimings : mCarePlay.dosages) {
            formatedTime.append(strtimings).append("  ");
        }
        return formatedTime.toString();
    }

    private int getIcon(String medName,String type) {
        int returnVal=0;
        if (type.equals("TABLET")) {
            returnVal=R.drawable.tablet;;
        } else if (type.equals("SYRUP")) {
            returnVal= R.drawable.syrup;
        } else if (type.equals("INHALER")) {
            medName = medName.replace(" ", "");
            if (medName.equals("Turbohaler")) {
                returnVal= R.drawable.beclate;
            } else if (medName.equals("SmartHaler")) {
                returnVal= R.drawable.group_25;
            } else if (medName.equals("Diskus")) {
                returnVal= R.drawable.group_9_copy_6;
            } else if (medName.equals("Ellipta")) {
                returnVal= R.drawable.group_9_copy_5;
            } else if (medName.equals("SeebriNeohaler")) {
                returnVal= R.drawable.group_12_copy_2;
            } else if (medName.equals("SpirivaCombo")) {
                returnVal= R.drawable.group_10_copy;
            } else if (medName.equals("Respimat")) {
                returnVal= R.drawable.group_24_copy_2;
            }
        }
        return returnVal;
    }

}
