package com.decoders.aerobit.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.BottomNavigation;
import com.decoders.aerobit.MedicationScreen.SelectMedicationTypeActivity;
import com.decoders.aerobit.R;

import java.io.ByteArrayOutputStream;

/**
 * Created by 10DECODERS on 2/22/2018.
 */

public class DashboardFragment extends Fragment {

    Button addMedicationButton;
    BottomNavigation instance;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        addMedicationButton = (Button) view.findViewById(R.id.addmedicationbtn);
        final String medicatin = "editMedication";
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.medicationinhalers);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        final byte[] byteArray = stream.toByteArray();
        instance=BottomNavigation.getInstance();

        addMedicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SelectMedicationTypeActivity.class);
                intent.putExtra("UserID",PrefManager.getInstance().getCurrentUser(getActivity().getApplicationContext()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }
}
