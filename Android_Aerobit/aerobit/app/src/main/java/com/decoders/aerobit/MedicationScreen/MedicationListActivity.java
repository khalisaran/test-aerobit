package com.decoders.aerobit.MedicationScreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.decoders.aerobit.Adapter.DataAdapter;
import com.decoders.aerobit.R;

import java.util.ArrayList;
import java.util.Arrays;

public class MedicationListActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {



    String[] username = {"Julie","Dinesh"};
    int[] userimage = {R.drawable.female, R.drawable.male};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicationlist);

      //  CustomSpinner customSpinner = new CustomSpinner(getApplicationContext(), userimage, username);
      //  spinner.setAdapter(customSpinner);

        ArrayList personNames = new ArrayList<>(Arrays.asList("Symbicort 250mg"));
        ArrayList personImages = new ArrayList<>(Arrays.asList(R.drawable.ic_smalldose));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleviewid);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        DataAdapter customAdapter = new DataAdapter(MedicationListActivity.this, personNames,personImages);
        recyclerView.setAdapter(customAdapter);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
