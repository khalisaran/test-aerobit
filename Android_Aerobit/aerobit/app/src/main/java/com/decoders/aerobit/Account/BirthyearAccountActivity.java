package com.decoders.aerobit.Account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.PrefManager;
import com.decoders.aerobit.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BirthyearAccountActivity extends AppCompatActivity  {


    private Button btns;
    private EditText EditFname,EditLname,userDOB;
    private  TextView headertext, textbirth,bottomtext1,bottomtext2;
    private Typeface font,font2;
    private TextInputLayout FirstnameLayout,LastnameLayout,DOBLayout;
    private Toolbar mToolbar;
    private RelativeLayout mBackNavigation,parentRelative;
    private TextView toolbarText;
    private int num1;

    private static final String PERSON_REF_DATA="person";
    private static final String FIRST_NAME_BLANK="First name can not be blank";
    private static final String LAST_NAME_BLANK="Last name can not be blank";
    private static final String YEAR_INCORRECT_MSG="Invalid year";


    private Person person;

    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthyear_account);

        prefManager=PrefManager.getInstance();

        initViews();
        toolbarInfo();
        fontInitialize();
        hideSoftKeyboard();
        //User previsouly entered data

        Bundle b = getIntent().getExtras();
        if (b != null) {
            person = (Person) getIntent().getExtras()
                    .getSerializable(PERSON_REF_DATA);
            if(person!=null){
                EditLname.setText(person.getLastName());
                EditFname.setText(person.getFirstName());
                userDOB.setText(person.getBirthDate());
            }
        }
        EditFname.setFilters(new InputFilter[]{getEditTextFilter()});
        EditLname.setFilters(new InputFilter[]{getEditTextFilter()});
        EditFname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                FirstnameLayout.setError(null);
                EditFname.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            FirstnameLayout.setError(null);
            }
        });
        EditLname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                LastnameLayout.setError(null);
                EditLname.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                LastnameLayout.setError(null);
            }
        });
        userDOB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                DOBLayout.setError(null);
                userDOB.setCompoundDrawables(null,null,null,null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                DOBLayout.setError(null);
            }
        });

    }

    //Onclick method for button click
    public void onClickNext(View v) {
        boolean firstname;
        boolean lastname;
        if (EditFname.getText().toString().equals("")) {
            FirstnameLayout.setError(FIRST_NAME_BLANK);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            EditFname.setCompoundDrawables(null, null, dr, null);
        }
        if (EditLname.getText().toString().equals("")) {
            LastnameLayout.setError(LAST_NAME_BLANK);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            EditLname.setCompoundDrawables(null, null, dr, null);

        }

        if (userDOB.getText()!=null && userDOB.getText().length() != 4) {
            DOBLayout.setError(YEAR_INCORRECT_MSG);
            Drawable dr = getResources().getDrawable(R.drawable.redicon);
            //add an error icon to yur drawable files
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
            userDOB.setCompoundDrawables(null, null, dr, null);
        }

        if (EditFname.getText()!=null && !EditFname.getText().toString().equals("") && EditLname.getText()!=null && !EditLname.getText().toString().equals("") &&
                userDOB.getText()!=null && userDOB.getText().length() == 4) {
            if (userDOB.getText().length() > 0) {
                int num = Integer.parseInt(userDOB.getText().toString());
                if (num >= 1900 && num <= 2018) {

                    //save the number
                    num1 = num;
                    //Creating Bundle
                    String text = userDOB.getText().toString();

                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    Intent intent = new Intent(getApplicationContext(), GenderSelectionActivity.class);
                    person.setFirstName(EditFname.getText().toString());
                    person.setLastName(EditLname.getText().toString());
                    person.setBirthDate(userDOB.getText().toString());
                    intent.putExtra("gender", "empty");

                    intent.putExtra(PERSON_REF_DATA, person);
                    //bundle creation
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivityForResult(intent,1);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Toast.makeText(BirthyearAccountActivity.this, "Range should be 1900-2018", Toast.LENGTH_SHORT).show();
                    userDOB.setText("");
                    num1 = -1;
                    DOBLayout.setError(YEAR_INCORRECT_MSG);
                    Drawable dr = getResources().getDrawable(R.drawable.redicon);
                    //add an error icon to yur drawable files
                    dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
                    userDOB.setCompoundDrawables(null, null, dr, null);
                }
            }
        }
    }

    private void fontInitialize() {
        font = Typeface.createFromAsset(this.getAssets(),"Font/Avenir Heavy.otf");
        font2 = Typeface.createFromAsset(this.getAssets(),"Font/Avenir Roman.otf");
        textbirth.setTypeface(font2);
        bottomtext2.setTypeface(font2);
        bottomtext1.setTypeface(font2);
        btns.setTypeface(font2);
    }

    // NOTE: Difference this is a custom method - not the one for back button
    @Override
    public void onBackPressed() {

        System.out.println("Before returning fname -- "+person.getFirstName());
        Intent intentParent = new Intent(getApplicationContext(),CreateAccountActivity.class);
        // send back the serialized person data
        if(person!=null){
            person.setFirstName(EditFname.getText().toString());
            person.setLastName(EditLname.getText().toString());
            person.setBirthDate(userDOB.getText().toString());
        }
        intentParent.putExtra(PERSON_REF_DATA, person);
        intentParent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentParent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public static InputFilter getEditTextFilter() {
        return new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
                Matcher ms = ps.matcher(String.valueOf(c));
                return ms.matches();
            }
        };
    }

    private void initViews(){
        userDOB = (EditText) findViewById(R.id.userYear);
        EditFname=(EditText) findViewById(R.id.edtbirthyr);
        EditLname=(EditText)findViewById(R.id.edtbirthyr1);
        textbirth=(TextView)findViewById(R.id.textbirth);
        bottomtext1=(TextView)findViewById(R.id.bottomtext1);
        bottomtext2=(TextView)findViewById(R.id.bottomtext2);
        btns = (Button) findViewById(R.id.btnbirthyr);
        FirstnameLayout=(TextInputLayout)findViewById(R.id.FirstnameLayout);
        LastnameLayout=(TextInputLayout)findViewById(R.id.LastnameLayout);
        mToolbar=(Toolbar)findViewById(R.id.familyToolbar);
        mBackNavigation=(RelativeLayout)findViewById(R.id.navBackbutton);
        parentRelative=(RelativeLayout)findViewById(R.id.relativeBack);
        toolbarText=(TextView)findViewById(R.id.toolbarText);
        DOBLayout = (TextInputLayout)findViewById(R.id.genderBirthYear);
        parentRelative.setVisibility(View.VISIBLE);

    }
    private void toolbarInfo()
    {
        mToolbar.setTitle("");
        toolbarText.setText("Create Account");
        mBackNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                BirthyearAccountActivity.super.onBackPressed();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                return false;
            }
        });

       userDOB.setOnEditorActionListener(new TextView.OnEditorActionListener() {
           @Override
           public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

               if (actionId == EditorInfo.IME_ACTION_DONE){
                   onClickNext(v);

                   return true;
               }
               return false;
           }
       });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            System.out.println("RESULT RESTORING");
            if (resultCode == Activity.RESULT_OK) {
                Bundle b = data.getExtras();
                if (b != null) {
                    Person person = (Person) b.getSerializable(PERSON_REF_DATA);
                    System.out.println("email : " + person.getEmail()+ "name "+person.getFirstName());
                    userDOB.setText(person.getBirthDate());
                    EditFname.setText(person.getFirstName());
                    EditLname.setText(person.getLastName());
                }
            } else if (resultCode == 0) {
                System.out.println("RESULT CANCELLED");
            }
        }
    }
    public void hideSoftKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

}

