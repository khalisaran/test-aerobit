package com.decoders.aerobit.CustomComponents;

/**
 * Created by 10DECODERS on 2/27/2018.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.decoders.aerobit.MedicationScreen.InhalerDetailsActivity;
import com.decoders.aerobit.R;

import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final List<String> itemname;
    InhalerDetailsActivity instance;

    public CustomListAdapter(Activity context, List<String> itemname) {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.timeList);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.crossIcon);
        instance= InhalerDetailsActivity.getInstance();

        txtTitle.setText(itemname.get(position));
        imageView.setImageResource(R.drawable.roundcross);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = itemname.get(position);
                instance.getScheduledTimes().remove(item);
                instance.adapter.notifyDataSetChanged();
                itemname.remove(position);
                notifyDataSetChanged();
            }
        });
        rowView.setTag(imageView);
        return rowView;
    }
}
