package com.aerobit.medapp.model.remote;

import com.aerobit.medapp.model.local.Gender;

import java.util.Date;
import java.util.List;

/**
 * Created by ashwin on 1/3/18.
 */

public class Person {
    private Text text;

    private List<Identifier> identifier;

    private Name name;

    private String password;

    private String type;

    private List<Telecom> telecom;

    private String gender;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String birthDate;

    private Boolean active;

    private List<Address> address;

    private Photo photo;

    private List<Link> link;

    private String status;

    private List<Contact> contact;

    public void setText(Text text) {
        this.text = text;
    }

    public Text getText() {
        return this.text;
    }

    public void setIdentifier(List<Identifier> identifier) {
        this.identifier = identifier;
    }

    public List<Identifier> getIdentifier() {
        return this.identifier;
    }

    public Boolean getActive() {
        return active;
    }
    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(List<Link> link) {
        this.link = link;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Name getName() {
        return this.name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setTelecom(List<Telecom> telecom) {
        this.telecom = telecom;
    }

    public List<Telecom> getTelecom() {
        return this.telecom;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDate() {
        return this.birthDate;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public List<Address> getAddress() {
        return this.address;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Photo getPhoto() {
        return this.photo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setContact(List<Contact> contact) {
        this.contact = contact;
    }

    public List<Contact> getContact() {
        return this.contact;
    }

    private Date createdAt;

    private Date updatedAt;

    public String _id;

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "text=" + text +
                ", identifier=" + identifier +
                ", name=" + name +
                ", type='" + type + '\'' +
                ", telecom=" + telecom +
                ", gender=" + gender +
                ", birthDate='" + birthDate + '\'' +
                ", active=" + active +
                ", address=" + address +
                ", photo=" + photo +
                ", relatedPerson=" + link +
                ", status='" + status + '\'' +
                ", contact=" + contact +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", _id='" + _id + '\'' +
                '}';
    }
}
