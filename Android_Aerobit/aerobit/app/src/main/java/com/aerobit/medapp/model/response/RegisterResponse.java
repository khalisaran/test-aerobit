package com.aerobit.medapp.model.response;

/**
 * Created by thomas on 12/03/18.
 */

public class RegisterResponse {

    private String email;
    private String type;
    private String userId;
    private String xid;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }
}
