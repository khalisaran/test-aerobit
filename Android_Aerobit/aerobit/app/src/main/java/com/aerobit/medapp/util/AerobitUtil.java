package com.aerobit.medapp.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.util.UUID;

/**
 * Created by edrinthomas on 02/03/18.
 */

public class AerobitUtil {

    public static final String DEFAULT_THEME_COLOR = "#FF4081";

    private static Gson gson = new GsonBuilder().create();

    public static Object fromJson(String json, Type classType) {
        return gson.fromJson(json, classType);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    public static String genUUID() {
        return UUID.randomUUID().toString();
    }

    public static String getIdFromURL(String uriStr) {
        if (uriStr == null) return null;
        String result[] = uriStr.split("#");
        return result[result.length - 1];
    }

    public static String findHyphenPresentOrNot(String identifierStr) {
        if (identifierStr.contains("-")) {
            return "1";
        } else {
            return "0";
        }
    }
}
