package com.aerobit.medapp.mapper;

import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.remote.Address;
import com.aerobit.medapp.model.remote.Avatar;
import com.aerobit.medapp.model.remote.Evening;
import com.aerobit.medapp.model.remote.MedicationRemainder;
import com.aerobit.medapp.model.remote.Morning;
import com.aerobit.medapp.model.remote.Name;
import com.aerobit.medapp.model.remote.Notification;
import com.aerobit.medapp.model.remote.Person;
import com.aerobit.medapp.model.remote.Preference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by thomas on 04/03/18.
 */

public class PreferencesDataMapper {

    private static PreferencesDataMapper instance;

    private PreferencesDataMapper(){}

    public static PreferencesDataMapper getInstance(){

        if(instance == null){
            instance = new PreferencesDataMapper();
        }
        return instance;
    }

    public Preferences transform(com.aerobit.medapp.model.remote.Preference preferenceRemote) {

        Preferences preference = null;
        if(preferenceRemote != null){

            preference = new Preferences();
            preference.setId(preferenceRemote.get_id());
            preference.setActive(preferenceRemote.getActive());
            //preference.setTheme(preferenceRemote.getTheme());
            preference.setAvatarColor(preferenceRemote.getAvatar().getColor());
            preference.setAvatarImage(preferenceRemote.getAvatar().getImage());
            preference.setCreatedDate(preferenceRemote.getCreatedAt());
            preference.setUpdatedDate(preferenceRemote.getUpdatedAt());
            preference.setEveningenable(preferenceRemote.getMedicationRemainder().getEvening().getEnable());
            preference.setEveningtime(preferenceRemote.getMedicationRemainder().getEvening().getTime());
            preference.setMorningenable(preferenceRemote.getMedicationRemainder().getMorning().getEnable());
            preference.setMorningtime(preferenceRemote.getMedicationRemainder().getMorning().getTime());
            preference.setLed(preferenceRemote.getNotification().getLed());
            preference.setNeedEmail(preferenceRemote.getNotification().getNeedEmail());
            preference.setSound(preferenceRemote.getNotification().getSound());
            preference.setVibrate(preferenceRemote.getNotification().getVibrate());
            preference.setMaxPerMed(preferenceRemote.getMedicationRemainder().getMaxPerMed());
            preference.setSnooze(preferenceRemote.getMedicationRemainder().getSnooze());
            preference.setPopupNotification(preferenceRemote.getMedicationRemainder().getPopupNotification());
            preference.setPopupOnLastRemainder(preferenceRemote.getMedicationRemainder().getPopupOnLastRemainder());
            preference.setShowMedName(preferenceRemote.getMedicationRemainder().getShowMedName());
            preference.setShakeToTake(preferenceRemote.getMedicationRemainder().getShakeToTake());
//            preference.setPerformer(preferenceRemote.getPerformer().get_id())
            if(preferenceRemote.getEvening() != null){

                preference.setEveningenable(preferenceRemote.getEvening().getEnable());
                preference.setEveningtime(preferenceRemote.getEvening().getTime());
            }
            if(preferenceRemote.getMorning() != null){

                preference.setMorningenable(preferenceRemote.getMorning().getEnable());
                preference.setMorningtime(preferenceRemote.getMorning().getTime());
            }
        }

        return preference;
    }

    public com.aerobit.medapp.model.remote.Preference transformToRemote(Preferences preference){

        com.aerobit.medapp.model.remote.Preference remotepreference = new Preference();

        if(preference != null){

            remotepreference.set_id(preference.getId());
            remotepreference.setActive(preference.getActive());
            Avatar avatar = new Avatar();
            avatar.setColor(preference.getAvatarColor());
            avatar.setImage(preference.getAvatarImage());
            remotepreference.setAvatar(avatar);
            remotepreference.setCreatedAt(preference.getCreatedDate());
            MedicationRemainder medicationRemainders = new MedicationRemainder();
            medicationRemainders.setMaxPerMed(preference.getMaxPerMed());
            medicationRemainders.setPopupNotification(preference.getPopupNotification());
            medicationRemainders.setPopupOnLastRemainder(preference.getPopupOnLastRemainder());
            medicationRemainders.setShakeToTake(preference.getShakeToTake());
            medicationRemainders.setShowMedName(preference.getShowMedName());
            medicationRemainders.setSnooze(preference.getSnooze());
            remotepreference.setMedicationRemainder(medicationRemainders);
            Evening evening = new Evening();
            evening.setEnable(preference.getEveningenable());
            evening.setTime(preference.getEveningtime());
            remotepreference.setEvening(evening);
            Morning morning = new Morning();
            morning.setEnable(preference.getMorningenable());
            morning.setTime(preference.getMorningtime());
            remotepreference.setMorning(morning);
            Notification notification = new Notification();
            notification.setLed(preference.getLed());
            notification.setNeedEmail(preference.getNeedEmail());
            notification.setSound(preference.getSound());
            notification.setVibrate(preference.getVibrate());
            remotepreference.setNotification(notification);
            //remotepreference.setTheme(preference.getTheme());
            remotepreference.setUpdatedAt(preference.getUpdatedDate());
            Person performer = new Person();
            performer.set_id(preference.getPerformer().getId());
            remotepreference.setPerformer(performer);


        }

        return remotepreference;
    }

    public List<Preferences> transform(Collection<com.aerobit.medapp.model.remote.Preference> remotePreferenceCollection) {
        final List<Preferences> preferencesList = new ArrayList<Preferences>();
        for (com.aerobit.medapp.model.remote.Preference preferenceEntity : remotePreferenceCollection) {
            final Preferences preferences = transform(preferenceEntity);
            if (preferences != null) {
                preferencesList.add(preferences);
            }
        }
        return preferencesList;
    }
}
