package com.aerobit.medapp.model.local;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class CarePlan extends RealmObject {

    @PrimaryKey
    @Required
    public String id;
    public Medication medication;
    public RealmList<String> dosages;
    public String strength;
    public Boolean active;
    public Person performer;
    public Device device;
    public String category;
    public Date createdDate;
    public Date updatedDate;

    private String syncStatus;

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public CarePlan(){}

    public CarePlan(String id, Medication medication, RealmList<String> dosages, String strength, Boolean active, Person performer, Device device, String category, Date createdDate, Date updatedDate) {
        this.id = id;
        this.medication = medication;
        this.dosages = dosages;
        this.strength = strength;
        this.active = active;
        this.performer = performer;
        this.device = device;
        this.category = category;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public RealmList<String> getDosages() {
        return dosages;
    }

    public void setDosages(RealmList<String> dosages) {
        this.dosages = dosages;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Person getPerformer() {
        return performer;
    }

    public void setPerformer(Person performer) {
        this.performer = performer;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}