package com.aerobit.medapp.model.local;

/**
 * Created by edrinthomas on 04/03/18.
 */

public enum SyncStatus {

    NOT_SYNCED ("NOT_SYNCED"),
    SYNCED ("SYNCED"),
    IN_PROGRESS ("SYNCED");

    private final String value;

    private SyncStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
