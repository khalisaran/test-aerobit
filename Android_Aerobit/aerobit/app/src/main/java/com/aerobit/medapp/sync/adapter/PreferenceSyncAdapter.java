package com.aerobit.medapp.sync.adapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.OperationCanceledException;
import android.os.RemoteException;
import android.util.Log;

import com.aerobit.medapp.provider.PreferenceContentProvider;
import com.aerobit.medapp.service.remote.MedicationRequestService;
import com.aerobit.medapp.sync.authenticator.AccountGeneral;
import com.aerobit.medapp.util.PrefManager;

import java.io.IOException;

/**
 * Created by ashwin on 14/3/18.
 */

public class PreferenceSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "Preference_SYNC_ADAPTER";
    private final ContentResolver resolver;

    private com.aerobit.medapp.service.local.PreferenceService PreferenceService;

    private MedicationRequestService medicationRequestService;

    private PrefManager prefManager;

    private com.aerobit.medapp.mapper.PreferencesDataMapper PreferenceDataMapper;

    public PreferenceSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.resolver = context.getContentResolver();
    }

    public PreferenceSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.resolver = context.getContentResolver();
    }


    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        try {
            Log.v("SyncMe", "Called Sync in Preference....");
            createOrUpdatePreference(contentProviderClient);


        } catch (IOException ex) {
            Log.e(TAG, "Error synchronizing!", ex);
            syncResult.stats.numIoExceptions++;
        } catch (OperationCanceledException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createOrUpdatePreference(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {
        try {
            Log.v("daa", "aaa");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Manual force Android to perform a sync with our SyncAdapter.
     */
    public static void performSync() {
        Bundle b = new Bundle();
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(AccountGeneral.getAccount(),
                PreferenceContentProvider.CONTENT_AUTHORITY, b);
    }
}