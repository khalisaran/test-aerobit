package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.ClinicalImpression;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;

/**
 * Created by ashwin on 2/3/18.
 */

public class ClinicalImpressionRepository {

    private static ClinicalImpressionRepository instance = null;

    private ClinicalImpressionRepository() {
    }

    public static final ClinicalImpressionRepository getInstance(){

        if(instance ==null){
            instance = new ClinicalImpressionRepository();
        }
        return instance;
    }

    public List<ClinicalImpression> getAllClinicalImpressions() {

        Realm mRealm = Realm.getDefaultInstance();
        List<ClinicalImpression> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(ClinicalImpression.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return  data;
    }

    public ClinicalImpression getClinicalImpression(final int clinicalImpressionId) {
        Realm mRealm = Realm.getDefaultInstance();
        ClinicalImpression data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(ClinicalImpression.class).equalTo("id", clinicalImpressionId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public void insertClinicalImpression(ClinicalImpression clinicalImpression) {
        Realm mRealm = Realm.getDefaultInstance();
        try{
            mRealm.beginTransaction();
            if(clinicalImpression.getId() == null && clinicalImpression.getId().isEmpty())
                clinicalImpression.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(clinicalImpression);
            mRealm.commitTransaction();
        } catch (Exception e){
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }


    }
}