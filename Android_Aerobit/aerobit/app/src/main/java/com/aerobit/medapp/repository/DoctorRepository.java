package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Doctor;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class DoctorRepository {
    private static DoctorRepository instance = null;

    private DoctorRepository() {
    }

    public static final DoctorRepository getInstance(){

        if(instance ==null){
            instance = new DoctorRepository();
        }
        return instance;
    }

    public List<Doctor> getAllDoctors() {
        Realm mRealm = Realm.getDefaultInstance();
        List <Doctor> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Doctor.class).findAll());;
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Doctor getDoctor(final int doctorId) {
        Realm mRealm = Realm.getDefaultInstance();
        Doctor data = null;
        try{
            data = mRealm.where(Doctor.class).equalTo("id", doctorId).findFirst();
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }

        return data;
    }

    public void insertDoctor( Doctor doctor) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (doctor.getId() == null && doctor.getId().isEmpty())
                doctor.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(doctor);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }
    }
}