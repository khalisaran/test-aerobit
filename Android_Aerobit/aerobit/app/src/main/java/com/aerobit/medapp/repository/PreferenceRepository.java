package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Preferences;

import com.aerobit.medapp.model.remote.Preference;
import com.aerobit.medapp.util.AerobitUtil;
import java.util.List;

import io.realm.Realm;

/**
 * Created by ashwin on 4/3/18.
 */

public class PreferenceRepository {
    private static PreferenceRepository instance = null;

    private PreferenceRepository() {

    }

    public static final PreferenceRepository getInstance(){

        if(instance ==null){
            instance = new PreferenceRepository();
        }
        return instance;
    }

    public List<Preferences> getAllPreferences() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Preferences> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Preferences.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Preferences getPreference(final int preferenceId) {
        Realm mRealm = Realm.getDefaultInstance();
        Preferences data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Preferences.class).equalTo("id", preferenceId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Preferences getPreferenceByPatientId(final String performer) {
        Realm mRealm = Realm.getDefaultInstance();
        Preferences preferencesDB = null;
        try {
            preferencesDB = mRealm.where(Preferences.class).equalTo("performer.id", performer).findFirst();
            if (preferencesDB != null) {
                return mRealm.copyFromRealm(preferencesDB);
            } else {
                return new Preferences();
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return null;
    }

    public Preferences insertPreference(final Preferences preference) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (preference.getId() == null || preference.getId().isEmpty())
                preference.setId(AerobitUtil.genUUID());
            final Preferences savedPreference = mRealm.copyToRealmOrUpdate(preference);
            mRealm.commitTransaction();
            return savedPreference;
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }
        return null;
    }

}
