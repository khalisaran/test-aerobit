package com.aerobit.medapp.service.intent;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.Session;
import com.aerobit.medapp.repository.MedicationRepository;
import com.aerobit.medapp.service.local.MedicationService;
import com.aerobit.medapp.service.remote.FetchLoginDataService;
import com.aerobit.medapp.service.remote.MedicationDispenseService;
import com.aerobit.medapp.service.remote.MedicationRequestService;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.util.PrefManager;

import java.util.List;

/**
 * Created by thomas on 07/03/18.
 */

public class DownloadService extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private static final String TAG = "DownloadService";

    public DownloadService() {
        super(DownloadService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");

        receiver.send(STATUS_RUNNING, Bundle.EMPTY);

        String userId = intent.getStringExtra("userId");
        boolean loginFlow = intent.getBooleanExtra("LOGIN", false);
        String xid = PrefManager.getInstance().getAuthorize(getApplicationContext());
//        Session mSession = new Session();
//        mSession.setEmail();
//        mSession.setPassword(intent.getStringExtra("password"));
//        mSession = PersonLoadDataService.getInstance().login(mSession, PrefManager.getInstance().getAuthorize(getApplicationContext()));
//        PrefManager.getInstance().setXid(getApplicationContext(), mSession.getXid());
        if (xid != null && userId != null) {

            // load all the person data into the DB

            Person person = new Person();
            person.setId(userId);

            List<Person> allPerson;
            allPerson = FetchLoginDataService.getInstance().loadPersonData(getApplicationContext(), person);

            receiver.send(STATUS_FINISHED, Bundle.EMPTY);

            // load all Preferences for all Users

            if(loginFlow)
                FetchLoginDataService.getInstance().loadAllCarePlanData(getApplicationContext(), allPerson);

//            List<Medication> medications = MedicationDispenseService.getInstance().getAllMedicationDispense(getApplicationContext());

            // create all medications
//            MedicationRepository.getInstance().insertMedication(medications);

        } else {
            // tell the activity there is a error
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
        }

        this.stopSelf();
    }

    public class DownloadException extends Exception {

        public DownloadException(String message) {
            super(message);
        }

        public DownloadException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
