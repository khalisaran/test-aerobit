package com.aerobit.medapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.Gson;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {

    // shared pref mode
    private static final int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "DemoPref";

    private static PrefManager instance = null;

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private PrefManager() {}

    public static PrefManager getInstance(){

        if(instance == null){
            instance = new PrefManager();
        }
        return instance;
    }

    public void setGuardian(Context context, Person person){

        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).edit();
        Gson gson=new Gson();
        String json=gson.toJson(person);
        editor.putString("GUARDIAN",json);
        System.out.println("Values before saving -- "+json);
        editor.commit();
        System.out.println("Values after saving -- "+context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).getString("GUARDIAN",null));
        editor.apply();
    }
    public void setSelectedUser(Context context, Person person){

        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).edit();

        Gson gson=new Gson();
        String json=gson.toJson(person);
        editor.putString("CURRENTUSER",json);
        editor.commit();
        editor.apply();
    }
    public String getGuardian(Context context){

        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString("GUARDIAN",null);
    }

    public Person getGuardianPerson(Context context){

        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return (Person)AerobitUtil.fromJson(pref.getString("GUARDIAN",null), Person.class);
    }

    public String getCurrentUser(Context context){

        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString("CURRENTUSER",null);
    }

    public Person getCurrentUserObject(Context context){

        return (Person)AerobitUtil.fromJson(getCurrentUser(context), Person.class);
    }

    public void setXid(Context context, String xid)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).edit();
        editor.putString("xid",xid);
        editor.commit();
        editor.apply();
    }

    public String getXid(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString("xid","");
    }


    public String getToken(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString("token","");

    }

    public void Authorize(Context context, String Authorize,String time)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).edit();
        editor.putString("Authorize",Authorize);
        editor.putString("expires_in",time);
        editor.commit();

        editor.apply();
    }

//    public String getAuthorize(Context context)
//    {
//        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//        return pref.getString("Authorize","");
//
//    }
    public String getAuthorize(Context context)
    {
        return "Bearer " +context.getSharedPreferences(PREF_NAME, PRIVATE_MODE).getString("Authorize","");

    }
    public String getTimer(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        return pref.getString("expires_in","");
    }



    public Person getSelecedUserObject(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

        String personJson = pref.getString("selectedUser","");

        Person person = (Person)AerobitUtil.fromJson(personJson, Person.class);

        return person;
    }


}
