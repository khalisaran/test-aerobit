package com.aerobit.medapp.model.remote;

/**
 * Created by ashwin on 1/3/18.
 */

import java.util.List;

public class Name {
    private String use;

    private String family;

    private List<String> given;

    public void setUse(String use) {
        this.use = use;
    }

    public String getUse() {
        return this.use;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFamily() {
        return this.family;
    }

    public void setGiven(List<String> given) {
        this.given = given;
    }

    public List<String> getGiven() {
        return this.given;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}