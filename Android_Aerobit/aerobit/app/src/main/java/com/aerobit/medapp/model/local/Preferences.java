package com.aerobit.medapp.model.local;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 4/3/18.
 */

public class Preferences extends RealmObject {
    @PrimaryKey
    @Required
    public String id;
    private Person performer;
    private String maxPerMed;
    private String snooze;
    private String popupNotification;
    private String popupOnLastRemainder;
    private Boolean showMedName;
    private Boolean shakeToTake;
    private Boolean morningenable;
    private String morningtime;
    private Boolean eveningenable;
    private String eveningtime;
    private Boolean sound;
    private Boolean vibrate;
    private Boolean led;
    private Boolean needEmail;
    private String avatarImage;
    private String avatarColor;
//    private String theme;
    public Boolean active;
    private Date createdDate;
    private Date updatedDate;

    private String syncStatus;

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public Preferences(){ }


    public Preferences(String id,Person performer,String theme){
        this.id = id;
        this.performer = performer;
        //this.theme = theme;

    }

    public Preferences(String id, Person performer, String maxPerMed, String snooze, String popupNotification, String popupOnLastRemainder, Boolean showMedName, Boolean shakeToTake, Boolean morningenable, String morningtime, Boolean eveningenable, String eveningtime, Boolean sound, Boolean vibrate, Boolean led, Boolean needEmail, String avatarImage, String avatarColor, String theme, Boolean active, Date createdDate, Date updatedDate) {
        this.id = id;
        this.performer = performer;
        this.maxPerMed = maxPerMed;
        this.snooze = snooze;
        this.popupNotification = popupNotification;
        this.popupOnLastRemainder = popupOnLastRemainder;
        this.showMedName = showMedName;
        this.shakeToTake = shakeToTake;
        this.morningenable = morningenable;
        this.morningtime = morningtime;
        this.eveningenable = eveningenable;
        this.eveningtime = eveningtime;
        this.sound = sound;
        this.vibrate = vibrate;
        this.led = led;
        this.needEmail = needEmail;
        this.avatarImage = avatarImage;
        this.avatarColor = avatarColor;
        //this.theme = theme;
        this.active = active;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }


    public Boolean getActive() { return this.active; }

    public void setActive(Boolean active) { this.active = active; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Person getPerformer() {
        return performer;
    }

    public void setPerformer(Person performer) {
        this.performer = performer;
    }

    public String getMaxPerMed() {
        return maxPerMed;
    }

    public void setMaxPerMed(String maxPerMed) {
        this.maxPerMed = maxPerMed;
    }

    public String getSnooze() {
        return snooze;
    }

    public void setSnooze(String snooze) {
        this.snooze = snooze;
    }

    public String getPopupNotification() {
        return popupNotification;
    }

    public void setPopupNotification(String popupNotification) {
        this.popupNotification = popupNotification;
    }

    public String getPopupOnLastRemainder() {
        return popupOnLastRemainder;
    }

    public void setPopupOnLastRemainder(String popupOnLastRemainder) {
        this.popupOnLastRemainder = popupOnLastRemainder;
    }

    public Boolean getShowMedName() {
        return showMedName;
    }

    public void setShowMedName(Boolean showMedName) {
        this.showMedName = showMedName;
    }

    public Boolean getShakeToTake() {
        return shakeToTake;
    }

    public void setShakeToTake(Boolean shakeToTake) {
        this.shakeToTake = shakeToTake;
    }

    public Boolean getMorningenable() {
        return morningenable;
    }

    public void setMorningenable(Boolean morningenable) {
        this.morningenable = morningenable;
    }

    public String getMorningtime() {
        return morningtime;
    }

    public void setMorningtime(String morningtime) {
        this.morningtime = morningtime;
    }

    public Boolean getEveningenable() {
        return eveningenable;
    }

    public void setEveningenable(Boolean eveningenable) {
        this.eveningenable = eveningenable;
    }

    public String getEveningtime() {
        return eveningtime;
    }

    public void setEveningtime(String eveningtime) {
        this.eveningtime = eveningtime;
    }

    public Boolean getSound() {
        return sound;
    }

    public void setSound(Boolean sound) {
        this.sound = sound;
    }

    public Boolean getVibrate() {
        return vibrate;
    }

    public void setVibrate(Boolean vibrate) {
        this.vibrate = vibrate;
    }

    public Boolean getLed() {
        return led;
    }

    public void setLed(Boolean led) {
        this.led = led;
    }

    public Boolean getNeedEmail() {
        return needEmail;
    }

    public void setNeedEmail(Boolean needEmail) {
        this.needEmail = needEmail;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    public String getAvatarColor() {
        return avatarColor;
    }

    public void setAvatarColor(String avatarColor) {
        this.avatarColor = avatarColor;
    }

//    public String getTheme() {
//        return theme;
//    }
//
//    public void setTheme(String theme) {
//        this.theme = theme;
//    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "Preference{" +
                "id='" + id + '\'' +
                ", performer=" + performer +
                ", maxPerMed='" + maxPerMed + '\'' +
                ", snooze='" + snooze + '\'' +
                ", popupNotification='" + popupNotification + '\'' +
                ", popupOnLastRemainder='" + popupOnLastRemainder + '\'' +
                ", showMedName='" + showMedName + '\'' +
                ", shakeToTake='" + shakeToTake + '\'' +
                ", morningenable=" + morningenable +
                ", morningtime='" + morningtime + '\'' +
                ", eveningenable=" + eveningenable +
                ", eveningtime='" + eveningtime + '\'' +
                ", sound=" + sound +
                ", vibrate=" + vibrate +
                ", led=" + led +
                ", needEmail=" + needEmail +
                ", avatarImage='" + avatarImage + '\'' +
                ", avatarColor='" + avatarColor + '\'' +
                //", theme='" + theme + '\'' +
                ", active=" + active +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}
