package com.aerobit.medapp.model.remote;

import java.util.Date;

public class Udi
{
  private String firmwareId;

  public String getFirmwareId() { return this.firmwareId; }

  public void setFirmwareId(String firmwareId) { this.firmwareId = firmwareId; }

  private Date firmwareDate;

  public Date getFirmwareDate() { return this.firmwareDate; }

  public void setFirmwareDate(Date firmwareDate) { this.firmwareDate = firmwareDate; }

  private Date firmwareUpdateDate;

  public Date getFirmwareUpdateDate() { return this.firmwareUpdateDate; }

  public void setFirmwareUpdateDate(Date firmwareUpdateDate) { this.firmwareUpdateDate = firmwareUpdateDate; }

  private String entryType;

  public String getEntryType() { return this.entryType; }

  public void setEntryType(String entryType) { this.entryType = entryType; }

  private String name;

  public String getName() { return this.name; }

  public void setName(String name) { this.name = name; }
}