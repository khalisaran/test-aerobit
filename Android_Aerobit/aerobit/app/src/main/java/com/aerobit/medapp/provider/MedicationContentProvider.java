package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.repository.MedicationRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by ashwin on 3/3/18.
 */

public class MedicationContentProvider extends ContentProvider {

    MedicationRepository medicationRepository;
    private Realm mRealm;

    static final String authority = "com.aerobit.medapp.provider.MedicationContentProvider";
    static final String URL = "content://" + authority + "/medication";
    static final public Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int MEDICATION = 1;
    static final int MEDICATION_ID = 2;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(authority, "medication", MEDICATION);
        matcher.addURI(authority, "medication/#", MEDICATION_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {

        medicationRepository = MedicationRepository.getInstance();
        mRealm = Realm.getDefaultInstance();
        return medicationRepository == null ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        MatrixCursor myCursor = new MatrixCursor(new String[]{"MEDICATIONOBJECT"});

        Object medication = null;
        switch (sUriMatcher.match(uri)) {
            case MEDICATION:
                medication = medicationRepository.getAllMedications();
                break;

            case MEDICATION_ID:
                medication = medicationRepository.getMedication( new Integer(uri.getLastPathSegment()));
                break;

            default:
        }
        String json = AerobitUtil.toJson(medication);
        Object[] rowData = new Object[]{json};
        myCursor.addRow(rowData);
        return myCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        medicationRepository = medicationRepository.getInstance();
        List<Medication> medicationJSON = (List<Medication>) AerobitUtil.fromJson(values.get("MEDICATIONOBJECT").toString(), new TypeToken<ArrayList<Medication>>() {
        }.getType());
        try {
            medicationRepository.insertMedication(medicationJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Uri notifyURI = Uri.parse(URL+medicationJSON.getId());
        //TODO: need to notify
        getContext().getContentResolver().notifyChange(uri, null, true);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
