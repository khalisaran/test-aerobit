package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.aerobit.medapp.model.local.DoseRegister;
import com.aerobit.medapp.repository.DoseRegisterRepository;
import com.aerobit.medapp.util.AerobitUtil;

import io.realm.Realm;

/**
 * Created by ashwin on 3/3/18.
 */

public class DoseRegisterContentProvider  extends ContentProvider {

    DoseRegisterRepository doseRegisterRepository;
    private Realm mRealm;

    static final String authority = "com.aerobit.medapp.provider.DoseRegisterContentProvider";
    static final String URL = "content://" + authority + "/doseregister";
    static final public Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int DOSEREGISTER = 1;
    static final int DOSEREGISTER_ID = 2;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(authority, "doseregister" , DOSEREGISTER);
        matcher.addURI(authority, "doseregister/#", DOSEREGISTER_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {

        doseRegisterRepository = DoseRegisterRepository.getInstance();
        mRealm = Realm.getDefaultInstance();
        return doseRegisterRepository == null ? false: true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        MatrixCursor myCursor = new MatrixCursor(new String[]{"DOSEREGISTEROBJECT"});

        Object doseregister = null;
        switch (sUriMatcher.match(uri)) {
            case DOSEREGISTER:
                myCursor.addRow(doseRegisterRepository.getAllDoseRegisters());
                break;

            case DOSEREGISTER_ID:
                doseregister = doseRegisterRepository.getDoseRegister(new Integer(uri.getLastPathSegment()));
                break;

            default:
        }
        String json = AerobitUtil.toJson(doseregister);
        System.out.println(""+ json);
        Object[] rowData = new Object[]{};
        return myCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        doseRegisterRepository = doseRegisterRepository.getInstance();
        DoseRegister doseregisterJSON = (DoseRegister) AerobitUtil.fromJson(values.get("DOSEREGISTEROBJECT").toString(), DoseRegister.class);
        try {
            doseRegisterRepository.insertDoseRegister(doseregisterJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri notifyURI = Uri.parse(URL+doseregisterJSON.getId());
        getContext().getContentResolver().notifyChange(notifyURI, null);
        return notifyURI;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
