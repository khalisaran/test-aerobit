package com.aerobit.medapp.model.remote;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ashwin on 4/3/18.
 */

public class MedicationRequest {
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isActive() {
        return active;
    }
    private Device device;

    public Device getDevice() { return this.device; }

    public void setDevice(Device device) { this.device = device; }

    private Date createdAt;

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    private ArrayList<DosageInstruction> dosageInstruction;

    public ArrayList<DosageInstruction> getDosageInstruction() {
        return this.dosageInstruction;
    }

    public void setDosageInstruction(ArrayList<DosageInstruction> dosageInstruction) {
        this.dosageInstruction = dosageInstruction;
    }

    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private Identifier identifier;

    public Identifier getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    private String category;

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    private Person performer;

    public Person getPerformer() {
        return this.performer;
    }

    public void setPerformer(Person performer) {
        this.performer = performer;
    }

    private String strength;

    public String getStrength() {
        return this.strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    private MedicationDispense medicationReference;

    public MedicationDispense getMedicationReference() {
        return this.medicationReference;
    }

    public void setMedicationReference(MedicationDispense medicationReference) {
        this.medicationReference = medicationReference;
    }

    private boolean active;

    public boolean getActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private Text text;

    public Text getText() {
        return this.text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    private Date updatedAt;

    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}