package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.provider.CarePlanContentProvider;
import com.aerobit.medapp.repository.CarePlanRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by ashwin on 3/3/18.
 */

public class CarePlanService {

    private Realm mRealm;

    private static CarePlanService instance = null;

    private CarePlanService(){
    }

    public static final CarePlanService getInstance(){

        if(instance ==null){
            instance = new CarePlanService();
        }
        return instance;
    }
    private CarePlanRepository carePlanRepository;

    public CarePlan createCarePlan(ContentResolver contentResolver, CarePlan careplan){

        ContentValues values = new ContentValues();
        String careplanJSON = AerobitUtil.toJson(careplan);
        values.put("CAREPLANOBJECT", careplanJSON);
        contentResolver.insert(CarePlanContentProvider.CONTENT_URI, values);
        return careplan;
    }

    public List<CarePlan> createCarePlanListWithoutSync(ContentResolver contentResolver, List<CarePlan> careplan){

        CarePlanRepository.getInstance().insertCarePlan( careplan);
        return careplan;
    }

    public List<CarePlan> getAllCarePlan(ContentResolver contentResolver){

        String[] projection = {"CAREPLANOBJECT"};
        Cursor cursor = contentResolver.query(CarePlanContentProvider.CONTENT_URI, projection, null, null,
                null);
        String careplansJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    careplansJSON = cursor.getString(cursor.getColumnIndex("CAREPLANOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<CarePlan> carePlans = (List<CarePlan>)AerobitUtil.fromJson(careplansJSON, new TypeToken<ArrayList<CarePlan>>() {}.getType());
        return carePlans;

    }

    public List<CarePlan> getCarePlanByPatientId(ContentResolver contentResolver, String personId){

        String[] projection = {"CAREPLANOBJECT"};
        Cursor cursor = contentResolver.query(Uri.parse(CarePlanContentProvider.URL+"/patient/#"+personId), projection, null, null,
                null);
        String careplansJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    careplansJSON = cursor.getString(cursor.getColumnIndex("CAREPLANOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<CarePlan> carePlans = (List<CarePlan>)AerobitUtil.fromJson(careplansJSON, new TypeToken<ArrayList<CarePlan>>() {}.getType());
        return carePlans;

    }

    public void deactivateCarePlan(ContentResolver contentResolver, String carePlanId) {
        carePlanRepository = carePlanRepository.getInstance();
        carePlanRepository.deactivateCarePlan(carePlanId);
    }

    public List<CarePlan> getCarePlanByTypeAndPatientId(ContentResolver contentResolver, String personId, String type){
        carePlanRepository = carePlanRepository.getInstance();
        List<CarePlan> carePlans = carePlanRepository.getCarePlanByTypeAndPatientId( personId, type);

        return carePlans;

    }

    public void removeCarePlan(ContentResolver contentResolver, CarePlan careplan){

        //set value to false and update the DB
        careplan.setActive(false);

        ContentValues values = new ContentValues();
        String careplanJSON = AerobitUtil.toJson(careplan);
        values.put("CAREPLANOBJECT", careplanJSON);
        contentResolver.insert(CarePlanContentProvider.CONTENT_URI, values);

    }
}

