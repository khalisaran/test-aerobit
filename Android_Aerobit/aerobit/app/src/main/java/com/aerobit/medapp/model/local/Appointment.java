package com.aerobit.medapp.model.local;


import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class Appointment  extends RealmObject {

    @PrimaryKey
    @Required
    public String id;
    public Person performer;
    public Doctor doctor;
    public String timing;
    public Boolean active;
    public Date createdDate;
    public Date updatedDate;

    public Appointment(String id, Person performer, Doctor doctor, String timing, Boolean active, Date createdDate, Date updatedDate) {
        this.id = id;
        this.performer = performer;
        this.doctor = doctor;
        this.timing = timing;
        this.active = active;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }
    public Appointment(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Person getPerformer() {
        return performer;
    }

    public void setPerformer(Person performer) {
        this.performer = performer;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", performer=" + performer +
                ", doctor=" + doctor +
                ", timing='" + timing + '\'' +
                ", active=" + active +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}