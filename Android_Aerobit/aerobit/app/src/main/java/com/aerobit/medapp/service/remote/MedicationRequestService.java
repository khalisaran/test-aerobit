package com.aerobit.medapp.service.remote;

import android.content.Context;
import android.util.Log;

import com.aerobit.medapp.http.HttpClient;
import com.aerobit.medapp.http.MedicationRequestRetrofit;
import com.aerobit.medapp.mapper.CarePlanDataMapper;
import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.MedicationRequest;
import com.aerobit.medapp.util.PrefManager;

import java.util.List;

import retrofit2.Call;

/**
 * Created by thomas on 05/03/18.
 */

public class MedicationRequestService {

    private static MedicationRequestService instance = null;

    private MedicationRequestService(){}

    public static MedicationRequestService getInstance(){

        if(instance == null){
            instance = new MedicationRequestService();
        }
        return instance;
    }

    public List<CarePlan> getAllCarePlanByPerson(Context context, Person person){

        String token = PrefManager.getInstance().getAuthorize(context);
        String xid =  PrefManager.getInstance().getXid(context);

        MedicationRequestRetrofit medicationDispenseRetrofit = HttpClient.httpService(MedicationRequestRetrofit.class, token, xid);
        Call<List<MedicationRequest>>  call = medicationDispenseRetrofit.getMedicationRequestByPatientId(person.getId());
        List<MedicationRequest> medicationRequests = null;
        try {
            medicationRequests = call.execute().body();

        } catch (Exception e) {
            Log.e("MedApp", "Error in getAllCarePlanByPerson");
            e.printStackTrace();
        }

        if(medicationRequests != null && !medicationRequests.isEmpty()){

            return CarePlanDataMapper.getInstance().transform(medicationRequests);
        }
        return null;
    }
}
