package com.aerobit.medapp.service.remote;

import android.content.Context;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.local.SyncStatus;
import com.aerobit.medapp.service.local.CarePlanService;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.local.PreferenceService;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

/**
 * Created by ashwin on 4/3/18.
 */


public class FetchLoginDataService {

    private static FetchLoginDataService instance = null;

    private FetchLoginDataService() {
    }

    public static FetchLoginDataService getInstance() {

        if (instance == null) {
            instance = new FetchLoginDataService();
        }
        return instance;
    }

    public List<Person> loadPersonData(Context context, Person person){

        List<Person> allPerson = PersonLoadDataService.getInstance().getAllPerson(context, person);

        PersonService.getInstance().createBulkPersonWithoutSync(allPerson);

        return allPerson;
    }

    public void loadAllCarePlanData(Context context, List<Person> allPerson){

//        //Load and save all medications
//        List<Medication> medications = MedicationDispenseService.getInstance().getAllMedicationDispense(context);
//
//        MedicationService.getInstance().createMedication(context.getContentResolver(), medications);

        //Load and save all the person related person
//        List<Person> allPerson = PersonLoadDataService.getInstance().getAllPerson(context, person);
//
//        PersonService.getInstance().createBulkPersonWithoutSync(allPerson);

        // Load and save all the careplan for all users
        if(!allPerson.isEmpty()){

            for(Person person1: allPerson){

                Preferences preferences = PreferencesDataService.getInstance().getByPerson(context, person1.getId());

                // Set Default theme if not available
                if(preferences == null){
                    preferences = new Preferences();
                }
                if(preferences.getAvatarColor() == null){
                    preferences.setAvatarColor(AerobitUtil.DEFAULT_THEME_COLOR);
                }

                preferences.setPerformer(person1);
                preferences.setSyncStatus(SyncStatus.SYNCED.getValue());
                PreferenceService.getInstance().createPreferenceWithoutSync(preferences);

                List<CarePlan> carePlans = MedicationRequestService.getInstance().getAllCarePlanByPerson(context, person1);

                if(carePlans != null && !carePlans.isEmpty())
                    CarePlanService.getInstance().createCarePlanListWithoutSync(context.getContentResolver(), carePlans);
            }
        }

    }
}


