package com.aerobit.medapp.sync.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.aerobit.medapp.sync.adapter.PersonSyncAdapter;

/**
 * Created by manikandan on 04/03/18.
 */

public class PersonSyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();
    private static PersonSyncAdapter sSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null)
                sSyncAdapter = new PersonSyncAdapter(getApplicationContext(), true);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
