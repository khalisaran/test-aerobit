package com.aerobit.medapp.model.remote;

import java.util.Date;

/**
 * Created by ashwin on 1/3/18.
 */

public class Coding {
    private String system;

    private String code;

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSystem() {
        return this.system;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}
