package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Symptom;
import com.aerobit.medapp.model.local.Trigger;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class TriggerRepository {
    private static TriggerRepository instance = null;

    private TriggerRepository() {

    }

    public static final TriggerRepository getInstance(){

        if(instance ==null){
            instance = new TriggerRepository();
        }
        return instance;
    }

    public List<Trigger> getAllTriggers() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Trigger> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Trigger.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Trigger getTrigger(final int triggerId) {
        Realm mRealm = Realm.getDefaultInstance();
        Trigger data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Trigger.class).equalTo("id", triggerId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data ;
    }

    public Trigger insertPreference(final Trigger trigger) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (trigger.getId() == null && trigger.getId().isEmpty())
                trigger.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(trigger);
            mRealm.commitTransaction();
            return trigger;
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return null;
    }
}
