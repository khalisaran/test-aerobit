package com.aerobit.medapp.model.local;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class ClinicalImpression extends RealmObject {

    @PrimaryKey
    @Required
    public String id;
    public Person performer;
    public Symptom symptom;
    public Trigger trigger;
    public Date createdDate;
    public Date updatedDate;

    public ClinicalImpression(){}

    public ClinicalImpression(String id, Person performer, Symptom symptom, Trigger trigger, Date createdDate, Date updatedDate) {
        this.id = id;
        this.performer = performer;
        this.symptom = symptom;
        this.trigger = trigger;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Person getPerformer() {
        return performer;
    }

    public void setPerformer(Person performer) {
        this.performer = performer;
    }

    public Symptom getSymptom() {
        return symptom;
    }

    public void setSymptom(Symptom symptom) {
        this.symptom = symptom;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "ClinicalImpression{" +
                "id=" + id +
                ", performer=" + performer +
                ", symptom=" + symptom +
                ", trigger=" + trigger +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}