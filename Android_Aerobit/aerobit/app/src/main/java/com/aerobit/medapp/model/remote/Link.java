package com.aerobit.medapp.model.remote;

public class Link {
    private String _id;

    public String getId() {
        return this._id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    private Person target;

    public Person getTarget() {
        return this.target;
    }

    public void setTarget(Person target) {
        this.target = target;
    }
}
