package com.aerobit.medapp.model.remote;

public class Notification
{
  private boolean needEmail;

  public boolean getNeedEmail() { return this.needEmail; }

  public void setNeedEmail(boolean needEmail) { this.needEmail = needEmail; }

  private boolean led;

  public boolean getLed() { return this.led; }

  public void setLed(boolean led) { this.led = led; }

  private boolean vibrate;

  public boolean getVibrate() { return this.vibrate; }

  public void setVibrate(boolean vibrate) { this.vibrate = vibrate; }

  private boolean sound;

  public boolean getSound() { return this.sound; }

  public void setSound(boolean sound) { this.sound = sound; }
}
