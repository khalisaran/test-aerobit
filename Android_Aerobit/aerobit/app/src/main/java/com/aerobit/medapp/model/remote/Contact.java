package com.aerobit.medapp.model.remote;

import java.util.Date;
import java.util.List;

/**
 * Created by ashwin on 1/3/18.
 */


public class Contact {
    private Name name;

    private List<Telecom> telecom;

    private Address address;

    private String gender;

    public void setName(Name name) {
        this.name = name;
    }

    public Name getName() {
        return this.name;
    }

    public void setTelecom(List<Telecom> telecom) {
        this.telecom = telecom;
    }

    public List<Telecom> getTelecom() {
        return this.telecom;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}