package com.aerobit.medapp.http;

import com.aerobit.medapp.model.remote.Person;
import com.aerobit.medapp.model.remote.Session;
import com.aerobit.medapp.model.request.EmailCheckRequest;
import com.aerobit.medapp.model.response.ApiResponse;
import com.aerobit.medapp.model.response.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Query;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by ashwin on 4/3/18.
 */

public interface PersonRetrofit {

    @POST("person")
    Call<RegisterResponse> postPerson(@Body Person person);

    @POST("login")
    Call<Session> loginPerson(@Body Session session);

    @POST("relatedperson?")
    Call<Person> postRelatedPerson(@Query("gid") String gid, @Body Person relatedPerson);

    @GET("person")
    Call<Person> getPerson(@Query("id") String id);

    @PUT("person?")
    Call<Person> updatePerson(@Query("id") String id, @Body Person person);

    @POST("checkmailexists")
    Call<ApiResponse> emailcheck(@Body EmailCheckRequest email);

    @POST("forgotpassword")
    Call<ApiResponse> forgetpassword(@Body EmailCheckRequest email);

    @POST("resetpassword")
    Call<ApiResponse> resetpassword(@Query("token") String token, @Body EmailCheckRequest email);

}
