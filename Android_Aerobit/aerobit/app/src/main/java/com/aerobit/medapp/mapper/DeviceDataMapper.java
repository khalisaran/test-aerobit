package com.aerobit.medapp.mapper;

import com.aerobit.medapp.model.local.Device;
import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.remote.MedicationDispense;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by thomas on 04/03/18.
 */

public class DeviceDataMapper {

    private static DeviceDataMapper instance;

    private DeviceDataMapper(){}

    public static DeviceDataMapper getInstance(){

        if(instance == null){
            instance = new DeviceDataMapper();
        }
        return instance;
    }

    public Device transform(com.aerobit.medapp.model.remote.Device medicationDispenseRemote) {

        Device medication = null;
        if(medicationDispenseRemote != null){

            medication = new Device();
            medication.setId(medicationDispenseRemote.get_id());
            medication.setName(medicationDispenseRemote.getUdi().getName());
            medication.setType(medicationDispenseRemote.getType());
            medication.setActive(medicationDispenseRemote.getActive());
            medication.setColor(medicationDispenseRemote.getColor());
            medication.setFirmwareId(medicationDispenseRemote.getUdi().getFirmwareId());
            //medication.setMacAddress(medicationDispenseRemote.getManufac);
            medication.setCreatedDate(medicationDispenseRemote.getCreatedAt());
            medication.setUpdatedDate(medicationDispenseRemote.getUpdatedAt());
        }
        return medication;
    }

    public List<Device> transform(Collection<com.aerobit.medapp.model.remote.Device> remoteMedicationDispenseCollection) {
        final List<Device> medicationList = new ArrayList<Device>();
        for (com.aerobit.medapp.model.remote.Device medEntity : remoteMedicationDispenseCollection) {
            final Device medication = transform(medEntity);
            if (medication != null) {
                medicationList.add(medication);
            }
        }
        return medicationList;
    }
}
