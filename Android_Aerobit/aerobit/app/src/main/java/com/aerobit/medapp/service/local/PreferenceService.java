package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.provider.PreferenceContentProvider;
import com.aerobit.medapp.repository.PreferenceRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashwin on 4/3/18.
 */

public class PreferenceService {
    private static PreferenceService instance = null;

    private PreferenceService(){
    }

    public static final PreferenceService getInstance(){

        if(instance ==null){
            instance = new PreferenceService();
        }
        return instance;
    }

    public Preferences createPreference(ContentResolver contentResolver, Preferences preference){

        ContentValues values = new ContentValues();
        String preferenceJSON = AerobitUtil.toJson(preference);
        values.put("PREFERENCEOBJECT", preferenceJSON);
        contentResolver.insert(PreferenceContentProvider.CONTENT_URI, values);
        return preference;
    }

    public Preferences createPreferenceWithoutSync(Preferences preference){

        preference = PreferenceRepository.getInstance().insertPreference(preference);

        return preference;
    }

    public List<Preferences> getAllPreference(ContentResolver contentResolver){

        String[] projection = {"PREFERENCEOBJECT"};
        Cursor cursor = contentResolver.query(PreferenceContentProvider.CONTENT_URI, projection, null, null,
                null);

        String preferencesJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    preferencesJSON = cursor.getString(cursor.getColumnIndex("PREFERENCEOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<Preferences> preferences = (List<Preferences>)AerobitUtil.fromJson(preferencesJSON, new TypeToken<ArrayList<Preferences>>() {}.getType());

        return preferences;

    }

        public Preferences getPreferenceByPerson(ContentResolver contentResolver, Person person){

        String[] projection = {"PREFERENCEOBJECT"};
        Cursor cursor = contentResolver.query(Uri.parse(PreferenceContentProvider.CONTENT_URI.toString() +"/performer/#"+person.getId()), projection, null, null,
                null);

        String preferencesJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    preferencesJSON = cursor.getString(cursor.getColumnIndex("PREFERENCEOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        Preferences preferences = (Preferences)AerobitUtil.fromJson(preferencesJSON, Preferences.class);

        return preferences;

    }
}
