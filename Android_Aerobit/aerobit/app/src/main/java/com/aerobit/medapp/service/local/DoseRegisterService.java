package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import com.aerobit.medapp.model.local.DoseRegister;
import com.aerobit.medapp.provider.DoseRegisterContentProvider;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashwin on 3/3/18.
 */

public class DoseRegisterService {
    private static DoseRegisterService instance = null;

    private DoseRegisterService(){
    }

    public static final DoseRegisterService getInstance(){

        if(instance ==null){
            instance = new DoseRegisterService();
        }
        return instance;
    }

    public DoseRegister createDoseRegister(ContentResolver contentResolver, DoseRegister doseRegister){

        ContentValues values = new ContentValues();
        String doseregisterJSON = AerobitUtil.toJson(doseRegister);
        values.put("DOSEREGISTEROBJECT", doseregisterJSON);
        contentResolver.insert(DoseRegisterContentProvider.CONTENT_URI, values);
        return doseRegister;
    }

    public List<DoseRegister> getAllDoseRegister(ContentResolver contentResolver){

        String[] projection = {"DOSEREGISTEROBJECT"};
        Cursor cursor = contentResolver.query(DoseRegisterContentProvider.CONTENT_URI, projection, null, null,
                null);
        String doseregisterJSON = null;

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    doseregisterJSON = cursor.getString(cursor.getColumnIndex("DOSEREGISTEROBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<DoseRegister> doseRegisters = (List<DoseRegister>)AerobitUtil.fromJson(doseregisterJSON, new TypeToken<ArrayList<DoseRegister>>() {}.getType());
        return doseRegisters;

    }
}
