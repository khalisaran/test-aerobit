package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.repository.PersonRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by edrinthomas on 02/03/18.
 */

public class PersonContentProvider extends ContentProvider{

    private PersonRepository personRepository;
    private Realm mRealm;

    public static final String CONTENT_AUTHORITY = "com.aerobit.medapp.provider.PersonContentProvider";
    private static final String URL = "content://" + CONTENT_AUTHORITY + "/person";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int PERSON = 1;
    private static final int PERSON_ID = 2;
    private static final int PERSON_BY_SYNC_STATUS = 3;
    private static final int CHECK_EMAIL_EXISTS = 4;
    private static final int DELETE_SYNCED_PERSON_ID = 4;
//    private static final int PERSON_LIST = 3;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CONTENT_AUTHORITY, "person" , PERSON);
        matcher.addURI(CONTENT_AUTHORITY, "persons/#", PERSON_ID);
        matcher.addURI(CONTENT_AUTHORITY, "person/status/#", PERSON_BY_SYNC_STATUS);
        matcher.addURI(CONTENT_AUTHORITY, "person/check-email/", CHECK_EMAIL_EXISTS);
        matcher.addURI(CONTENT_AUTHORITY, "person/delete/#", DELETE_SYNCED_PERSON_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {

        personRepository = PersonRepository.getInstance();
        mRealm = Realm.getDefaultInstance();
        return personRepository == null ? false: true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        MatrixCursor myCursor = new MatrixCursor(new String[]{"PERSONOBJECT"});
        Object person = null;
        String uriStr = uri.toString();

        if(uriStr.matches("^"+URL+"/#"+"[A-Za-z0-9\\-]*$")) {
            person = personRepository.getPerson(new Integer(uri.getLastPathSegment()));
        }
            else if(uriStr.matches("^"+URL+"$")){
            person = personRepository.getAllPersons();
        }
        else if(uriStr.matches("^"+URL+"/status/#"+"[A-Za-z0-9\\-_]*$")){
            person = personRepository.getPersonBySyncStatus(AerobitUtil.getIdFromURL(uri.toString()));
        }
        else if(uriStr.matches("^"+URL+"/delete/#"+"[A-Za-z0-9\\-_]*$")){
            person = personRepository.deleteSyncedPerson(AerobitUtil.getIdFromURL(uri.toString()));
        }
        String json = AerobitUtil.toJson(person);

        Object[] rowData = new Object[]{json};

        myCursor.addRow(rowData);
        System.out.println("Collection Length:"+myCursor.getCount());

        return myCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        switch (getUriMatch(uri)) {
            case PERSON:

                Person personJSON = (Person) AerobitUtil.fromJson(values.get("PERSONOBJECT").toString(), Person.class);
                try {
                    personRepository.insertPerson(personJSON);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case PERSON_ID:
                List<Person> personsJSON = (List<Person>) AerobitUtil.fromJson(values.get("PERSONOBJECT").toString(), new TypeToken<ArrayList<Person>>() {}.getType());
                try {
                    personRepository.insertPersons(personsJSON);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            default:
        }

        //Uri notifyURI = Uri.parse(URL+personJSON.getId());
        //TODO: need to notify
        getContext().getContentResolver().notifyChange(uri, null, true);
        return uri;
    }


    public int getUriMatch(Uri uri){

        String uriStr = uri.toString();

        if(uriStr.matches("^"+URL+"/#"+"[A-Za-z0-9\\-]*$")){

            return 2;

        }
        else if(uriStr.matches("^"+URL+"$")){

            return 1;
        }
        else if(uriStr.matches("^"+URL+"/status/#"+"[A-Za-z0-9\\-]*$")){

            return 3;
        }
        return -1;
    }


    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
       /* Person personJSON = (Person) AerobitUtil.fromJson(values.get("PERSONOBJECT").toString(), Person.class);
        try {
            personRepository.insertPerson(mRealm, personJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri notifyURI = Uri.parse(URL+personJSON.getId());
        //TODO: need to notify
        getContext().getContentResolver().notifyChange(notifyURI, null);*/
        return 1;
    }
}

