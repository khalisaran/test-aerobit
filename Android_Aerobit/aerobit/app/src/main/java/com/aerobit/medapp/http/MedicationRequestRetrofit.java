package com.aerobit.medapp.http;

import com.aerobit.medapp.model.remote.MedicationRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Query;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by ashwin on 4/3/18.
 */

public interface MedicationRequestRetrofit {

    @POST("medicationrequest")
    Call<MedicationRequest> postMedicationRequest(@Body MedicationRequest medicationRequest);

    @PUT("medicationrequest")
    Call<MedicationRequest> putMedicationRequest(@Query("id") String id, @Body MedicationRequest medicationRequest);

    @GET("medicationrequest/patient")
    Call<List<MedicationRequest>> getMedicationRequestByPatientId(@Query("patientId") String id);

    @GET("medicationrequest/patient/history")
    Call<List<MedicationRequest>> getMedicationRequestHistoryOfPatient(@Query("medicationId") String medicationId, @Query("patientId") int patientId);


}
