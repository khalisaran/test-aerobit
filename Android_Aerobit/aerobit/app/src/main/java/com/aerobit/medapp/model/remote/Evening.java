package com.aerobit.medapp.model.remote;

public class Evening
{
  private String time;

  public String getTime() { return this.time; }

  public void setTime(String time) { this.time = time; }

  private boolean enable;

  public boolean getEnable() { return this.enable; }

  public void setEnable(boolean enable) { this.enable = enable; }
}