package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.repository.PreferenceRepository;
import com.aerobit.medapp.util.AerobitUtil;

import io.realm.Realm;

/**
 * Created by ashwin on 4/3/18.
 */

public class PreferenceContentProvider extends ContentProvider {

    PreferenceRepository preferenceRepository;
    private Realm mRealm;

    static final public String CONTENT_AUTHORITY = "com.aerobit.medapp.provider.PreferenceContentProvider";
    static final String URL = "content://" + CONTENT_AUTHORITY + "/preference";
    static final public Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int PREFERENCE = 1;
    static final int PREFERENCE_ID = 2;
    static final int PERFORMER_ID = 3;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CONTENT_AUTHORITY, "preference", PREFERENCE);
        matcher.addURI(CONTENT_AUTHORITY, "preference/#", PREFERENCE_ID);
        matcher.addURI(CONTENT_AUTHORITY, "performer/#", PERFORMER_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {

        preferenceRepository = PreferenceRepository.getInstance();
        mRealm = Realm.getDefaultInstance();
        return preferenceRepository == null ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        MatrixCursor myCursor = new MatrixCursor(new String[]{"PREFERENCEOBJECT"});

        Object preference = null;
        String id = uri.getLastPathSegment();
        switch (getUriMatch(uri)) {
            case PREFERENCE:
                preference = preferenceRepository.getAllPreferences();
                break;

            case PREFERENCE_ID:
                preference = preferenceRepository.getPreference(new Integer(uri.getLastPathSegment()));
                break;

            case PERFORMER_ID:
                preference = preferenceRepository.getPreferenceByPatientId(AerobitUtil.getIdFromURL(uri.toString()));
                break;

            default:
        }
        String json = AerobitUtil.toJson(preference);
        Object[] rowData = new Object[]{json};
        myCursor.addRow(rowData);
        return myCursor;
    }

    public int getUriMatch(Uri uri) {

        String uriStr = uri.toString();

        if (uriStr.matches("^" + URL + "/#" + "[A-Za-z0-9\\-]*$")) {

            return 2;

        } else if (uriStr.matches("^" + URL + "$")) {

            return 1;
        } else if (uriStr.matches("^" + URL + "/performer/#" + "[A-Za-z0-9\\-]*$")) {

            return 3;
        }
        return -1;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        preferenceRepository = preferenceRepository.getInstance();
        Preferences preferenceJSON = (Preferences) AerobitUtil.fromJson(values.get("PREFERENCEOBJECT").toString(), Preferences.class);
        try {
            preferenceRepository.insertPreference(preferenceJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri notifyURI = Uri.parse(URL + preferenceJSON.getId());
        //TODO: need to notify
        getContext().getContentResolver().notifyChange(notifyURI, null);
        return notifyURI;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
