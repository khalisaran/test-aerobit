package com.aerobit.medapp.service.remote;

import android.content.Context;
import android.util.Log;

import com.aerobit.medapp.http.HttpClient;
import com.aerobit.medapp.http.MedicationDispenseRetrofit;
import com.aerobit.medapp.mapper.MedicationDataMapper;
import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.remote.MedicationDispense;
import com.aerobit.medapp.util.PrefManager;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Created by edrinthomas on 05/03/18.
 */

public class MedicationDispenseService {

    private static MedicationDispenseService instance = null;

    private MedicationDispenseService(){}

    public static MedicationDispenseService getInstance(){

        if(instance == null){
            instance = new MedicationDispenseService();
        }
        return instance;
    }

    public List<Medication> getAllMedicationDispense(Context context) {

        String token = PrefManager.getInstance().getAuthorize(context);
        String xid =  PrefManager.getInstance().getXid(context);

        MedicationDispenseRetrofit apiService =
                HttpClient.httpService(MedicationDispenseRetrofit.class, token, xid);
        Call<List<MedicationDispense>> call = apiService.getAllMedicationDispense();
        List<MedicationDispense> medicationDispenses = null;
        try {
            medicationDispenses = call.execute().body();
        } catch (IOException e) {
            Log.e("MedApp", "Error in FetchDataService");
        }
        return MedicationDataMapper.getInstance().transform(medicationDispenses);
    }
}
