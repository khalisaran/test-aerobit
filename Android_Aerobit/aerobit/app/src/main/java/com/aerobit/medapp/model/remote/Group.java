package com.aerobit.medapp.model.remote;

import java.util.Date;

/**
 * Created by ashwin on 5/3/18.
 */

public class Group
{
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isActive() {
        return active;
    }

    private Date updatedAt;

    public Date getUpdatedAt() { return this.updatedAt; }

    public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

    private Date createdAt;

    public Date getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    private String type;

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    private Person performer;

    public Person getPerformer() { return this.performer; }

    public void setPerformer(Person performer) { this.performer = performer; }

    private Device device;

    public Device getDevice() { return this.device; }

    public void setDevice(Device device) { this.device = device; }

    private boolean active;

    public boolean getActive() { return this.active; }

    public void setActive(boolean active) { this.active = active; }

    private Text text;

    public Text getText() { return this.text; }

    public void setText(Text text) { this.text = text; }

    private String resourceType;

    public String getResourceType() { return this.resourceType; }

    public void setResourceType(String resourceType) { this.resourceType = resourceType; }
}