package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class MedicationRepository {
    private static MedicationRepository instance = null;

    private MedicationRepository() {
    }

    public static final MedicationRepository getInstance(){

        if(instance ==null){
            instance = new MedicationRepository();
        }
        return instance;
    }

    public List<Medication> getAllMedications() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Medication> results = null;
        try {
            results =  mRealm.copyFromRealm(mRealm.where(Medication.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }


        return results;
    }

    public Medication getMedication( final int medicationId) {
        Realm mRealm = Realm.getDefaultInstance();
        Medication data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Medication.class).equalTo("id", medicationId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data ;
    }

    public List<Medication> getMedicationByType( final String medicationType) {
        Realm mRealm = Realm.getDefaultInstance();
        List<Medication> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Medication.class).equalTo("type", medicationType).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public List<Medication> insertMedication( List<Medication> medications) {
        Realm mRealm = Realm.getDefaultInstance();
        try{
            mRealm.beginTransaction();
            for(Medication medication: medications){

                if(medication.getId() == null && medication.getId().isEmpty())
                    medication.setId(AerobitUtil.genUUID());
            }
            medications = mRealm.copyToRealmOrUpdate(medications);
            mRealm.commitTransaction();

        } catch(Exception e){
            mRealm.cancelTransaction();;
        }finally {
            mRealm.close();
        }

        return mRealm.copyFromRealm(medications);
    }

    public List<Medication> getMedicationByName(String name, String type) {
        Realm mRealm = Realm.getDefaultInstance();
        List<Medication> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Medication.class).beginsWith("name", name, Case.INSENSITIVE)
                    .equalTo("type", type).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }
}
