package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Group;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class GroupRepository {
    private static GroupRepository instance = null;

    private GroupRepository() {
    }

    public static final GroupRepository getInstance(){

        if(instance ==null){
            instance = new GroupRepository();
        }
        return instance;
    }

    public List<Group> getAllGroups() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Group> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Group.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Group getGroup( final int groupId) {
        Realm mRealm = Realm.getDefaultInstance();
        Group data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Group.class).equalTo("id", groupId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data ;
    }

    public void insertGroup( Group group){
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (group.getId() == null && group.getId().isEmpty())
                group.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(group);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }
    }
}