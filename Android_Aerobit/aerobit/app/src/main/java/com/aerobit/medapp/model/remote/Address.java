package com.aerobit.medapp.model.remote;

import java.util.Date;
import java.util.List;

/**
 * Created by ashwin on 1/3/18.
 */

public class Address {
    private String country;

    private String use;

    private List<String> line;

    private String state;

    private String city;

    private String postalCode;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return this.country;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getUse() {
        return this.use;
    }

    public void setLine(List<String> line) {
        this.line = line;
    }

    public List<String> getLine() {
        return this.line;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    private Date createdDate;

    private Date updatedDate;

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}