package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import com.aerobit.medapp.model.local.Appointment;
import com.aerobit.medapp.provider.AppointmentContentProvider;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashwin on 3/3/18.
 */

public class AppointmentService {
    private static AppointmentService instance = null;

    private AppointmentService(){
    }

    public static final AppointmentService getInstance(){

        if(instance ==null){
            instance = new AppointmentService();
        }
        return instance;
    }

    public Appointment createAppointment(ContentResolver contentResolver, Appointment appointment){


        ContentValues values = new ContentValues();

        // get appointment JSON
        String appointmentJSON = AerobitUtil.toJson(appointment);
        System.out.println("appointmentJSON:"+appointmentJSON);

        // prepare the content values
        values.put("APPOINTMENTOBJECT", appointmentJSON);
        contentResolver.insert(AppointmentContentProvider.CONTENT_URI, values);

        // find the appointment and send it back

        return appointment;
    }

    public List<Appointment> getAllAppointment(ContentResolver contentResolver){

        String[] projection = {"APPOINTMENTOBJECT"};

        Cursor cursor = contentResolver.query(AppointmentContentProvider.CONTENT_URI, projection, null, null,
                null);

        String appointmentsJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    appointmentsJSON = cursor.getString(cursor.getColumnIndex("APPOINTMENTOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<Appointment> appointments = (List<Appointment>)AerobitUtil.fromJson(appointmentsJSON, new TypeToken<ArrayList<Appointment>>() {}.getType());
        return appointments;

    }
}
