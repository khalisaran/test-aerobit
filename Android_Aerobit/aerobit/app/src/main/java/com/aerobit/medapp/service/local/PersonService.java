package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.SyncStatus;
import com.aerobit.medapp.provider.PersonContentProvider;
import com.aerobit.medapp.repository.PersonRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;


/**
 * Created by edrinthomas on 02/03/18.
 */

public class PersonService {

    private static PersonService instance = null;

    private PersonService(){
    }

    public static final PersonService getInstance(){

        if(instance ==null){
            instance = new PersonService();
        }
        return instance;
    }

    public Person createPerson(ContentResolver contentResolver, Person person){

        ContentValues values = new ContentValues();
        if(person.getSyncStatus() == null){
            person.setSyncStatus(SyncStatus.NOT_SYNCED.getValue());
        }
        person.setUpdatedDate(new Date());
        // get person JSON
        String personJSON = AerobitUtil.toJson(person);

        // prepare the content values
        values.put("PERSONOBJECT", personJSON);
        contentResolver.insert(PersonContentProvider.CONTENT_URI, values);
        // find the person and send it back
        return person;
    }

    public List<Person> createBulkPersonWithoutSync(List<Person> person){

        PersonRepository.getInstance().insertPersons(person);
        return person;
    }

    public Person updatePerson(ContentResolver contentResolver, Person person){

        ContentValues values = new ContentValues();

        // get person JSON
        String personJSON = AerobitUtil.toJson(person);

        // prepare the content values
        values.put("PERSONOBJECT", personJSON);
        contentResolver.update(PersonContentProvider.CONTENT_URI, values, null,
                null);
        // find the person and send it back
        return person;
    }

    public List<Person> getAllPerson(ContentResolver contentResolver){

        String[] projection = {"PERSONOBJECT"};
        Cursor cursor = contentResolver.query(PersonContentProvider.CONTENT_URI, projection, null, null,
                null);

        String personJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    personJSON = cursor.getString(cursor.getColumnIndex("PERSONOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<Person> persons = (List<Person>)AerobitUtil.fromJson(personJSON, new TypeToken<ArrayList<Person>>() {}.getType());

        return persons;
    }

    public List<Person> getPersonBySyncStatus(ContentResolver contentResolver, String status){

        String[] projection = {"PERSONOBJECT"};
        Cursor cursor = contentResolver.query(Uri.parse(PersonContentProvider.CONTENT_URI+"/status/#"+status), projection, null, null,
                null);

        String personJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    personJSON = cursor.getString(cursor.getColumnIndex("PERSONOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        List<Person> persons = (List<Person>)AerobitUtil.fromJson(personJSON, new TypeToken<ArrayList<Person>>() {}.getType());

        return persons;
    }
    public Person deleteSyncedPerson(ContentResolver contentResolver, String idToBeRemoved){

        String[] projection = {"PERSONOBJECT"};
        Cursor cursor = contentResolver.query(Uri.parse(PersonContentProvider.CONTENT_URI+"/delete/#"+idToBeRemoved), projection, null, null,
                null);

        String personJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    personJSON = cursor.getString(cursor.getColumnIndex("PERSONOBJECT"));
                } while (cursor.moveToNext());
            }
        }
        Person persons = (Person)AerobitUtil.fromJson(personJSON, new TypeToken<Person>() {}.getType());

        return persons;
    }
}
