package com.aerobit.medapp.model.local;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class Person extends RealmObject implements Serializable{
    @PrimaryKey
    @Required
    public String id;
    private String firstName;
    private String lastName;
    public String email;
    public String birthDate;
    public String gender;
    public Boolean active;
    public String country;
    //private RealmList<Person> relatedPerson;
    private Person guardian;

    @Ignore
    private String password;

    @Ignore
    private String themeColor;

    public Date createdDate;
    public Date updatedDate;

    private String syncStatus;

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public Person() {
    }

    public Person(String id, String firstName, String lastName, String email, String birthDate, String gender, Boolean active, String country, Person guardian, Date createdDate, Date updatedDate, String syncStatus) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.active = active;
        this.country = country;
        this.guardian = guardian;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.syncStatus = syncStatus;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Person getGuardian() {
        return guardian;
    }

    public void setGuardian(Person guardian) {
        this.guardian = guardian;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }



    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender=" + gender +
                ", active=" + active +
                ", country='" + country + '\'' +
                ", guardian=" + guardian +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                ", syncStatus=" + syncStatus +
                '}';
    }
}