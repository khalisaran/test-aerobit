package com.aerobit.medapp.model.remote;

/**
 * Created by ashwin on 1/3/18.
 */

public class Photo {
    private String contentType;

    private String data;

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return this.data;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}