package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Device;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class DeviceRepository {
    private static DeviceRepository instance = null;

    private DeviceRepository() {
    }

    public static final DeviceRepository getInstance(){

        if(instance ==null){
            instance = new DeviceRepository();
        }
        return instance;
    }

    public List<Device> getAllDevices() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Device> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Device.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public Device getDevice(final int deviceId) {
        Realm mRealm = Realm.getDefaultInstance();
        Device data = null ;
        try {
        data =  mRealm.copyFromRealm(mRealm.where(Device.class).equalTo("id", deviceId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public void insertDevice(Device device) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if(device.getId() == null && device.getId().isEmpty())
                device.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(device);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }

    }
}
