package com.aerobit.medapp.model.remote;

public class Image
{
  private String contentType;

  public String getContentType() { return this.contentType; }

  public void setContentType(String contentType) { this.contentType = contentType; }

  private String data;

  public String getData() { return this.data; }

  public void setData(String data) { this.data = data; }
}