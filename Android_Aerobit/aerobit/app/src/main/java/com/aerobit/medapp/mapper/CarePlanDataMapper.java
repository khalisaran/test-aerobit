package com.aerobit.medapp.mapper;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.Address;
import com.aerobit.medapp.model.remote.DosageInstruction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by thomas on 04/03/18.
 */

public class CarePlanDataMapper {

    private static CarePlanDataMapper instance;

    private CarePlanDataMapper(){}

    public static CarePlanDataMapper getInstance(){

        if(instance == null){
            instance = new CarePlanDataMapper();
        }
        return instance;
    }

    public CarePlan transform(com.aerobit.medapp.model.remote.MedicationRequest medicationRequestRemote) {

        CarePlan carePlan = null;
        if(medicationRequestRemote != null){

            carePlan = new CarePlan();
            carePlan.setId(medicationRequestRemote.get_id());
            carePlan.setActive(medicationRequestRemote.getActive());
            carePlan.setCategory(medicationRequestRemote.getCategory());
            carePlan.setDevice(DeviceDataMapper.getInstance().transform(medicationRequestRemote.getDevice()));
            carePlan.setMedication(MedicationDataMapper.getInstance().transform(medicationRequestRemote.getMedicationReference()));
            carePlan.setPerformer(PersonDataMapper.getInstance().transform(medicationRequestRemote.getPerformer()));

            if(medicationRequestRemote.getDosageInstruction() != null && !medicationRequestRemote.getDosageInstruction().isEmpty()){

                RealmList<String> stringRealmList = new RealmList<String>();

                for(DosageInstruction dose : medicationRequestRemote.getDosageInstruction()){
                    stringRealmList.add(dose.getTiming());
                }

                carePlan.setDosages(stringRealmList);
            }
            carePlan.setCreatedDate(medicationRequestRemote.getCreatedAt());
            carePlan.setUpdatedDate(medicationRequestRemote.getUpdatedAt());

        }

        return carePlan;
    }

    public List<CarePlan> transform(Collection<com.aerobit.medapp.model.remote.MedicationRequest> remoteMedicationRequestCollection) {
        final List<CarePlan> carePlanList = new ArrayList<CarePlan>();
        for (com.aerobit.medapp.model.remote.MedicationRequest medReqEntity : remoteMedicationRequestCollection) {
            final CarePlan carePlan = transform(medReqEntity);
            if (carePlan != null) {
                carePlanList.add(carePlan);
            }
        }
        return carePlanList;
    }
}
