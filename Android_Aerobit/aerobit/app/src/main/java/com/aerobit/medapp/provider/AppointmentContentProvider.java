package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aerobit.medapp.model.local.Appointment;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.repository.AppointmentRepository;
import com.aerobit.medapp.repository.PersonRepository;
import com.aerobit.medapp.util.AerobitUtil;

import io.realm.Realm;

/**
 * Created by ashwin on 3/3/18.
 */

public class AppointmentContentProvider extends ContentProvider {

    private AppointmentRepository appointmentRepository;

    static final String authority = "com.aerobit.medapp.provider.AppointmentContentProvider";
    static final String URL = "content://" + authority + "/appointment";
    static final public Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int APPOINTMENT = 1;
    private static final int APPOINTMENT_ID = 2;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(authority, "appointment" , APPOINTMENT);
        matcher.addURI(authority, "appointment/#", APPOINTMENT_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        appointmentRepository = AppointmentRepository.getInstance();
        return appointmentRepository == null ? false: true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        MatrixCursor myCursor = new MatrixCursor(new String[]{"OBJECT"});

        Object appointment = null;
        switch (sUriMatcher.match(uri)) {
            case APPOINTMENT:
                myCursor.addRow(appointmentRepository.getAllAppointments());
                //person = ;
                break;

            case APPOINTMENT_ID:
                appointment = appointmentRepository.getAppointment(new Integer(uri.getLastPathSegment()));
                break;

            default:
        }
        String json = AerobitUtil.toJson(appointment);
        Object[] rowData = new Object[]{};
        return myCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        Appointment appointmentJSON = (Appointment) AerobitUtil.fromJson(values.get("APPOINTMENTOBJECT").toString(), Appointment.class);
        try {
            appointmentRepository.insertAppointment(appointmentJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri notifyURI = Uri.parse(URL+appointmentJSON.getId());
        getContext().getContentResolver().notifyChange(notifyURI, null);
        return notifyURI;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}


