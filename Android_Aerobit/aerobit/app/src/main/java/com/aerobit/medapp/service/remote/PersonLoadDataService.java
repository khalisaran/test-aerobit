package com.aerobit.medapp.service.remote;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aerobit.medapp.http.HttpClient;
import com.aerobit.medapp.http.PersonRetrofit;
import com.aerobit.medapp.mapper.PersonDataMapper;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.Link;
import com.aerobit.medapp.model.remote.Session;
import com.aerobit.medapp.model.request.EmailCheckRequest;
import com.aerobit.medapp.model.response.ApiResponse;
import com.aerobit.medapp.model.response.RegisterResponse;
import com.aerobit.medapp.util.PrefManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;

/**
 * Created by ashwin on 5/3/18.
 */

public class PersonLoadDataService {

    private static PersonLoadDataService instance = null;

    private PersonLoadDataService() {
    }

    private PrefManager prefManager;

    public static PersonLoadDataService getInstance() {

        if (instance == null) {
            instance = new PersonLoadDataService();
        }
        return instance;
    }

    public List<Person> getAllPerson(Context context, com.aerobit.medapp.model.local.Person person) {

        String token = prefManager.getInstance().getAuthorize(context);

        String xid =  prefManager.getInstance().getXid(context);

        PersonRetrofit personRetrofit = HttpClient.httpService(PersonRetrofit.class, token, xid);
        Call<com.aerobit.medapp.model.remote.Person> call = personRetrofit.getPerson(person.getId());
        com.aerobit.medapp.model.remote.Person persons = null;
        try {
            persons = call.execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MedApp", "Error in getPerson");
        }
        List<Person> personList = new ArrayList<Person>();
        Person guardian = PersonDataMapper.getInstance().transform(persons);
        personList.add(guardian);

        prefManager = prefManager.getInstance();
        prefManager.setGuardian(context, guardian);
        prefManager.setSelectedUser(context, guardian);


        if(persons != null && persons.getLink() != null)
            for (Link link : persons.getLink()) {

                if(link.getTarget() != null){

                    Person relatedPerson = PersonDataMapper.getInstance().transform(link.getTarget());
                    relatedPerson.setGuardian(guardian);
                    personList.add(relatedPerson);
                }
            }

        return personList;
    }

    public RegisterResponse registerPerson(Context context, Person person) {

        RegisterResponse personData = null;

        try {
            personData = new AsyncRegister(context, person).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return personData;
    }

    public Person postRelatedPerson(com.aerobit.medapp.model.remote.Person person, String gid,  String token, String xid) {

        PersonRetrofit personRetrofit = HttpClient.httpService(PersonRetrofit.class, token, xid);
        Call<com.aerobit.medapp.model.remote.Person> call = personRetrofit.postRelatedPerson(gid, person);
        com.aerobit.medapp.model.remote.Person personData = null;
        try {
            personData = call.execute().body();
        } catch (IOException e) {
            Log.e("MedApp", "Error in Post related person");
        }
        Person savedPerson = PersonDataMapper.getInstance().transform(personData);
        return savedPerson;
    }

    public Person updatePerson(com.aerobit.medapp.model.remote.Person person, String id,  String token, String xid) {

        PersonRetrofit personRetrofit = HttpClient.httpService(PersonRetrofit.class, token, xid);
        Call<com.aerobit.medapp.model.remote.Person> call = personRetrofit.updatePerson(id, person);
        com.aerobit.medapp.model.remote.Person personData = null;
        try {
            personData = call.execute().body();
        } catch (IOException e) {
            Log.e("MedApp", "Error in Post related person");
        }
        Person updatedPerson = PersonDataMapper.getInstance().transform(personData);
        return updatedPerson;
    }

    public boolean emailCheck(Context context, String email){

        ApiResponse data = null;
        try {
            data = new AsyncEmailCheck(context, email).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return data.isStatus();
    }

    private class AsyncEmailCheck extends AsyncTask<Void,Void,ApiResponse> {

        String email;
        Context context;
        AsyncEmailCheck(Context context, String email){
            this.email = email;
            this.context = context;
        }
        @Override
        protected ApiResponse doInBackground(Void... voids) {

            String token = prefManager.getInstance().getAuthorize(context);
            Log.i("the email ",email.toString());
            PersonRetrofit apiservice = HttpClient.httpService(PersonRetrofit.class,token,"");

            EmailCheckRequest emailCheckRequest = new EmailCheckRequest();
            emailCheckRequest.setEmail(email);
            Call<ApiResponse> call = apiservice.emailcheck(emailCheckRequest);
            ApiResponse data = null;
            try {
                data = call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in EmailCheck");
            }
            return data;

        }


    }

    private class AsyncRegister extends AsyncTask<Void,Void,RegisterResponse> {

        Person person;
        Context context;
        AsyncRegister(Context context, Person person){
            this.person = person;
            this.context = context;
        }
        @Override
        protected RegisterResponse doInBackground(Void... voids) {

            String token = prefManager.getInstance().getAuthorize(context);

            PersonRetrofit apiservice = HttpClient.httpService(PersonRetrofit.class,token,"");

            com.aerobit.medapp.model.remote.Person remotePerson = PersonDataMapper.getInstance().transformToRemote(this.person);

            Call<RegisterResponse> call = apiservice.postPerson(remotePerson);
            RegisterResponse data = null;
            try {
                data = call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in Register");
            }
            return data;

        }


    }

    public Session login(Context context, Session session) {

        Session data = null;
        try {
            data = new AsyncLogin(context, session).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // set xid
        PrefManager.getInstance().setXid(context, data.getXid());

        return data;
    }

    private class AsyncLogin extends AsyncTask<Void,Void,Session> {

        Session session;
        Context context;
        AsyncLogin(Context context, Session session){
            this.session = session;
            this.context = context;
        }
        @Override
        protected Session doInBackground(Void... voids) {

            String token = prefManager.getInstance().getAuthorize(context);

            PersonRetrofit apiService =
                    HttpClient.httpService(PersonRetrofit.class, token, "");

            Call<Session> call = apiService.loginPerson(session);
            Session sessionData = null;
            try {
                sessionData = call.execute().body();
            } catch (IOException e) {
                Log.e("MedApp", "Error in Login");
            }
            return sessionData;
        }


    }

    //forgetpassword
    public ApiResponse forgetpassword(Context context, String email){

        ApiResponse data = null;
        try {
            data = new Asyncforgetpassword(context, email).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return data;
    }

    private class Asyncforgetpassword extends AsyncTask<Void,Void,ApiResponse> {

        String email;
        Context context;
        Asyncforgetpassword(Context context, String email){
            this.email = email;
            this.context = context;
        }
        @Override
        protected ApiResponse doInBackground(Void... voids) {

            String token = prefManager.getInstance().getAuthorize(context);
            Log.i("the email ",email.toString());
            PersonRetrofit apiservice = HttpClient.httpService(PersonRetrofit.class,token,"");

            EmailCheckRequest emailCheckRequest = new EmailCheckRequest();
            emailCheckRequest.setEmail(email);
            Call<ApiResponse> call = apiservice.forgetpassword(emailCheckRequest);
            ApiResponse data = null;
            try {
                data = call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in ForgetPassword");
            }
            return data;

        }


    }


    public ApiResponse resetpassword(Context context, String password, String token){

        ApiResponse data = null;
        try {
            data = new resetpassword(context, password, token).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return data;
    }

    private class resetpassword extends AsyncTask<Void,Void,ApiResponse> {

        String password;
        Context context;
        String token;
        resetpassword(Context context, String password, String token){
            this.password = password;
            this.context = context;
            this.token = token;
        }
        @Override
        protected ApiResponse doInBackground(Void... voids) {

            String access = prefManager.getInstance().getAuthorize(context);
            Log.i("the password ",password.toString());
            PersonRetrofit apiservice = HttpClient.httpService(PersonRetrofit.class,access,"");

            EmailCheckRequest emailCheckRequest = new EmailCheckRequest();
            emailCheckRequest.setPassword(password);
            Call<ApiResponse> call = apiservice.resetpassword(token, emailCheckRequest);
            ApiResponse data = null;
            try {
                data = call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in EmailCheck");
            }
            return data;

        }


    }

}

