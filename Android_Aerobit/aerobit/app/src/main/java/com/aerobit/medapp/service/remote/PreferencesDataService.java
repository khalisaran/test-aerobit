package com.aerobit.medapp.service.remote;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceDataStore;
import android.util.Log;

import com.aerobit.medapp.http.HttpClient;
import com.aerobit.medapp.http.PreferenceRetrofit;
import com.aerobit.medapp.mapper.PreferencesDataMapper;
import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.remote.Preference;
import com.aerobit.medapp.util.PrefManager;

import java.util.concurrent.ExecutionException;

import retrofit2.Call;

/**
 * Created by khalisaran on 13/3/18.
 */

public class PreferencesDataService {

    private static PreferencesDataService instance = null;

    private PrefManager prefManager;

    public static PreferencesDataService getInstance() {

        if (instance == null) {
            instance = new PreferencesDataService();
        }
        return instance;
    }

    public Preferences Save(Context context,String personid, Preferences localpreference){

        Preference preference = PreferencesDataMapper.getInstance().transformToRemote(localpreference);
        String token = prefManager.getInstance().getAuthorize(context);

        String xid =  prefManager.getInstance().getXid(context);

        Preference data = null;
        try{
            data = new AsynSave(token,xid,personid,preference).execute().get();
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Preferences result = PreferencesDataMapper.getInstance().transform(data);
        return result;
    }

    private class AsynSave extends AsyncTask<Void,Void,Preference> {

        String token;
        String xid;
        Preference preference;
        String personid;
        AsynSave(String token, String xid , String personid, Preference preference){
            this.token = token;
            this.xid = xid;
            this.preference = preference;
            this.personid = personid;
        }

        @Override
        protected Preference doInBackground(Void... voids) {

            PreferenceRetrofit apiserver = HttpClient.httpService(PreferenceRetrofit.class,token,xid);
            Call<Preference> call = apiserver.save(personid,preference);
            Preference data = null;
            try{
                data = call.execute().body();
            }catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in Save Preference");
            }
            return data;
        }

    }

    public Preferences getByPerson(Context context,String personid){

        String token = prefManager.getInstance().getAuthorize(context);

        String xid =  prefManager.getInstance().getXid(context);

        Preference data = null;
        try{
            data = new AsyngetByPerson(token,xid, personid).execute().get();
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Preferences localperference = PreferencesDataMapper.getInstance().transform(data);
        return localperference;
    }

    private class AsyngetByPerson extends AsyncTask<Void,Void,Preference> {

        String token;
        String xid;
        String personid;
        AsyngetByPerson(String token, String xid , String personid){
            this.token = token;
            this.xid = xid;
            this.personid = personid;
        }

        @Override
        protected Preference doInBackground(Void... voids) {

            PreferenceRetrofit apiserver = HttpClient.httpService(PreferenceRetrofit.class,token,xid);
            Call<Preference> call = apiserver.getByPerson(personid);
            Preference data = null;
            try{
                data = call.execute().body();
            }catch (Exception e) {
                e.printStackTrace();
                Log.e("MedApp", "Error in Get byperson Preference");
            }
            return data;
        }

    }


}
