package com.aerobit.medapp.model.remote;

import java.util.Date;

/**
 * Created by ashwin on 4/3/18.
 */

public class Preference
{
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Boolean isActive() {
        return active;
    }

    private Date updatedAt;

    public Date getUpdatedAt() { return this.updatedAt; }

    public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

    private Date createdAt;

    public Date getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

//    private String theme;

//    public String getTheme() { return this.theme; }

//    public void setTheme(String theme) { this.theme = theme; }

    private Avatar avatar;

    public Avatar getAvatar() { return this.avatar; }

    public void setAvatar(Avatar avatar) { this.avatar = avatar; }

    private Notification notification;

    public Notification getNotification() { return this.notification; }

    public void setNotification(Notification notification) { this.notification = notification; }

    private MedicationRemainder medicationRemainder;

    public MedicationRemainder getMedicationRemainder() { return this.medicationRemainder; }

    public void setMedicationRemainder(MedicationRemainder medicationRemainder) { this.medicationRemainder = medicationRemainder; }

    private Person performer;

    public Person getPerformer() { return this.performer; }

    public void setPerformer(Person performer) { this.performer = performer; }

    private Boolean active;

    public Boolean getActive() { return this.active; }

    public void setActive(Boolean active) { this.active = active; }

    public Morning morning;

    public Evening evening;

    public Morning getMorning() {
        return morning;
    }

    public void setMorning(Morning morning) {
        this.morning = morning;
    }

    public Evening getEvening() {
        return evening;
    }

    public void setEvening(Evening evening) {
        this.evening = evening;
    }
}
