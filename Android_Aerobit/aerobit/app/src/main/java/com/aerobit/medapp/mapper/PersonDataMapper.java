package com.aerobit.medapp.mapper;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.Address;
import com.aerobit.medapp.model.remote.Contact;
import com.aerobit.medapp.model.remote.Link;
import com.aerobit.medapp.model.remote.Telecom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by thomas on 04/03/18.
 */

public class PersonDataMapper {

    private static PersonDataMapper instance;

    private PersonDataMapper() {
    }

    public static PersonDataMapper getInstance() {

        if (instance == null) {
            instance = new PersonDataMapper();
        }
        return instance;
    }

    public Person transform(com.aerobit.medapp.model.remote.Person personRemote) {

        Person person = null;
        if (personRemote != null) {

            person = new Person();
            person.setId(personRemote.get_id());
            if (personRemote.getName().getGiven().size() > 0)
                person.setFirstName(personRemote.getName().getGiven().get(0));
            if (personRemote.getName().getGiven().size() > 1)
                person.setLastName(personRemote.getName().getGiven().get(1));

            person.setActive(personRemote.getActive());
            person.setBirthDate(personRemote.getBirthDate());
            // making assumption only email is present
            person.setEmail(personRemote.getTelecom().get(0).getValue());

            if (personRemote.getAddress() != null && !personRemote.getAddress().isEmpty()) {

                Address address = personRemote.getAddress().get(0);
                person.setCountry(address.getCountry());
            }
            person.setGender(personRemote.getGender());
            person.setCreatedDate(personRemote.getCreatedAt());
            person.setUpdatedDate(personRemote.getUpdatedAt());
//            person.setRelatedPerson
        }
        return person;
    }


    public com.aerobit.medapp.model.remote.Person transformToRemote(Person localPerson) {
        com.aerobit.medapp.model.remote.Person remotePerson = null;
        if (localPerson != null) {
            remotePerson = new com.aerobit.medapp.model.remote.Person();
            remotePerson.setActive(localPerson.getActive());
            List<com.aerobit.medapp.model.remote.Address> addressList = new ArrayList<com.aerobit.medapp.model.remote.Address>();
            addressList.add( new Address());
            addressList.get(0).setCountry(localPerson.getCountry());
            remotePerson.setAddress(addressList);
            remotePerson.setPassword(localPerson.getPassword());

            remotePerson.setBirthDate(localPerson.getBirthDate());
            remotePerson.setTelecom(new ArrayList<Telecom>());
            remotePerson.getTelecom().add(new Telecom());
            remotePerson.getTelecom().get(0).setValue(localPerson.getEmail());
            remotePerson.setType("PERSON");
//            List<com.aerobit.medapp.model.remote.Contact> contact = new ArrayList<com.aerobit.medapp.model.remote.Contact>();
//            contact.add(new Contact());
//            contact.get(0).setTelecom(new ArrayList<Telecom>());
//            contact.get(0).getTelecom().add(new Telecom());
//            contact.get(0).getTelecom().get(0).setValue(localPerson.getEmail());
//            remotePerson.setContact(contact);
            com.aerobit.medapp.model.remote.Name name = new com.aerobit.medapp.model.remote.Name();
            name.setUse("official");
            name.setFamily(localPerson.getLastName());
            name.setGiven(new ArrayList<String>());
            name.getGiven().add(localPerson.getFirstName());
            name.getGiven().add(localPerson.getLastName());
            remotePerson.setName(name);
            remotePerson.setGender(localPerson.getGender());
            List<com.aerobit.medapp.model.remote.Link> link = new ArrayList<com.aerobit.medapp.model.remote.Link>();
            link.add(new Link());
            link.get(0).getTarget();
            remotePerson.setLink(link);
            remotePerson.setCreatedAt(localPerson.getCreatedDate());
            remotePerson.setUpdatedAt(localPerson.getUpdatedDate());
        }
        return remotePerson;
    }

    public List<Person> transform(Collection<com.aerobit.medapp.model.remote.Person> remotePersonCollection) {
        final List<Person> personList = new ArrayList<Person>();
        for (com.aerobit.medapp.model.remote.Person userEntity : remotePersonCollection) {
            final Person user = transform(userEntity);
            if (user != null) {
                personList.add(user);
            }
        }
        return personList;
    }
}
