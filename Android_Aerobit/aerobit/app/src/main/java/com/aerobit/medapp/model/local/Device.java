package com.aerobit.medapp.model.local;


import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class Device extends RealmObject  {

    @PrimaryKey
    @Required
    public String id;
    public String type;
    public String name;
    public String macAddress;
    public String color;
    public Boolean active;
    public String firmwareId;
    public Date manufactureDate;
    public Date createdDate;
    public Date updatedDate;

    public Device(){}

    public Device(String id, String type, String name, String macAddress, String color, Boolean active, String firmwareId, Date manufactureDate, Date createdDate, Date updatedDate) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.macAddress = macAddress;
        this.color = color;
        this.active = active;
        this.firmwareId = firmwareId;
        this.manufactureDate = manufactureDate;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getFirmwareId() {
        return firmwareId;
    }

    public void setFirmwareId(String firmwareId) {
        this.firmwareId = firmwareId;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", color='" + color + '\'' +
                ", active=" + active +
                ", firmwareId='" + firmwareId + '\'' +
                ", manufactureDate=" + manufactureDate +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}