package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Appointment;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;

/**
 * Created by ashwin on 2/3/18.
 */

public class AppointmentRepository {

    private static AppointmentRepository instance = null;

    private AppointmentRepository() {

    }

    public static final AppointmentRepository getInstance(){

        if(instance ==null){
            instance = new AppointmentRepository();
        }
        return instance;
    }

    public List<Appointment> getAllAppointments() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Appointment> appointmentsList = null;
        try{
            appointmentsList = mRealm.copyFromRealm(mRealm.where(Appointment.class).findAll());
            // handle no data found
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
        return appointmentsList;
    }

    public Appointment getAppointment(final int appointmentId) {
        Realm mRealm = Realm.getDefaultInstance();
        Appointment appointment = null;
        try{
             appointment = mRealm.copyFromRealm(mRealm.where(Appointment.class).equalTo("id", appointmentId).findFirst());
            // return no appointment - if no data found
            return appointment;
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return appointment;
    }

    public void insertAppointment(Appointment appointment) {

        Realm mRealm = Realm.getDefaultInstance();
        try{
            mRealm.beginTransaction();
            if(appointment.getId() == null && appointment.getId().isEmpty()){
                appointment.setId(AerobitUtil.genUUID());
            }
            mRealm.copyToRealmOrUpdate(appointment);
            mRealm.commitTransaction();
        } catch(Exception e){
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }

    }
}