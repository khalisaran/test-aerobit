package com.aerobit.medapp.http;

import com.aerobit.medapp.model.remote.Preference;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by khalisaran on 13/3/18.
 */

public interface PreferenceRetrofit {

    @PUT("preference/person")
    Call<Preference> save(@Query("id") String id, @Body Preference preference);

    @GET("preference/person")
    Call<Preference> getByPerson(@Query("id") String personid);



}
