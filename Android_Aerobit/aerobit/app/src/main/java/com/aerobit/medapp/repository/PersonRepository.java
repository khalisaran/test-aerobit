package com.aerobit.medapp.repository;


import android.renderscript.ScriptIntrinsicYuvToRGB;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class PersonRepository {

    private static PersonRepository instance = null;

    private PersonRepository() {

    }

    public static final PersonRepository getInstance(){

        if(instance ==null){
            instance = new PersonRepository();
        }
        return instance;
    }

    public List<Person> getAllPersons() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Person> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Person.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
       return data;
    }

    public Person getPerson(final int personId) {
        Realm mRealm = Realm.getDefaultInstance();
        Person data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Person.class).equalTo("id", personId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
        return data;
    }

    public Person insertPerson(final Person person) {
        Realm mRealm = Realm.getDefaultInstance();
        try{
            mRealm.beginTransaction();
            if(person.getId()==null || person.getId().isEmpty()){

                person.setId(AerobitUtil.genUUID());
            }
            final Person savedPerson = mRealm.copyToRealmOrUpdate(person);
            mRealm.commitTransaction();
            return savedPerson;
        } catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        }finally {
            mRealm.close();
        }
        return null;
    }

    public List<Person> insertPersons(final List<Person> person) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            final List<Person> savedPerson = mRealm.copyToRealmOrUpdate(person);
            mRealm.commitTransaction();
            return savedPerson;
        }catch(Exception e){
            mRealm.cancelTransaction();
        }finally {
            mRealm.close();
        }
        return null;
    }

    public List<Person> getPersonBySyncStatus(final String status) {
        Realm mRealm = Realm.getDefaultInstance();
        List<Person> persons = null;
        try {
            persons = mRealm.copyFromRealm(mRealm.where(Person.class).equalTo("syncStatus", status).findAll());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
        return persons;
    }

    public Person checkPersonExistsByEmail(String email){
        Realm mRealm = Realm.getDefaultInstance();
        Person person = null;
        try{
            person = mRealm.copyFromRealm(mRealm.where(Person.class).equalTo("email", email).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
        return person;
    }
    public Person deleteSyncedPerson(String idToBeRemoved){
        Realm mRealm = Realm.getDefaultInstance();
        Person person = null;
        try{
            RealmResults results= null;
            results=mRealm.where(Person.class).equalTo("id", idToBeRemoved).findAll();
            mRealm.beginTransaction();
            results.deleteFromRealm(0);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            mRealm.close();
        }
        return person;
    }
}
