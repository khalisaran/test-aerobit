package com.aerobit.medapp.http;

import com.aerobit.medapp.model.remote.MedicationDispense;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

 
public interface MedicationDispenseRetrofit {

    @GET("allmedicationdispense")
    Call<List<MedicationDispense>> getAllMedicationDispense();

}