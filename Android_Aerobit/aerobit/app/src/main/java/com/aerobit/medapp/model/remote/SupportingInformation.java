package com.aerobit.medapp.model.remote;

public class SupportingInformation {
    private String effect;

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEffect() {
        return this.effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    private String safety;

    public String getSafety() {
        return this.safety;
    }

    public void setSafety(String safety) {
        this.safety = safety;
    }

    private String description;

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String maxStrength;

    public String getMaxStrength() {
        return this.maxStrength;
    }

    public void setMaxStrength(String maxStrength) {
        this.maxStrength = maxStrength;
    }
}