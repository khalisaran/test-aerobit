package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Preferences;
import com.aerobit.medapp.model.local.Symptom;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class SymptomRepository {
    private static SymptomRepository instance = null;

    private SymptomRepository() {
    }

    public static final SymptomRepository getInstance(){

        if(instance ==null){
            instance = new SymptomRepository();
        }
        return instance;
    }

    public List<Symptom> getAllSymptoms() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Symptom> data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Symptom.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data ;
    }

    public Symptom getSymptom(final int symptomId) {
        Realm mRealm = Realm.getDefaultInstance();
        Symptom data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(Symptom.class).equalTo("id", symptomId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data ;
    }

    public Symptom insertPreference(final Symptom symptom) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (symptom.getId() == null && symptom.getId().isEmpty())
                symptom.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(symptom);
            mRealm.commitTransaction();
            return symptom;
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }
        return null;
    }
}
