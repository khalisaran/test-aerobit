package com.aerobit.medapp.mapper;

import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.remote.Address;
import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.model.remote.MedicationDispense;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by thomas on 04/03/18.
 */

public class MedicationDataMapper {

    private static MedicationDataMapper instance;

    private MedicationDataMapper(){}

    public static MedicationDataMapper getInstance(){

        if(instance == null){
            instance = new MedicationDataMapper();
        }
        return instance;
    }

    public Medication transform(com.aerobit.medapp.model.remote.MedicationDispense medicationDispenseRemote) {

        Medication medication = null;
        if(medicationDispenseRemote != null){

            medication = new Medication();
            medication.setId(medicationDispenseRemote.get_id());
            medication.setName(medicationDispenseRemote.getName());
            medication.setType(medicationDispenseRemote.getType());
            medication.setActive(medicationDispenseRemote.getActive());
            medication.setImage(medicationDispenseRemote.getImage().getData());
            medication.setStrength(medicationDispenseRemote.getStrength());
            medication.setCreatedDate(medicationDispenseRemote.getCreatedAt());
            medication.setUpdatedDate(medicationDispenseRemote.getUpdatedAt());
        }
        return medication;
    }

    public List<Medication> transform(Collection<com.aerobit.medapp.model.remote.MedicationDispense> remoteMedicationDispenseCollection) {
        final List<Medication> medicationList = new ArrayList<Medication>();
        for (com.aerobit.medapp.model.remote.MedicationDispense medEntity : remoteMedicationDispenseCollection) {
            final Medication medication = transform(medEntity);
            if (medication != null) {
                medicationList.add(medication);
            }
        }
        return medicationList;
    }
}
