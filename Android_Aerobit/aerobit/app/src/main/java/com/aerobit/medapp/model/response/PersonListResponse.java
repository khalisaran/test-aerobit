package com.aerobit.medapp.model.response;

import java.util.List;

/**
 * Created by ashwin on 1/3/18.
 */

public class PersonListResponse {
    public List<NamedResource> results;
}
