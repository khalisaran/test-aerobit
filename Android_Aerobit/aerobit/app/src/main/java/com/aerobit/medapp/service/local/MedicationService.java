package com.aerobit.medapp.service.local;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import com.aerobit.medapp.model.local.Medication;
import com.aerobit.medapp.provider.MedicationContentProvider;
import com.aerobit.medapp.repository.MedicationRepository;
import com.aerobit.medapp.util.AerobitUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by ashwin on 3/3/18.
 */

public class MedicationService {

    private MedicationRepository medicationRepository;

    private Realm mRealm;


    private static MedicationService instance = null;

    private MedicationService() {
        mRealm = Realm.getDefaultInstance();
    }

    public static final MedicationService getInstance() {

        if (instance == null) {
            instance = new MedicationService();
        }
        return instance;
    }

    public List<Medication> createMedication(ContentResolver contentResolver, List<Medication> medication) {

        ContentValues values = new ContentValues();
        String medicationJSON = AerobitUtil.toJson(medication);
        values.put("MEDICATIONOBJECT", medicationJSON);
        contentResolver.insert(MedicationContentProvider.CONTENT_URI, values);
        return medication;
    }

    public List<Medication> createMedicationAsync(Realm realm, List<Medication> medication) {

        MedicationRepository.getInstance().insertMedication( medication);

        return medication;
    }

    public List<Medication> getAllMedication(ContentResolver contentResolver) {

        String[] projection = {"MEDICATIONOBJECT"};
        Cursor cursor = contentResolver.query(MedicationContentProvider.CONTENT_URI, projection, null, null,
                null);

        String medicationsJSON = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    medicationsJSON = cursor.getString(cursor.getColumnIndex("MEDICATIONOBJECT"));
                } while (cursor.moveToNext());
            }
        }

        List<Medication> medications = (List<Medication>) AerobitUtil.fromJson(medicationsJSON, new TypeToken<ArrayList<Medication>>() {
        }.getType());

        return medicationsJSON == null ? new ArrayList<Medication>() : medications;

    }

    public List<Medication> getMedicationByType(String medicationType) {

        medicationRepository = medicationRepository.getInstance();

        List<Medication> medicationsJSON = medicationRepository.getMedicationByType(medicationType);

        return medicationsJSON;

    }

    public List<Medication> getMedicationByName(String name, String type) {

        medicationRepository = medicationRepository.getInstance();

        List<Medication> medicationsJSON = medicationRepository.getMedicationByName(name, type);

        return medicationsJSON;

    }
}
