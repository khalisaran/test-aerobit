package com.aerobit.medapp.model.local;

/**
 * Created by edrinthomas on 04/03/18.
 */

public enum Gender {
    MALE("MALE"),
    FEMALE ("FEMALE");

    private final String value;

    private Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
