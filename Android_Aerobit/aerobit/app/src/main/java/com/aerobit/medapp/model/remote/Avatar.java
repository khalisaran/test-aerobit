package com.aerobit.medapp.model.remote;

public class Avatar
{
  private String color;

  public String getColor() { return this.color; }

  public void setColor(String color) { this.color = color; }

  private String image;

  public String getImage() { return this.image; }

  public void setImage(String image) { this.image = image; }
}