package com.aerobit.medapp.sync.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.aerobit.medapp.sync.adapter.CarePlanSyncAdapter;

/**
 * Created by ashwin on 13/3/18.
 */

public class CarePlanSyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();
    private static CarePlanSyncAdapter sSyncAdapter = null;

    @Override
    public void onCreate(){
        synchronized (sSyncAdapterLock){
            if(sSyncAdapter == null)
                sSyncAdapter  =  new CarePlanSyncAdapter(getApplicationContext(),true);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}

