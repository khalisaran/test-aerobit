package com.aerobit.medapp.model.local;


import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class DoseRegister extends RealmObject {

    @PrimaryKey
    @Required
    public String id;
    public String category;
    public String status;
    public CarePlan doseId;
    public CarePlan medicationId;
    public Device device;
    public String shake;
    public String exhale1;
    public String inhale;
    public String press;
    public String holdBreath;
    public String exhale2;
    public Date createdDate;
    public Date updatedDate;

    private String syncStatus;

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public DoseRegister(){}

    public DoseRegister(String id, String category, String status, CarePlan doseId, CarePlan medicationId, Device device, String shake, String exhale1, String inhale, String press, String holdBreath, String exhale2, Date createdDate, Date updatedDate) {
        this.id = id;
        this.category = category;
        this.status = status;
        this.doseId = doseId;
        this.medicationId = medicationId;
        this.device = device;
        this.shake = shake;
        this.exhale1 = exhale1;
        this.inhale = inhale;
        this.press = press;
        this.holdBreath = holdBreath;
        this.exhale2 = exhale2;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CarePlan getDoseId() {
        return doseId;
    }

    public void setDoseId(CarePlan doseId) {
        this.doseId = doseId;
    }

    public CarePlan getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(CarePlan medicationId) {
        this.medicationId = medicationId;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getShake() {
        return shake;
    }

    public void setShake(String shake) {
        this.shake = shake;
    }

    public String getExhale1() {
        return exhale1;
    }

    public void setExhale1(String exhale1) {
        this.exhale1 = exhale1;
    }

    public String getInhale() {
        return inhale;
    }

    public void setInhale(String inhale) {
        this.inhale = inhale;
    }

    public String getPress() {
        return press;
    }

    public void setPress(String press) {
        this.press = press;
    }

    public String getHoldBreath() {
        return holdBreath;
    }

    public void setHoldBreath(String holdBreath) {
        this.holdBreath = holdBreath;
    }

    public String getExhale2() {
        return exhale2;
    }

    public void setExhale2(String exhale2) {
        this.exhale2 = exhale2;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "DoseRegister{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", status='" + status + '\'' +
                ", doseId=" + doseId +
                ", medicationId=" + medicationId +
                ", device=" + device +
                ", shake='" + shake + '\'' +
                ", exhale1='" + exhale1 + '\'' +
                ", inhale='" + inhale + '\'' +
                ", press='" + press + '\'' +
                ", holdBreath='" + holdBreath + '\'' +
                ", exhale2='" + exhale2 + '\'' +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}