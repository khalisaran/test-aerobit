package com.aerobit.medapp.sync.adapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.OperationCanceledException;
import android.os.RemoteException;
import android.util.Log;

import com.aerobit.medapp.mapper.PersonDataMapper;
import com.aerobit.medapp.model.local.Person;
import com.aerobit.medapp.model.local.SyncStatus;
import com.aerobit.medapp.provider.PersonContentProvider;
import com.aerobit.medapp.service.local.PersonService;
import com.aerobit.medapp.service.remote.PersonLoadDataService;
import com.aerobit.medapp.sync.authenticator.AccountGeneral;
import com.aerobit.medapp.util.AerobitUtil;
import com.aerobit.medapp.util.PrefManager;

import java.io.IOException;
import java.util.List;


/**
 * Created by manikandan on 04/03/18.
 */

public class PersonSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "PERSON_SYNC_ADAPTER";
    private final ContentResolver resolver;

    private PersonService personService;

    private PersonLoadDataService personLoadDataService;

    private PrefManager prefManager;

    private PersonDataMapper personDataMapper;

    public PersonSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.resolver = context.getContentResolver();
    }

    public PersonSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.resolver = context.getContentResolver();
    }


    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        try {
            Log.v("SyncMe", "Called Sync in person....");
            createOrUpdatePerson(contentProviderClient);

            /**
             * Algorithm
             * ==========
             * Need to find data count for local DB
             * If row.Count <= 0 Then
             *  Need to request server with User_Id and Fresh_RecordFlag: True // True: Fresh DB, False: Updated record
             * Else
             *  Need to request server with User_Id and Fresh_RecordFlag: False
             *
             * Response: List<Person>  // Expected response from server be collection data with SyncFlag = NOT_SYNC
             *
             * If Fresh_RecordFlag = True Then
             *  Insert data into local DB send Response with List<updated_record_id>
             * Else
             *  Insert data into local DB
             *  List<Person> = Fetch local DB record where SyncFlag = NOT_SYNC
             *  Request: server with List<updated_record_id> and List<Person>
             *  Response: List<updated_record_id> update the SyncFlag based on each update_record_id
             */

            //Todo Need to remove below function

        } catch (IOException ex) {
            Log.e(TAG, "Error synchronizing!", ex);
            syncResult.stats.numIoExceptions++;
        } catch (OperationCanceledException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createOrUpdatePerson(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {
        try {
            Person returningCreatedPersonObject = null;
            com.aerobit.medapp.model.remote.Person remotePerson = null;

            personService = personService.getInstance();

            List<Person> personList = personService.getPersonBySyncStatus(getContext().getContentResolver(), "NOT_SYNCED");
            for (Person person : personList) {
                if (AerobitUtil.findHyphenPresentOrNot(person.getId()) == "1") {

                    /**
                     *  create a person in server since id containing hyphen
                     */
                    prefManager = prefManager.getInstance();
                    personLoadDataService = personLoadDataService.getInstance();

                    try {
                        personDataMapper = personDataMapper.getInstance();

                        remotePerson = personDataMapper.transformToRemote(person);
                        // Get Guardian Id
                        String guardianId = person.getGuardian().getId();
                        /**
                         *  returningCreatedPersonObject - Response Object returning from remote server
                         */

                        returningCreatedPersonObject = personLoadDataService.postRelatedPerson(remotePerson, guardianId, prefManager.getAuthorize(getContext().getApplicationContext()), prefManager.getXid(getContext().getApplicationContext()));
                        // setting syncStatus as "SYNCED" since it posts the data to server
                        //TODO handle error case
                        returningCreatedPersonObject.setSyncStatus(SyncStatus.SYNCED.getValue());
                        /**
                         *  returingUpdatedPersonObject - Response Object returning from local database
                         */
                        Person returingUpdatedPersonObject = personService.createPerson(getContext().getContentResolver(), returningCreatedPersonObject);

                        if (returingUpdatedPersonObject.getSyncStatus() == SyncStatus.SYNCED.getValue()) {
                            String idToBeRemovedId = person.getId();
                            /**
                             *  delete uuid based person object since its details are synced to remote server and new server id is created in local
                             */
                            personService.deleteSyncedPerson(getContext().getContentResolver(), idToBeRemovedId);
                        }
                    } catch (Exception e) {
                        Log.v("MedApp", "Error in Person Sync");
                        e.printStackTrace();
                    }
                } else {
                    /**
                     *  update a person in server since id is not containing hyphen
                     */
                    try {
                        personLoadDataService.updatePerson(remotePerson, person.getId(), prefManager.getAuthorize(getContext().getApplicationContext()), prefManager.getXid(getContext().getApplicationContext()));
                        Log.v("MedApp", "Updated Person Sync success");
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("MedApp", "Error in Updating Person Sync");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Manual force Android to perform a sync with our SyncAdapter.
     */
    public static void performSync() {
        Bundle b = new Bundle();
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(AccountGeneral.getAccount(),
                PersonContentProvider.CONTENT_AUTHORITY, b);
    }
}
