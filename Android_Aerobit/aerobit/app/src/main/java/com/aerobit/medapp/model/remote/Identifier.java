package com.aerobit.medapp.model.remote;

import java.util.Date;
import java.util.List;

/**
 * Created by ashwin on 1/3/18.
 */


public class Identifier {
    private String use;

    private List<Coding> coding;

    public void setUse(String use) {
        this.use = use;
    }

    public String getUse() {
        return this.use;
    }

    public void setCoding(List<Coding> coding) {
        this.coding = coding;
    }

    public List<Coding> getCoding() {
        return this.coding;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}