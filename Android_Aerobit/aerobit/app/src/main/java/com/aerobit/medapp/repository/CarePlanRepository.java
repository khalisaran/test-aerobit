package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class CarePlanRepository {

    private static CarePlanRepository instance = null;

    private CarePlanRepository() {

    }

    public static final CarePlanRepository getInstance(){

        if(instance ==null){
            instance = new CarePlanRepository();
        }
        return instance;
    }

    public List<CarePlan> getAllCarePlans() {
        Realm mRealm = Realm.getDefaultInstance();
        List<CarePlan> carePlanList = null;
        try{
            carePlanList = mRealm.copyFromRealm(mRealm.where(CarePlan.class).equalTo("active", true).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return carePlanList;
    }

    public CarePlan getCarePlan( final int carePlanId) {
        Realm mRealm = Realm.getDefaultInstance();
        CarePlan carePlan = null;
        try{
            carePlan = mRealm.copyFromRealm(mRealm.where(CarePlan.class).equalTo("id", carePlanId).findFirst());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return carePlan;
    }

    public void deactivateCarePlan(String carePlanId) {
        Realm mRealm = Realm.getDefaultInstance();
        CarePlan updateCarePlan = null;
        try{
            updateCarePlan = mRealm.where(CarePlan.class).equalTo("id", carePlanId).findFirst();
            mRealm.beginTransaction();
            updateCarePlan.setActive(false);
            mRealm.commitTransaction();
        }catch (Exception e){
            mRealm.cancelTransaction();
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
    }

    public List<CarePlan> getCarePlanByPatientId(final String performer) {
        Realm mRealm = Realm.getDefaultInstance();
        List<CarePlan> carePlans = null;
        try{
            carePlans = mRealm.copyFromRealm(mRealm.where(CarePlan.class)
                    .equalTo("active", true)
                    .equalTo("performer.id", performer).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }

        return carePlans;
    }

    public List<CarePlan> getCarePlanByTypeAndPatientId( final String performer, String type) {
        Realm mRealm = Realm.getDefaultInstance();
        List<CarePlan> carePlanList = null;
        try{
            carePlanList = mRealm.copyFromRealm(mRealm.where(CarePlan.class).equalTo("performer", performer).equalTo("type", type).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return carePlanList;
    }

    public void insertCarePlan( CarePlan carePlan) {
        Realm mRealm = Realm.getDefaultInstance();
        try{
            mRealm.beginTransaction();
            if(carePlan.getId() == null)
                carePlan.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(carePlan);
            mRealm.commitTransaction();
        }catch (Exception e){
            mRealm.cancelTransaction();
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
    }

    public void insertCarePlan( List<CarePlan> carePlans) {
        Realm mRealm = Realm.getDefaultInstance();
        try{

            mRealm.beginTransaction();
            for(CarePlan carePlan: carePlans){
                if(carePlan.getId() == null && carePlan.getId().isEmpty()){
                    carePlan.setId(AerobitUtil.genUUID());
                }
            }
            mRealm.copyToRealmOrUpdate(carePlans);
            mRealm.commitTransaction();
        } catch (Exception e){
            mRealm.cancelTransaction();
            e.printStackTrace();
        } finally {
            mRealm.close();
        }

    }

}
