package com.aerobit.medapp.model.local;


import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ashwin on 1/3/18.
 */

public class Friend extends RealmObject {
    @PrimaryKey
    @Required
    public String id;
    public Boolean active;
    public Person peroformer;
    public String firstName;
    public String lastName;
    public String email;
    public String code;
    public Date createdDate;
    public Date updatedDate;

    public Friend(){}

    public Friend(String id, Boolean active, Person peroformer, String firstName, String lastName, String email, String code, Date createdDate, Date updatedDate) {
        this.id = id;
        this.active = active;
        this.peroformer = peroformer;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.code = code;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Person getPeroformer() {
        return peroformer;
    }

    public void setPeroformer(Person peroformer) {
        this.peroformer = peroformer;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "id=" + id +
                ", active=" + active +
                ", peroformer=" + peroformer +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", code='" + code + '\'' +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}