package com.aerobit.medapp.interceptor;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ashwin on 4/3/18.
 */


public class AuthInterceptor implements Interceptor {

    private String authToken;

    private String xid;

    public AuthInterceptor(String token, String xid) {
        this.authToken = token;
        this.xid = xid;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("Authorization", authToken);

        builder.addHeader("xid",xid);

        Request request = builder.build();
        return chain.proceed(request);
    }
}