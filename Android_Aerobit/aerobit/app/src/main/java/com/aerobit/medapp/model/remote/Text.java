package com.aerobit.medapp.model.remote;

/**
 * Created by ashwin on 1/3/18.
 */

public class Text {
    private String status;

    private String div;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setDiv(String div) {
        this.div = div;
    }

    public String getDiv() {
        return this.div;
    }

    public String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}