package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.DoseRegister;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class DoseRegisterRepository {
    private static DoseRegisterRepository instance = null;

    private DoseRegisterRepository() {
    }

    public static final DoseRegisterRepository getInstance(){

        if(instance ==null){
            instance = new DoseRegisterRepository();
        }
        return instance;
    }
    public List<DoseRegister> getAllDoseRegisters() {
        Realm mRealm = Realm.getDefaultInstance();
        List<DoseRegister> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(DoseRegister.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }


        return data ;
    }

    public DoseRegister getDoseRegister(final int doseRegisterId) {
        Realm mRealm = Realm.getDefaultInstance();
        DoseRegister data = null;
        try {
            data = mRealm.copyFromRealm(mRealm.where(DoseRegister.class).equalTo("id", doseRegisterId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }

        return data;
    }

    public void insertDoseRegister(DoseRegister doseRegister) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (doseRegister.getId() == null && doseRegister.getId().isEmpty())
                doseRegister.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(doseRegister);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }

    }
}
