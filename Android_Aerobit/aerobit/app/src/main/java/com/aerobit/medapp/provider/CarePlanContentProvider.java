package com.aerobit.medapp.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.aerobit.medapp.model.local.CarePlan;
import com.aerobit.medapp.repository.CarePlanRepository;
import com.aerobit.medapp.util.AerobitUtil;

import io.realm.Realm;

/**
 * Created by ashwin on 3/3/18.
 */

public class CarePlanContentProvider extends ContentProvider {

    CarePlanRepository carePlanRepository;
    private Realm mRealm;

    static final public String CONTENT_AUTHORITY = "com.aerobit.medapp.provider.CarePlanContentProvider";
    static final public String URL = "content://" + CONTENT_AUTHORITY + "/careplan";
    static final public Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int CAREPLAN = 1;
    static final int CAREPLAN_ID = 2;
    static final int CAREPLAN_PATIENT_ID = 3;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(CONTENT_AUTHORITY, "careplan" , CAREPLAN);
        matcher.addURI(CONTENT_AUTHORITY, "careplan/#", CAREPLAN_ID);
        matcher.addURI(CONTENT_AUTHORITY, "careplan/patient/#", CAREPLAN_PATIENT_ID);

        return matcher;
    }
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        MatrixCursor myCursor = new MatrixCursor(new String[]{"CAREPLANOBJECT"});
        Object careplan = null;
        String uriStr = uri.toString();

        if(uriStr.matches("^"+URL+"/patient/#"+"[A-Za-z0-9\\-]*$")){

            String result[] = uriStr.split("#");
            String returnValue = result[result.length - 1];
            careplan = carePlanRepository.getCarePlanByPatientId(returnValue);
        }
        else if(uriStr.matches("^"+URL+"$")){
            careplan = carePlanRepository.getAllCarePlans();
        }
        else if(uriStr.matches("^"+URL+"/#"+"[A-Za-z0-9\\-]*$")){
            careplan = carePlanRepository.getCarePlan( new Integer(uri.getLastPathSegment()));
        }
        String json = AerobitUtil.toJson(careplan);
        System.out.println(""+ json);
        Object[] rowData = new Object[]{json};

        myCursor.addRow(rowData);

        return myCursor;
    }

    @Override
    public boolean onCreate() {

        carePlanRepository = CarePlanRepository.getInstance();
        mRealm = Realm.getDefaultInstance();
        return carePlanRepository == null ? false: true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        carePlanRepository = carePlanRepository.getInstance();
        CarePlan careplanJSON = (CarePlan) AerobitUtil.fromJson(values.get("CAREPLANOBJECT").toString(), CarePlan.class);
        try {
            carePlanRepository.insertCarePlan(careplanJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri notifyURI = Uri.parse(URL+careplanJSON.getId());
        //TODO: need to notify
        getContext().getContentResolver().notifyChange(notifyURI, null, true);
        return notifyURI;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
