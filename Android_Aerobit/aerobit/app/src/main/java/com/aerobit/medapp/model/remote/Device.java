package com.aerobit.medapp.model.remote;

import java.util.Date;

public class Device
{
  private String _id;

  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }

  public boolean isActive() {
    return active;
  }

  private Date updatedAt;

  public Date getUpdatedAt() { return this.updatedAt; }

  public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

  private Date createdAt;

  public Date getCreatedAt() { return this.createdAt; }

  public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

  private String type;

  public String getType() { return this.type; }

  public void setType(String type) { this.type = type; }

  private String status;

  public String getStatus() { return this.status; }

  public void setStatus(String status) { this.status = status; }

  private Date manufactureDate;

  public Date getManufactureDate() { return this.manufactureDate; }

  public void setManufactureDate(Date manufactureDate) { this.manufactureDate = manufactureDate; }

  private String color;

  public String getColor() { return this.color; }

  public void setColor(String color) { this.color = color; }

  private boolean active;

  public boolean getActive() { return this.active; }

  public void setActive(boolean active) { this.active = active; }

  private Udi udi;

  public Udi getUdi() { return this.udi; }

  public void setUdi(Udi udi) { this.udi = udi; }

  private Text text;

  public Text getText() { return this.text; }

  public void setText(Text text) { this.text = text; }

  private String resourceType;

  public String getResourceType() { return this.resourceType; }

  public void setResourceType(String resourceType) { this.resourceType = resourceType; }
}