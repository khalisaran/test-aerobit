package com.aerobit.medapp.http;

import android.text.TextUtils;
import com.aerobit.medapp.interceptor.AuthInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ashwin on 4/3/18.
 */

public class HttpClient {

    public static final String API_BASE_URL = "https://vbjgjko57j.execute-api.us-east-1.amazonaws.com/api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    public static <S> S httpService(
            Class<S> serviceClass, final String authToken, final String xid) {

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        if (!TextUtils.isEmpty(authToken)) {
            AuthInterceptor interceptor =
                    new AuthInterceptor(authToken, xid);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }
        return retrofit.create(serviceClass);
    }
}