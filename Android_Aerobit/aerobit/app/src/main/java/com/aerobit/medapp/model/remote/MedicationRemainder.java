package com.aerobit.medapp.model.remote;

public class MedicationRemainder
{
  private Evening evening;

  public Evening getEvening() { return this.evening; }

  public void setEvening(Evening evening) { this.evening = evening; }

  private Morning morning;

  public Morning getMorning() { return this.morning; }

  public void setMorning(Morning morning) { this.morning = morning; }

  private Boolean shakeToTake;

  public Boolean getShakeToTake() { return this.shakeToTake; }

  public void setShakeToTake(Boolean shakeToTake) { this.shakeToTake = shakeToTake; }

  private Boolean showMedName;

  public Boolean getShowMedName() { return this.showMedName; }

  public void setShowMedName(Boolean showMedName) { this.showMedName = showMedName; }

  private String popupOnLastRemainder;

  public String getPopupOnLastRemainder() { return this.popupOnLastRemainder; }

  public void setPopupOnLastRemainder(String popupOnLastRemainder) { this.popupOnLastRemainder = popupOnLastRemainder; }

  private String popupNotification;

  public String getPopupNotification() { return this.popupNotification; }

  public void setPopupNotification(String popupNotification) { this.popupNotification = popupNotification; }

  private String snooze;

  public String getSnooze() { return this.snooze; }

  public void setSnooze(String snooze) { this.snooze = snooze; }

  private String maxPerMed;

  public String getMaxPerMed() { return this.maxPerMed; }

  public void setMaxPerMed(String maxPerMed) { this.maxPerMed = maxPerMed; }
}