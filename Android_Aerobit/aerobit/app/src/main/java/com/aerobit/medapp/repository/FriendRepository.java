package com.aerobit.medapp.repository;

import com.aerobit.medapp.model.local.Friend;
import com.aerobit.medapp.util.AerobitUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ashwin on 2/3/18.
 */

public class FriendRepository {
    private static FriendRepository instance = null;

    private FriendRepository() {

    }

    public static final FriendRepository getInstance(){

        if(instance ==null){
            instance = new FriendRepository();
        }
        return instance;
    }

    public List<Friend> getAllFriends() {
        Realm mRealm = Realm.getDefaultInstance();
        List<Friend> data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Friend.class).findAll());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }

        return data ;
    }

    public Friend getFriend(final int friendId) {
        Realm mRealm = Realm.getDefaultInstance();
        Friend data = null;
        try{
            data = mRealm.copyFromRealm(mRealm.where(Friend.class).equalTo("id", friendId).findFirst());
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
        return data;
    }

    public void insertFriend(Friend friend) {
        Realm mRealm = Realm.getDefaultInstance();
        try {
            mRealm.beginTransaction();
            if (friend.getId() == null && friend.getId().isEmpty())
                friend.setId(AerobitUtil.genUUID());
            mRealm.copyToRealmOrUpdate(friend);
            mRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            mRealm.cancelTransaction();
        } finally {
            mRealm.close();
        }
    }
}